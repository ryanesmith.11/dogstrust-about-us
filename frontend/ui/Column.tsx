import cn from "classnames";
import Link from "next/link";

export function Columns(props: any) {
  return (
    <div
      className={cn(
        "columns wp-block-columns flex flex-row mx-auto overflow-visible w-full",
        props.attrs?.className,
        props.attrs?.verticalAlignment == "center" &&
          "!items-center !justify-center"
      )}
      style={{
        color: props.attrs.textColor,
        background: props.attrs.backgroundColor,
      }}
    >
      {props.children}
    </div>
  );
}

export function Column(props: any) {
  return (
    <div
      className={cn(
        "column wp-block-column flex flex-col h-auto",
        props.attrs?.className,
        !props.attrs?.width && "!basis-full", //if no explicit width is set, make it full width
        props.attrs?.className,
        props.attrs?.backgroundColor && "bg-" + props.attrs?.backgroundColor,
        props.attrs?.textColor && "text-" + props.attrs?.textColor
      )}
      style={{
        width: props.attrs.width,
      }}
    >
      {props.children}
    </div>
  );
}
