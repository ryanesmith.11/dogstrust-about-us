import Image from "next/image";
import { BlockContent } from "./BlockContent";
import Pill from "./components/atoms/Pill";
import { Container } from "./Container";
import moment from "moment";
import { PostFeed } from "./blocks/PostFeed";
import Button from "./components/atoms/Button";
import Link from "next/link";

export function PostTemplate({
  postData,
  blockData,
  allPosts,
  settings,
}: {
  postData: any;
  blockData: any;
  allPosts: any;
  settings: any;
}) {
  return (
    <>
      <Container settings={settings}>
        <div className="w-fit flex items-center justify-center gap-[9px] mb-[43px]">
          <Link href="/">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="14"
              height="15"
              viewBox="0 0 14 15"
              fill="none"
            >
              <g clipPath="url(#clip0_650_4184)">
                <path
                  d="M0.00991081 9.50182C0.00991081 8.55131 0.0355695 7.60079 0.00349615 6.65027C-0.0317845 5.67961 0.192729 4.9071 1.00739 4.29246C2.6303 3.06989 4.17944 1.73648 5.78631 0.487042C6.6106 -0.154471 7.22641 -0.15783 8.02824 0.460172C9.76661 1.8003 11.505 3.14378 13.1856 4.56116C13.686 4.98435 13.9907 5.65946 13.9907 6.39501C13.9971 8.40688 14.0067 10.4221 13.9907 12.434C13.9778 14.0159 12.9964 14.9664 11.4729 15C8.89422 15.0538 9.12515 14.7246 9.14439 12.5515C9.15401 11.6279 9.16684 10.7042 9.14118 9.78395C9.11232 8.73939 8.82045 8.45726 7.83901 8.45054C7.25206 8.44718 6.66512 8.45054 6.07818 8.45054C5.0358 8.4539 4.90109 8.59161 4.89468 9.71678C4.88505 10.8084 4.86902 11.8966 4.89788 12.9882C4.93958 14.4828 4.54828 15.1142 2.99914 14.9799C2.13637 14.906 1.2319 15.0739 0.542328 14.2846C0.221595 13.9152 0.0131181 13.5423 0.0195328 13.0318C0.0355695 11.8596 0.0259475 10.6841 0.0259475 9.50854C0.0227401 9.50854 0.0163255 9.50854 0.0131181 9.50854L0.00991081 9.50182Z"
                  style={{ fill: "var(--color-primary)" }}
                />
              </g>
              <defs>
                <clipPath id="clip0_650_4184">
                  <rect width="14" height="15" fill="white" />
                </clipPath>
              </defs>
            </svg>
          </Link>
          <span>{"/"}</span>
          <span>News</span>
        </div>
        <p className="mb-[41px]">{moment(postData.post_date).format("LL")}</p>
        <h1>{postData.post_title}</h1>
        <div className="gap-[35px] flex items-center">
          {postData.categoryNames &&
            postData.categoryNames.map((c: any, i: number) => (
              <Pill text={c} key={i} />
            ))}
          <p>
            {postData.acfData.author_override
              ? postData.acfData.author_override
              : postData.postAuthor}
          </p>
        </div>
      </Container>
      <div className="xl:px-[70px] px-[20px]">
        <div className="relative max-w-[1466px] w-full xl:h-[646px] lg:h-[500px] h-[400px] rounded-xl overflow-hidden mx-auto">
          <Image
            src={postData.fimg_url}
            fill
            alt={postData.post_title}
            className="object-cover"
          />
        </div>
      </div>
      <Container settings={settings}>
        <article>
          <BlockContent
            settings={settings}
            blockData={blockData}
            allPosts={allPosts}
          />
        </article>
      </Container>
      <Container settings={settings}>
        <div className="flex justify-between w-full mb-[50px]">
          <h2>
            Other <em>Articles</em>
          </h2>
          <Button
            effect={settings.btn_transition}
            variant="secondary"
            text="Read more"
            href="/news"
          />
        </div>
        <PostFeed
          data={{
            number_of_columns: "3",
            number_of_posts: "3",
            show_load_more: "0",
            number_on_load: "0",
            load_more_text: "Load More",
            show_search: "0",
            show_categories: "0",
            posts: null,
          }}
          posts={allPosts["post"].filter(function (post: any) {
            return post.post_name !== postData.post_name;
          })}
          settings={settings}
        />
      </Container>
    </>
  );
}
