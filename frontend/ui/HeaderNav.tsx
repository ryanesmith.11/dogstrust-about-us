"use client";

import Link from "next/link";
import Button from "./components/atoms/Button";
import { decode } from "html-entities";
import { useEffect, useState } from "react";
import { motion } from "framer-motion";
import { usePathname } from "next/navigation";
import cn from "classnames";
import { getLastPath } from "utils/url";

const API_URL = process.env.NEXT_PUBLIC_WORDPRESS_API_URL;

export function HeaderNav({
  menuItems,
  setMobileNavOpen,
  mobileNavOpen,
  settings,
}: {
  menuItems: any;
  setMobileNavOpen: any;
  mobileNavOpen: boolean;
  settings: any;
}) {
  const [selected, setSelected] = useState<number | null>(null);
  const path = usePathname();
  useEffect(() => {
    const itemSelected = menuItems.findIndex(
      (n: any) => getLastPath(n.url) == getLastPath(path)
    );
    setSelected(itemSelected);
  }, [path]);
  return (
    <>
      <div
        className="basis-full md:flex md:basis-auto items-center hidden"
        id="primary-menu"
        onMouseLeave={() =>
          setSelected(
            menuItems.findIndex((n: any) => getLastPath(n.url) == path)
          )
        }
      >
        <ul className="navul mynav md:mt-0 md:flex-row lg:space-x-[37px] space-x-[18px] md:space-y-0 lg:text-[14px] text-[12px] font-secondary flex flex-col items-center mt-6 space-y-[37px]">
          {menuItems.map((n: any, i: number) => {
            const title = decode(n.title);
            if (i === menuItems.length - 1)
              return (
                <Button
                  variant="secondary"
                  href={n.url.replace(API_URL, "/")}
                  text={title}
                  onMouseOver={() => setSelected(null)}
                  key={i}
                  effect={settings.btn_transition}
                />
              );
            return (
              <Link href={n.url.replace(API_URL, "/")} key={i}>
                <motion.li
                  className={cn(
                    "menu-link title",
                    i === selected ? "selected" : ""
                  )}
                  onMouseOver={() => setSelected(i)}
                  animate
                  style={{ color: settings.header_text_color }}
                >
                  {i === selected && (
                    <motion.div
                      className="motiondiv underline"
                      layoutId="underline"
                      style={{
                        backgroundColor: settings.header_text_hover_color,
                      }}
                    />
                  )}
                  {decode(n.title)}
                </motion.li>
              </Link>
            );
          })}
        </ul>
      </div>
      <div
        className="mobile-hamburger md:hidden p-[8px] flex items-center justify-center cursor-pointer"
        onClick={() => {
          setMobileNavOpen(!mobileNavOpen);
        }}
      >
        <button
          className={cn(
            "bg-[color:var(--header-mobile-hamburger-color)])",
            mobileNavOpen && "open"
          )}
          id="nav-icon3"
        >
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </button>
      </div>
    </>
  );
}
