import { getPosts } from "lib/api";
import Link from "next/link";
import { Calendly, CalendlyBook } from "./blocks/Calendly";
import { GravityForm } from "./blocks/GravityForm";
import { Hero } from "./blocks/Hero";
import { Slider } from "./blocks/Slider";
import { PostFeed } from "./blocks/PostFeed";
import { PricingSummary } from "./blocks/PricingSummary";
import { ToggleMenu } from "./blocks/ToggleMenu";
import { Column, Columns } from "./Column";
import Button from "./components/atoms/Button";
import { Container } from "./Container";
import { parse } from "node-html-parser";
import { Cover } from "./components/molecules/Cover";
import { Buttons } from "./components/molecules/Buttons";
import { AccordionItem } from "./blocks/AccordionItem";
const Map: any = dynamic(() => import("./blocks/Map") as any, { ssr: false });
import { Testimonials } from "./blocks/Testimonials";
import ImageComponent from "./components/atoms/Image";
import dynamic from "next/dynamic";
type blockDataProps = {
  blockData: any;
};
import Parser from "html-react-parser";
import { LogoSlider } from "./blocks/LogoSlider";
import { Marketo } from "./blocks/Marketo";
import { Hero2 } from "./blocks/Hero2";

const API_URL = process.env.NEXT_PUBLIC_WORDPRESS_API_URL;

let blockIndex = -1;
export function BlockContent({
  settings,
  blockData,
  allPosts,
}: {
  settings: any;
  blockData: any;
  allPosts: any;
}) {
  return (
    <>
      {/* <div dangerouslySetInnerHTML={{ __html: page.content.rendered }} /> */}
      {/* {JSON.stringify(blockData)} */}
      {blockData.map((b: any, i: number) => {
        blockIndex++;
        return (
          <>
            {(() => {
              switch (b.blockName) {
                case "core/block": //reusable block
                  return (
                    <BlockContent
                      settings={settings}
                      blockData={b.innerBlocks}
                      allPosts={allPosts}
                    />
                  );
                case "core/group":
                  let groupRoot = parse(b.innerHTML);
                  const idElement = groupRoot.querySelector("[id]");
                  if (idElement) {
                    const id = idElement.id;
                    console.log(id);
                    b.attrs.id = id;
                  }
                  return (
                    <Container settings={settings} attrs={b.attrs} key={i}>
                      <BlockContent
                        settings={settings}
                        blockData={b.innerBlocks}
                        allPosts={allPosts}
                      />
                    </Container>
                  );
                case "core/columns":
                  return (
                    <Columns attrs={b.attrs} key={i}>
                      <BlockContent
                        settings={settings}
                        blockData={b.innerBlocks}
                        allPosts={allPosts}
                      />
                    </Columns>
                  );
                case "core/column":
                  return (
                    <Column attrs={b.attrs} key={i}>
                      <BlockContent
                        settings={settings}
                        blockData={b.innerBlocks}
                        allPosts={allPosts}
                      />
                    </Column>
                  );
                case "core/cover":
                  // return (
                  //   <div
                  //     dangerouslySetInnerHTML={{ __html: b.innerHTML }}
                  //     key={i}
                  //   />
                  // );
                  return (
                    // @ts-expect-error Server Component
                    <Cover attrs={b.attrs} key={i} blockIndex={blockIndex}>
                      <BlockContent
                        settings={settings}
                        blockData={b.innerBlocks}
                        allPosts={allPosts}
                      />
                    </Cover>
                  );
                // case "core/image":
                //   const data = b.innerHTML;

                //   const regex = /<img.*?src=['"](.*?)['"]/;

                //   return (
                //     <>
                //       {JSON.stringify(b)}
                //       {/* <ImageComponent
                //         src={regex.exec(data)[1]}
                //         attrs={b.attrs}
                //       /> */}
                //       ;
                //     </>
                //   );
                case "core/heading":
                case "core/image":
                case "core/paragraph":
                case "core/video":
                case "core/separator":
                default:
                  return <>{Parser(b.innerHTML)}</>;
                case "core/quote":
                  let quoteText = b.innerBlocks.map((block: any, i: number) => {
                    let root = parse(block.innerHTML);
                    let text = root.innerText || root.firstChild.innerText;
                    return <h2>{text}</h2>;
                  });
                  let root = parse(b.innerHTML);
                  let text = root.innerText || root.firstChild.innerText;
                  let cite = <p className="cite">{text}</p>;

                  return (
                    <div>
                      <blockquote>
                        {quoteText}
                        {cite}
                      </blockquote>
                    </div>
                  );
                case "core/list":
                  const listItems = b.innerBlocks.map(
                    (block: any, i: number) => {
                      const root = parse(block.innerHTML);
                      const text = root.innerText || root.firstChild.innerText;
                      return <li key={i}>{text}</li>;
                    }
                  );
                  if (b.attrs.ordered) return <ol>{listItems}</ol>;
                  else if (b.attrs.unordered) return <ul>{listItems}</ul>;
                  else return <ul>{listItems}</ul>;
                case "core/buttons":
                  const buttons = b.innerBlocks.map((but: any, i: number) => {
                    const root = parse(but.innerHTML);
                    const text = root.innerText || root.firstChild.innerText;
                    let href = null;
                    const hrefMatches = but.innerHTML.match(/href="([^"]*)/);
                    if (hrefMatches)
                      href = hrefMatches[1].replace(API_URL, "/");
                    return {
                      text: text,
                      href: href,
                      html: but.root,
                      attrs: but.attrs,
                    };
                  });
                  return (
                    <Buttons
                      settings={settings}
                      buttons={buttons}
                      attrs={b.attrs}
                    />
                  );
                case "gravityforms/form":
                  return (
                    <GravityForm
                      settings={settings}
                      gfData={b.gfData}
                      gfDataInnerHtml={b.gfDataInnerHtml}
                      key={i}
                    />
                  );
                case "acf/hero":
                  return (
                    <>
                      {/* @ts-expect-error Server Component */}
                      <Hero settings={settings} data={b.attrs.data} key={i} />
                    </>
                  );
                case "acf/hero2":
                  return (
                    <>
                      {/* @ts-expect-error Server Component */}
                      <Hero2 settings={settings} data={b.attrs.data} key={i} />
                    </>
                  );
                case "acf/marketo":
                  return (
                    <>
                      <Marketo
                        settings={settings}
                        data={b.attrs.data}
                        key={i}
                      />
                    </>
                  );
                case "acf/logo-slider":
                  // @ts-expect-error Server Component
                  return <LogoSlider data={b.attrs.data} key={i} />;
                case "acf/pricing-summary":
                  return <PricingSummary data={b.attrs.data} key={i} />;
                case "acf/calendly":
                  return (
                    <CalendlyBook
                      settings={settings}
                      data={b.attrs.data}
                      key={i}
                    />
                  );
                case "acf/slider":
                  return (
                    <Slider
                      attrs={b.attrs}
                      data={b.attrs.data}
                      posts={allPosts[b.attrs.data.post_type]}
                      key={i}
                      settings={settings}
                    />
                  );
                case "acf/slider2":
                  return (
                    <Slider
                      attrs={b.attrs}
                      data={b.attrs.data}
                      posts={allPosts[b.attrs.data.post_type]}
                      key={i}
                      variant="2"
                      settings={settings}
                    />
                  );
                case "acf/slider3":
                  return (
                    <Slider
                      attrs={b.attrs}
                      data={b.attrs.data}
                      posts={allPosts[b.attrs.data.post_type]}
                      key={i}
                      variant="3"
                      settings={settings}
                    />
                  );
                case "acf/testimonials":
                  return (
                    <Testimonials
                      attrs={b.attrs}
                      data={b.attrs.data}
                      posts={allPosts[b.attrs.data.post_type]}
                      key={i}
                      settings={settings}
                    />
                  );
                case "acf/post-feed":
                  return (
                    <PostFeed
                      data={b.attrs.data}
                      posts={allPosts[b.attrs.data.post_type]}
                      key={i}
                      settings={settings}
                    />
                  );
                case "acf/post-feed2":
                  return (
                    <PostFeed
                      variant="2"
                      data={b.attrs.data}
                      posts={allPosts[b.attrs.data.post_type]}
                      key={i}
                      settings={settings}
                    />
                  );
                case "acf/post-feed3":
                  return (
                    <PostFeed
                      variant="3"
                      data={b.attrs.data}
                      posts={allPosts[b.attrs.data.post_type]}
                      key={i}
                      settings={settings}
                    />
                  );
                case "acf/post-feed4":
                  return (
                    <PostFeed
                      variant="4"
                      data={b.attrs.data}
                      posts={allPosts[b.attrs.data.post_type]}
                      key={i}
                      settings={settings}
                    />
                  );
                case "acf/toggle-menu":
                  // @ts-expect-error
                  return <ToggleMenu data={b.attrs.data} key={i} />;
                case "acf/map":
                  return (
                    <Map data={b.attrs.data}>
                      <></>
                    </Map>
                  );
                case "pb/accordion-item":
                  let title =
                    parse(b.innerHTML).innerText ||
                    parse(b.innerHTML).firstChild.innerText;
                  return (
                    <AccordionItem title={title} attrs={b.attrs} key={i}>
                      <BlockContent
                        settings={settings}
                        blockData={b.innerBlocks}
                        allPosts={allPosts}
                      />
                    </AccordionItem>
                  );
              }
            })()}
          </>
        );
      })}
    </>
  );
}
