import Link from "next/link";
import { useEffect, useRef, useState } from "react";
import Button from "ui/components/atoms/Button";

type Props = {
  data: {
    heading: string;
    subheading: string;
    stats: {
      technology_text: string;
      technology_image: string;
    };
  };
};

export function PricingSummary(props: Props) {
  return (
    <div className="relative mb-[50px] rounded-2xl bg-neutral-100 p-[60px]"></div>
  );
}
