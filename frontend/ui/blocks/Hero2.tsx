import { getAttachment } from "lib/api";
import Image from "next/image";
import { Container } from "ui/Container";
import Button from "ui/components/atoms/Button";
import CircleButton from "ui/components/atoms/CircleButton";

export type hero2Props = {
  data: {
    heading: string;
    testimonial: string;
    testimonial_author: string;
    img: string;
  };
  settings: any;
};

export async function Hero2({ data, settings }: hero2Props) {
  const imageObj = await getAttachment(data.img);
  return (
    <section className="hero2 relative md:h-[917px] h-auto overflow-hidden flex items-center bg-quinary py-20 md:py-0">
      <div className="hero2__image absolute md:w-[55%] w-full top-0 left-0 z-[1] h-full bg-quinary animation-fade">
        <Image
          src={imageObj.source_url}
          alt={data.heading}
          layout="fill"
          className="object-cover ellipses-mask"
        />
      </div>
      <div className="container max-w-[length:var(--container-md)] xl:px-[70px] lg:px-[50px] md:px-[40px] px-[20px] mx-auto w-full relative z-[4] flex justify-end animation-zoom-in-up">
        <div className="md:w-[40%] w-full relative ">
          <div className="relative z-[4]">
            <div
              className="text-secondary"
              dangerouslySetInnerHTML={{ __html: data.heading }}
            />
            <div className="text-black">
              <div dangerouslySetInnerHTML={{ __html: data.testimonial }} />
              <p>{data.testimonial_author}</p>
            </div>
          </div>
          {/* <div className="ellipse-bg w-[1810px] h-[2462px] bg-quinary absolute right-0 top-1/2 -translate-y-1/2 translate-x-[100px] rounded-[50%] z-[2]" /> */}
        </div>
      </div>
    </section>
  );
}
