"use client";

import React, { useCallback, useRef, useState } from "react";
import { Navigation, Pagination, Scrollbar, A11y, Autoplay } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.min.css";
import Image from "next/image";
import Button from "ui/components/atoms/Button";
import Pill from "ui/components/atoms/Pill";
import cn from "classnames";
import { ArticleListing } from "./PostFeed";
import CircleButton from "ui/components/atoms/CircleButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faImage } from "@fortawesome/free-solid-svg-icons";
import { makeRepeated } from "lib/utils";
import Link from "next/link";

type Props = {
  data: {
    number_of_slides_per_view: number;
    number_of_posts: number;
    posts: any;
  };
  posts: any;
  attrs: any;
  variant?: "1" | "2" | "3";
  settings?: any;
};

export function Slider(props: Props) {
  const [prevHidden, setPrevHidden] = useState(true);
  const [nextHidden, setNextHidden] = useState(false);

  const sliderRef = useRef<any>(null);

  let posts = props.posts;
  //if b.attrs.data.posts is set, filter the posts to only include those
  if (props.data.posts) {
    posts = props.posts.filter((p: any) => {
      return props.data.posts.includes(p.ID.toString());
    });
  }

  const handlePrev = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current.swiper.slidePrev();
  }, []);

  const handleNext = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current.swiper.slideNext();
  }, []);

  const onSlideChange = () => {
    if (!sliderRef.current) return;
    const slideIndex = sliderRef.current.swiper.realIndex;
    if (slideIndex === 0) {
      setPrevHidden(true);
      setNextHidden(false);
    } else if (slideIndex === sliderRef.current.swiper.slides.length - 1) {
      setPrevHidden(false);
      setNextHidden(true);
    } else {
      setPrevHidden(false);
      setNextHidden(false);
    }
  };
  // return JSON.stringify(props.posts)
  switch (props.variant) {
    default:
    case "1":
      return (
        <div
          className={cn(
            "slider w-[100%] max-w-[100%] max-h-[100vh] min-h-0 min-w-0 overflow-hidden rounded-t-xl",
            props.attrs.className && props.attrs.className
          )}
        >
          <Swiper
            onSlideChange={() => onSlideChange()}
            slidesPerView={props.data.number_of_slides_per_view}
            pagination={
              {
                // dynamicBullets: true,
              }
            }
            autoplay={{
              delay: 4500,
              disableOnInteraction: false,
            }}
            modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
            className="slider1 mySwiper"
            loop={true}
            ref={sliderRef}
          >
            {posts
              .slice(0, props.data.number_of_posts)
              .map((p: any, i: number) => {
                return (
                  <SwiperSlide key={i}>
                    <Link href={`/post/${p.post_name}`}>
                      <div
                        className="xl:h-[620px] lg:h-[500px] h-[400px] w-full relative bg-secondary overflow-hidden"
                        key={i}
                      >
                        {p.fimg_url ? (
                          <Image
                            alt={p.post_title}
                            src={p.fimg_url}
                            fill
                            className="object-cover"
                          />
                        ) : (
                          <div className="left-1/2 top-1/2 absolute text-[60px] text-primary -translate-y-1/2 -translate-x-1/2">
                            <FontAwesomeIcon icon={faImage} />
                          </div>
                        )}
                        <div className="left-1/2 top-1/2 absolute flex flex-col justify-end w-full h-full max-w-screen-xl transform -translate-x-1/2 -translate-y-1/2 pb-[87px] xl:px-[70px] px-[20px]">
                          <h1 className="max-w-[828px] text-white">
                            {p.post_title}
                          </h1>
                          <Pill variant="secondary" text="Article" />
                        </div>
                      </div>
                    </Link>
                  </SwiperSlide>
                );
              })}
          </Swiper>
        </div>
      );
    case "3":
      return (
        <div
          className={cn(
            "slider w-[calc(100%-40px)] md:w-full max-w-[1466px] max-h-[100vh] min-h-0 min-w-0 overflow-hidden md:mx-auto rounded-[20px] md:rounded-xl mx-[20px]",
            props.attrs.className && props.attrs.className
          )}
        >
          <Swiper
            onSlideChange={() => onSlideChange()}
            slidesPerView={props.data.number_of_slides_per_view}
            pagination={
              {
                // dynamicBullets: true,
              }
            }
            autoplay={{
              delay: 4500,
              disableOnInteraction: false,
            }}
            modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
            className="slider2 mySwiper"
            // loop={true}
            ref={sliderRef}
          >
            {posts
              .slice(0, props.data.number_of_posts)
              .map((p: any, i: number) => {
                return (
                  <SwiperSlide key={i}>
                    <Link href={`/post/${p.post_name}`}>
                      <div
                        className="xl:h-[675px] lg:h-[500px] h-[400px] w-full relative bg-secondary overflow-hidden"
                        key={i}
                      >
                        {p.fimg_url ? (
                          <Image
                            alt={p.post_title}
                            src={p.fimg_url}
                            fill
                            className="object-cover"
                          />
                        ) : (
                          <div className="left-1/2 top-1/2 absolute text-[60px] text-primary -translate-y-1/2 -translate-x-1/2">
                            <FontAwesomeIcon icon={faImage} />
                          </div>
                        )}
                        <div className="left-1/2 top-1/2 absolute flex flex-col justify-end w-full h-full max-w-screen-xl transform -translate-x-1/2 -translate-y-1/2 pb-[87px] xl:px-[70px] px-[20px]">
                          <h1 className="max-w-[828px] text-white">
                            {p.post_title}
                          </h1>
                          <Pill variant="secondary" text="Article" />
                        </div>
                      </div>
                    </Link>
                  </SwiperSlide>
                );
              })}
          </Swiper>
        </div>
      );
    case "2":
      return (
        <div
          className={cn(
            "slider relative w-full overflow-visible",
            props.attrs.className && props.attrs.className
          )}
        >
          <Swiper
            onSlideChange={() => onSlideChange()}
            slidesPerView={props.data.number_of_slides_per_view}
            pagination={{
              dynamicBullets: true,
            }}
            // navigation={true}
            // autoplay={{
            //   delay: 4500,
            //   disableOnInteraction: false,
            // }}
            breakpoints={{
              0: {
                slidesPerView: 1,
              },
              768: {
                slidesPerView: 2,
              },
              1000: {
                slidesPerView: props.data.number_of_slides_per_view,
              },
            }}
            spaceBetween={19}
            modules={[Navigation, Pagination, A11y, Autoplay]}
            className="slider3 mySwiper"
            loop={true}
            ref={sliderRef}
          >
            {posts
              .slice(0, props.data.number_of_posts)
              .map((p: any, i: number) => {
                return (
                  <SwiperSlide key={i}>
                    <div className="py-[calc(0.5*35px)]">
                      <ArticleListing post={p} />
                    </div>
                  </SwiperSlide>
                );
              })}
          </Swiper>
          {posts.slice(0, props.data.number_of_posts).length >
            props.data.number_of_slides_per_view && (
            <SliderNavigation
              prevHidden={prevHidden}
              nextHidden={nextHidden}
              handlePrev={handlePrev}
              handleNext={handleNext}
              settings={props.settings}
            />
          )}
        </div>
      );
  }
}

type SliderNavigationProps = {
  prevHidden: boolean;
  nextHidden: boolean;
  handlePrev: () => void;
  handleNext: () => void;
  settings: any;
};

function SliderNavigation({
  prevHidden,
  nextHidden,
  handlePrev,
  handleNext,
  settings,
}: SliderNavigationProps) {
  return (
    <div className="slider-nav absolute top-[130px] z-[98] w-full flex justify-between">
      <div
        className={cn(
          "transform rotate-180 lg:translate-x-[-50%] translate-x-[20px] transition-all duration-300 ease-in-out",
          prevHidden ? "scale-0" : "scale-1"
        )}
      >
        <CircleButton
          icon="arrowRight"
          onClick={() => handlePrev()}
          effect={settings.btn_transition}
        />
      </div>
      <div
        className={cn(
          "lg:translate-x-[50%] translate-x-[-20px]",
          nextHidden ? "scale-0" : "scale-1"
        )}
      >
        <CircleButton
          icon="arrowRight"
          onClick={() => handleNext()}
          effect={settings.btn_transition}
        />
      </div>
    </div>
  );
}
