import { getAttachment } from "lib/api";
import Image from "next/image";
import { Container } from "ui/Container";
import Button from "ui/components/atoms/Button";
import CircleButton from "ui/components/atoms/CircleButton";

export type heroProps = {
  data: {
    heading: string;
    subheading: string;
    cta_text: string;
    cta_link: string;
    after_cta_text: string;
    img: string;
  };
  settings: any;
};

export async function Hero({ data, settings }: heroProps) {
  const imageObj = await getAttachment(data.img);
  return (
    <section className="hero relative md:h-[1054px] md:pb-0 pb-[500px] h-auto overflow-hidden flex flex-col md:flex-row items-center animation-fade">
      <div className="container max-w-[length:var(--container-md)] xl:px-[70px] lg:px-[50px] md:px-[40px] px-[20px] mx-auto w-full relative z-[4] animation-zoom-in-left">
        <div className="md:w-[55%] w-full relative py-20 md:py-0">
          <div className="relative z-[4] text-white">
            <div dangerouslySetInnerHTML={{ __html: data.heading }} />
            <div dangerouslySetInnerHTML={{ __html: data.subheading }} />
            <div className="flex flex-col md:flex-row md:items-center items-start gap-4 md:mt-[246px] mt-[40px]">
              <Button
                href={data.cta_link}
                icon="chevronRight"
                text={data.cta_text}
                effect="grow"
              />
              <div dangerouslySetInnerHTML={{ __html: data.after_cta_text }} />
            </div>
          </div>
          <div className="ellipse-bg w-[1810px] h-[2462px] bg-secondary absolute md:right-0 md:top-1/2 md:-translate-y-1/2 md:translate-x-[100px] -bottom-[50px] right-1/2 translate-x-1/2 rounded-[50%] z-[2]" />
        </div>
      </div>

      <div className="hero__image absolute w-full top-0 left-0 z-[1] h-full bg-secondary">
        <Image
          src={imageObj.source_url}
          alt={data.heading}
          layout="fill"
          className="object-cover"
        />
      </div>
    </section>
  );
}
