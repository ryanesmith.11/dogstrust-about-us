"use client";

import React, { useEffect, useState } from "react";

export type marketoProps = {
  data: {
    marketo_base_url: string;
    marketo_munchkin_id: string;
    marketo_form_id: string;
  };
  settings: any;
};

export function Marketo({ data, settings }: marketoProps) {
  const [scriptLoaded, setScriptLoaded] = useState(false); // Add state to track script loading

  useEffect(() => {
    if (!scriptLoaded) {
      const script = document.createElement("script");
      script.src = data.marketo_base_url + "/js/forms2/js/forms2.min.js ";
      script.async = true;
      script.onload = () => {
        // @ts-ignore
        MktoForms2.loadForm(
          data.marketo_base_url,
          data.marketo_munchkin_id,
          data.marketo_form_id
        );
        setScriptLoaded(true); // Set the flag once script is loaded
      };
      document.body.appendChild(script);

      return () => {
        document.body.removeChild(script);
      };
    }
  }, [scriptLoaded]); // Only run the effect when scriptLoaded changes

  return <form id="mktoForm_3930"></form>;
}
