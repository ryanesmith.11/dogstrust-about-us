"use client";
import { InlineWidget } from "react-calendly";
import { useRef, useState, useEffect } from "react";
import Button from "ui/components/atoms/Button";
import cn from "classnames";

export type Props = {
  data: {
    heading: string;
    subheading: string;
  };
  settings: any;
};

export function CalendlyBook(props: Props) {
  const [open, setOpen] = useState(false);
  return (
    <>
      <p>dsffwef</p>
      <Button
        text="Book a slot"
        href="#"
        onClick={() => {
          setOpen(true);
        }}
        effect={props.settings.btn_transition}
      />
      <div className={cn(open ? "scale-1" : "scale-0")}>
        <Calendly selector="#main" />
      </div>
    </>
  );
}

export function Calendly(props: any) {
  const [mount, setMount] = useState(false);
  const ref = useRef();

  useEffect(() => {
    ref.current = document.querySelector(props.selector);
    setMount(true);
  }, [props.selector]);

  return mount ? (
    <InlineWidget url="https://calendly.com/ryanesmith-11/intro-call" />
  ) : // <PopupButton
  //   url="https://calendly.com/ryanesmith-11/intro-call"
  //   rootElement={document.querySelector(selector)}
  //   text="Click here to schedule!"
  //   styles={{
  //     height: "1000px",
  //     overflow: "hidden",
  //   }}
  //   pageSettings={{
  //     className: "overflow-hidden",
  //     backgroundColor: "ffffff",
  //     hideEventTypeDetails: false,
  //     hideLandingPageDetails: false,
  //     hideGdprBanner: true,
  //     primaryColor: "000",
  //     textColor: "4d5055",
  //   }}
  // />
  null;
}
