"use client";

import React, { useRef, useState } from "react";
import Image from "next/image";
import Button from "ui/components/atoms/Button";
import { getAllCategoriesFromPosts } from "lib/utils";
import cn from "classnames";
import Pill from "ui/components/atoms/Pill";
import CircleButton from "ui/components/atoms/CircleButton";
import { decode, decodeEntity } from "html-entities";
import Link from "next/link";
import ReactDOM from "react-dom";
import { Modal } from "ui/components/molecules/Modal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faImage } from "@fortawesome/free-solid-svg-icons";
import moment from "moment";

type Props = {
  data: {
    number_of_columns: string;
    number_of_posts: string;
    show_load_more: string;
    number_on_load: string;
    load_more_text: string;
    show_search: string;
    show_categories: string;
    posts: any;
  };
  posts: any;
  variant?: "1" | "2" | "3" | "4" | "5";
  settings: any;
};

export function PostFeed(props: Props) {
  const [numberPosts, setNumberPosts] = useState(
    parseInt(props.data.number_of_posts)
  );
  const [searchTerm, setSearchTerm] = useState("");
  const [catChosen, setCatChosen] = useState("All");

  let posts = props.posts;
  //if b.attrs.data.posts is set, filter the posts to only include those
  if (props.data.posts) {
    posts = props.posts.filter((p: any) => {
      return props.data.posts.includes(p.ID.toString());
    });
  }

  const cats = getAllCategoriesFromPosts(posts);

  const filteredPosts = posts
    .filter((post: any) => {
      return post.post_title.toLowerCase().includes(searchTerm.toLowerCase());
    })
    .filter((post: any) => {
      if (catChosen == "All") return post;
      return post.categoryNames.includes(catChosen);
    })
    .sort(
      (a: any, b: any) =>
        new Date(b.publish_date).valueOf() - new Date(a.publish_date).valueOf()
    );
  return (
    <div className="post-feed mb-[10px]">
      {(props.data.show_search == "1" || props.data.show_categories == "1") && (
        <div className="news-articles-top flex flex-col pb-[30px]">
          {props.data.show_search == "1" && (
            <PostsSearch
              searchTerm={searchTerm}
              setSearchTerm={setSearchTerm}
            />
          )}
          {props.data.show_categories == "1" && (
            <ArticleCategoryFilter
              cats={cats}
              catChosen={catChosen}
              setCatChosen={setCatChosen}
              settings={props.settings}
            />
          )}
        </div>
      )}
      <div
        className={cn(
          "grid w-full gap-x-[19px] gap-y-[19px] relative items-start",
          props.data.number_of_columns == "1" && "grid-cols-1",
          props.data.number_of_columns == "2" &&
            "grid-cols-1 md:grid-cols-2 lg:grid-cols-2",
          props.data.number_of_columns == "3" &&
            "grid-cols-1 md:grid-cols-2 lg:grid-cols-3",
          props.data.number_of_columns == "4" &&
            "grid-cols-1 md:grid-cols-2 lg:grid-cols-4",
          props.data.number_of_columns == "5" &&
            "grid-cols-1 md:grid-cols-2 lg:grid-cols-5",
          props.data.number_of_columns == "6" &&
            "grid-cols-1 md:grid-cols-2 lg:grid-cols-6"
        )}
      >
        {filteredPosts.slice(0, numberPosts).map((p: any, i: number) => {
          return (
            <ArticleListing
              settings={props.settings}
              post={p}
              key={i}
              variant={props.variant}
            />
          );
        })}
      </div>
      {numberPosts < filteredPosts.length && props.data.show_load_more == "1" && (
        <div className="w-full flex justify-center pt-[80px]">
          <Button
            text={
              props.data.load_more_text
                ? props.data.load_more_text
                : "Read more"
            }
            onClick={() => {
              setNumberPosts(numberPosts + parseInt(props.data.number_on_load));
            }}
            icon="chevronDown"
            effect={props.settings.btn_transition}
          />
        </div>
      )}
    </div>
  );
}
type ArticleListingProps = {
  post: any;
  variant?: "1" | "2" | "3" | "4" | "5";
  settings?: any;
};
export function ArticleListing({
  post,
  variant,
  settings,
}: ArticleListingProps) {
  switch (variant) {
    case "1":
    default:
      return (
        <div className="flex flex-col justify-between h-full">
          <div>
            <div className="relative h-[302px] rounded-md overflow-hidden bg-secondary">
              <Link href={`/post/${post.post_name}`}>
                {post.acfData.thumbnail_image || post.fimg_url ? (
                  <Image
                    alt={post.post_title}
                    src={
                      post.acfData.thumbnail_image
                        ? post.acfData.thumbnail_image.url
                        : post.fimg_url
                    }
                    fill
                    className="object-cover"
                  />
                ) : (
                  <div className="left-1/2 top-1/2 absolute text-[60px] text-primary -translate-y-1/2 -translate-x-1/2">
                    <FontAwesomeIcon icon={faImage} />
                  </div>
                )}
                <h3 className="absolute top-0 left-0 m-[20px] text-white max-w-[386px] bg-primary  px-[20px] py-[10px]">
                  {post.post_title}
                </h3>
                <div className="absolute bottom-0 right-0 m-[20px]">
                  <Pill
                    text={post.acfData.breed ? post.acfData.breed : "Breed"}
                  />
                </div>
              </Link>
            </div>
          </div>
        </div>
      );
    case "2":
      return (
        <div className="md:flex-row flex flex-col items-center h-full mb-[40px] md:mb-0">
          <div className="relative lg:h-[249px] h-[160px] md:w-[317px] w-full rounded-md overflow-hidden shadow-[0_4px_4px_rgba(0,0,0,0.25)] bg-[#F6F7F9] bg-opacity-30 flex items-center justify-center">
            <div className="relative w-[285px] h-[160px]">
              <Link href={`/post/${post.post_name}`}>
                {post.fimg_url ? (
                  <Image
                    alt={post.post_title}
                    src={post.fimg_url}
                    fill
                    className="object-contain"
                  />
                ) : (
                  <div className="left-1/2 top-1/2 absolute text-[60px] text-primary -translate-y-1/2 -translate-x-1/2">
                    <FontAwesomeIcon icon={faImage} />
                  </div>
                )}
              </Link>
            </div>
          </div>
          <div className="md:w-[715px] w-full md:ml-[85px] ml-[40px] md:mr-[80px] mr-[40px] pt-[30px] md:pt-0">
            <h3 className="">{decode(post.post_title)}</h3>
            <p
              className="b1"
              dangerouslySetInnerHTML={{ __html: decode(post.post_content) }}
            />
          </div>
          <div className="md:justify-center flex justify-end">
            <CircleButton icon="arrowRight" effect={settings.btn_transition} />
          </div>
        </div>
      );
    case "3":
      return (
        <div className="flex md:flex-row w-full flex-col items-center h-full max-w-[1040px] mx-auto mb-[30px]">
          <div className="relative md:w-[461px] md:h-[409px] h-[242px] w-full rounded-md overflow-hidden shrink-0  bg-secondary">
            {post.fimg_url ? (
              <Image
                alt={post.post_title}
                src={post.fimg_url}
                fill
                className="object-cover"
              />
            ) : (
              <div className="left-1/2 top-1/2 absolute text-[60px] text-primary -translate-y-1/2 -translate-x-1/2">
                <FontAwesomeIcon icon={faImage} />
              </div>
            )}
            <div className="left-[20px] top-[20px] absolute">
              <Pill
                variant="secondary"
                text={`${moment(post.acfData.event_time, "LT").format(
                  "LT"
                )}  |  ${moment(post.acfData.event_date, "DD/MM/YYYY").format(
                  "DD MMM YYYY"
                )}`}
              />
            </div>
          </div>
          <div className="md:w-[715px] w-full mt-[30px] md:mt-0 flex flex-col md:ml-[68px] md-[40px]">
            {post.categoryNames && (
              <div className="mb-[28px]">
                <Pill text={post.categoryNames[0]} variant="secondary" />
              </div>
            )}
            <h3 className="">{post.post_title}</h3>
            <div className="border-b-[1px] border-solid border-tertiary b1">
              <div
                className="mb-[28px]"
                style={{
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                  display: "-webkit-box",
                  lineClamp: "3",
                  WebkitLineClamp: "3",
                  WebkitBoxOrient: "vertical",
                }}
                dangerouslySetInnerHTML={{ __html: post.post_content }}
              />
            </div>
            <div className="flex my-[20px] b1">
              <div className="w-1/2">{post.acfData.event_location}</div>
              <div className="flex justify-end w-1/2">
                {`${moment(post.acfData.event_time, "LT").format(
                  "LT"
                )}  |  ${moment(post.acfData.event_date, "DD/MM/YYYY").format(
                  "DD MMM YYYY"
                )}`}
              </div>
            </div>
            <div className="b1 flex">
              <div className="w-1/3 text-[18px] font-bold mt-[14px]">
                {post.acfData.event_cost}
              </div>
              <div className="flex justify-end w-2/3">
                <Button
                  text="View event details"
                  icon="externalLink"
                  effect={settings.btn_transition}
                />
              </div>
            </div>
          </div>
        </div>
      );
    case "4":
      const [open, setOpen] = useState(false);
      const pillText = post.acfData.job_title
        ? decode(post.acfData.job_title)
        : post.categoryNames
        ? decode(post.categoryNames[0])
        : null;
      return (
        <div className="relative flex flex-col items-start">
          <div
            className="group relative w-full h-full overflow-hidden rounded-md cursor-pointer"
            onClick={() => {
              setOpen(true);
            }}
          >
            <div className="relative h-[431px] w-full overflow-hidden shrink-0 bg-transparent">
              {post.fimg_url ? (
                <Image
                  alt={post.post_title}
                  src={post.fimg_url}
                  fill
                  className="object-cover"
                />
              ) : (
                <div className="relative w-full h-full bg-secondary">
                  <div className="left-1/2 top-1/2 absolute text-[60px] text-primary -translate-y-1/2 -translate-x-1/2">
                    <FontAwesomeIcon icon={faImage} />
                  </div>
                </div>
              )}
            </div>
            <div className="right-[17px] bottom-[17px] absolute">
              {pillText && <Pill variant="secondary" text={pillText} />}
            </div>
            <div className="bg-primary opacity-0 group-hover:opacity-20 z-[30] mix-blend-[soft-light] duration-300 pointer-events-none w-full h-full transition-all absolute top-0 left-0" />
            <div className="group-hover:scale-[1] absolute top-[18px] right-[18px] transform scale-[0] duration-300">
              <CircleButton
                icon="arrowRight"
                effect={settings.btn_transition}
              />
            </div>
            <h3 className="mb-[20px] ml-[20px] absolute bottom-0 left-0 bg-primary px-[10px] py-[5px]">
              {decode(post.post_title)}
            </h3>
          </div>
          <Modal open={open} setOpen={setOpen}>
            <>
              <div className="flex flex-col md:flex-row gap-[20px] items-center justify-between w-full mb-[27px]">
                <h3 className="flex-shrink-0 mb-0">{post.post_title}</h3>
                {pillText && <Pill variant="secondary" text={pillText} />}
              </div>
              <div
                dangerouslySetInnerHTML={{
                  __html: decodeEntity(post.post_content),
                }}
              />
              {post.acfData.associated_link && (
                <Button
                  variant="secondary"
                  text={post.acfData.associated_link.title}
                  icon="externalLink"
                  href={post.acfData.associated_link.url}
                  effect={settings.btn_transition}
                  target="_blank"
                />
              )}
            </>
          </Modal>
        </div>
      );
  }
}
type PostsSearchProps = {
  searchTerm: string;
  setSearchTerm: any;
};

export function PostsSearch({ searchTerm, setSearchTerm }: PostsSearchProps) {
  return (
    <div className="md:w-4/12 w-full">
      <input
        className="w-full max-w-[475px] h-[34px] px-[20px] py-[27px] border-[1px] border-solid border-secondary rounded-full"
        placeholder="Search"
        onInput={(e) => {
          setSearchTerm((e.target as HTMLInputElement).value);
        }}
      />
    </div>
  );
}

type ArticleCategoryFilterProps = {
  cats: any;
  catChosen: string;
  setCatChosen: any;
  settings: any;
};
export function ArticleCategoryFilter({
  cats,
  catChosen,
  setCatChosen,
  settings,
}: ArticleCategoryFilterProps) {
  if (!cats.includes("All")) cats.unshift("All"); //add all cat to start of array
  return (
    <ul className="article-category-filter md:w-full w-[calc(100%+40px)] px-[20px] ml-[-20px] flex gap-[18px] justify-start overflow-x-auto py-[20px] text-[13px] md:text-inherit">
      {cats.map((cat: string, i: number) => {
        return (
          <Button
            text={decode(cat)}
            key={i + 1}
            data-choice={decode(cat)}
            onClick={() => {
              setCatChosen(cat);
            }}
            variant="secondary"
            active={catChosen == cat}
            effect={settings.btn_transition}
          />
        );
      })}
    </ul>
  );
}
