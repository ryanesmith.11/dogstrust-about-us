"use client";

import React, { useCallback, useRef, useState } from "react";
import { Navigation, Pagination, Scrollbar, A11y, Autoplay } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.min.css";
import Image from "next/image";
import Button from "ui/components/atoms/Button";
import Pill from "ui/components/atoms/Pill";
import cn from "classnames";
import { ArticleListing } from "./PostFeed";
import CircleButton from "ui/components/atoms/CircleButton";

type Props = {
  data: {
    number_of_slides_per_view: number;
    number_of_posts: number;
    posts: any;
  };
  posts: any;
  attrs: any;
  variant?: "1";
  settings?: any;
};

export function Testimonials(props: Props) {
  const [prevHidden, setPrevHidden] = useState(true);
  const [nextHidden, setNextHidden] = useState(false);

  const sliderRef = useRef<any>(null);

  const handlePrev = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current.swiper.slidePrev();
  }, []);

  const handleNext = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current.swiper.slideNext();
  }, []);

  const onSlideChange = () => {
    if (!sliderRef.current) return;
    const slideIndex = sliderRef.current.swiper.realIndex;
    if (slideIndex === 0) setPrevHidden(true);
    else if (slideIndex === sliderRef.current.swiper.slides.length - 1)
      setNextHidden(true);
    else {
      setPrevHidden(false);
      setNextHidden(false);
    }
  };

  switch (props.variant) {
    default:
    case "1":
      return (
        <div
          className={cn(
            "testimonials slider relative w-full overflow-visible",
            props.attrs.className && props.attrs.className
          )}
        >
          <Swiper
            onSlideChange={() => onSlideChange()}
            slidesPerView={props.data.number_of_slides_per_view}
            pagination={
              {
                // dynamicBullets: true,
              }
            }
            // navigation={true}
            // autoplay={{
            //   delay: 4500,
            //   disableOnInteraction: false,
            // }}
            breakpoints={{
              0: {
                slidesPerView: 1,
              },
              768: {
                slidesPerView: 2,
              },
              1000: {
                slidesPerView: props.data.number_of_slides_per_view,
              },
            }}
            spaceBetween={19}
            modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
            className="slider3 mySwiper"
            loop={true}
            ref={sliderRef}
          >
            {props.posts
              .slice(0, props.data.number_of_posts)
              .map((p: any, i: number) => {
                return (
                  <SwiperSlide key={i}>
                    <div className="testimonial py-[77px] px-[48px] flex flex-col gap-[42px] bg-tertiary">
                      <p className="b1">{p.acfData.quote}</p>
                      <div className="quotee-content flex gap-[20px]">
                        <div className="quotee-image w-[63px] h-[63px] object-cover relative">
                          <Image
                            src={p.fimg_url}
                            alt={p.acfData.quotee_name}
                            fill
                            className="shrink-0 grow-0 object-cover"
                          />
                        </div>
                        <div className="quotee-details flex flex-col">
                          <h3 className="mb-[5px]">{p.acfData.quotee_name}</h3>
                          <p className="b1">{p.acfData.quotee_position}</p>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                );
              })}
          </Swiper>
          <SliderNavigation
            prevHidden={prevHidden}
            nextHidden={nextHidden}
            handlePrev={handlePrev}
            handleNext={handleNext}
            settings={props.settings}
          />
        </div>
      );
  }
}

type SliderNavigationProps = {
  prevHidden: boolean;
  nextHidden: boolean;
  handlePrev: () => void;
  handleNext: () => void;
  settings?: any;
};

function SliderNavigation({
  prevHidden,
  nextHidden,
  handlePrev,
  handleNext,
  settings,
}: SliderNavigationProps) {
  return (
    <div className="slider-nav absolute top-1/2 -translate-y-1/2 z-[98] w-full flex justify-between">
      <div
        className={cn(
          "transform rotate-180 lg:translate-x-[-50%] translate-x-[20px] transition-all duration-300 ease-in-out",
          prevHidden ? "scale-0" : "scale-1"
        )}
      >
        <CircleButton
          icon="arrowRight"
          onClick={() => handlePrev()}
          effect={settings.btn_transition}
        />
      </div>
      <div
        className={cn(
          "lg:translate-x-[50%] translate-x-[-20px]",
          nextHidden ? "scale-0" : "scale-1"
        )}
      >
        <CircleButton
          icon="arrowRight"
          onClick={() => handleNext()}
          effect={settings.btn_transition}
        />
      </div>
    </div>
  );
}
