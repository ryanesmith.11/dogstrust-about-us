import { AccordionElement } from "ui/components/molecules/AccordionElement";

type AccordionItemProps = {
  title: string;
  attrs: any;
  children: any;
};

export function AccordionItem({ title, attrs, children }: AccordionItemProps) {
  return (
    <AccordionElement title={title} attrs={attrs}>
      {children}
    </AccordionElement>
  );
}
