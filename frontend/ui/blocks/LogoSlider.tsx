"use client";

import { getAttachment } from "lib/api";
import { getRepeaterData, makeRepeated } from "lib/utils";
import Image from "next/image";
import {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Autoplay,
  FreeMode,
} from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

export type logoSliderProps = {
  data: {
    logos: any;
  };
};

export async function LogoSlider({ data }: logoSliderProps) {
  const logosData = getRepeaterData(data, "logos");
  const attachments = await Promise.all(
    logosData.map(async (logo: any) => {
      const img = await getAttachment(logo.logo);
      return img;
    })
  );

  return (
    <div className="logo-slider w-full pt-[40px]">
      <Swiper
        modules={[FreeMode, Pagination, Autoplay]}
        freeMode={{
          enabled: true,
          momentum: false,
        }}
        spaceBetween={60}
        grabCursor={true}
        loop={true}
        // autoplay={{
        //   delay: 0,
        //   disableOnInteraction: true,
        // }}
        slidesPerView={"auto"}
        speed={1000}
      >
        {makeRepeated(attachments, 3).map((img: any, index) => {
          return (
            <SwiperSlide key={index}>
              <Image
                key={index}
                src={img.source_url}
                width={img.dimensions[0]}
                height={img.dimensions[1]}
                alt="logo"
              />
            </SwiperSlide>
          );
        })}
      </Swiper>
    </div>
  );
}
