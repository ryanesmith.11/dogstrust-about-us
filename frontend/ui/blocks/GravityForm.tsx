"use client";

import Link from "next/link";
import Button from "ui/components/atoms/Button";
import { useForm } from "react-hook-form";
import { useState } from "react";
const API_URL = process.env.NEXT_PUBLIC_WORDPRESS_API_URL;

export type GravityFormProps = {
  gfData: any;
  gfDataInnerHtml: any;
  settings: any;
};

export function GravityForm({
  gfData,
  gfDataInnerHtml,
  settings,
}: GravityFormProps) {
  const [confirmationMessage, setConfirmationMessage] = useState(null);

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm();

  const onSubmit = async (data: any) => {
    try {
      const response = await fetch(
        `${API_URL}wp-json/gf/v2/forms/${gfData.id}/submissions`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        }
      );
      const res = await response.json();
      setConfirmationMessage(res.confirmation_message);
      reset();
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className="bg-primary p-[53px] rounded-md">
      {confirmationMessage ? (
        <div dangerouslySetInnerHTML={{ __html: confirmationMessage }} />
      ) : (
        <form onSubmit={handleSubmit(onSubmit)} className="flex flex-col">
          {gfData.fields.map((field: any, i: number) => {
            return (
              <div className="mb-[33px] flex flex-col" key={i}>
                {field.labelPlacement !== "hidden_label" && (
                  <label htmlFor={field.label} className="b2 font-secondary">
                    {field.label}
                  </label>
                )}
                {(() => {
                  switch (field.type) {
                    case "text":
                      return (
                        <>
                          <input
                            type="text"
                            // defaultValue={intialValues.firstName}
                            placeholder={field.placeholder}
                            {...register("input_" + field.id, {
                              // validate: (value) => value !== "bill",
                            })}
                          />
                          {/* {errors.firstName && <p>Your name is not bill</p>} */}
                        </>
                      );
                    case "email":
                      return (
                        <>
                          <input
                            type="email"
                            // name={field.label}
                            // defaultValue={intialValues.firstName}
                            placeholder={field.placeholder}
                            {...register("input_" + field.id, {
                              // validate: (value) => value !== "bill",
                            })}
                          />
                          {/* {errors.firstName && <p>Your name is not bill</p>} */}
                        </>
                      );
                    case "textarea":
                      return (
                        <>
                          <textarea
                            className="min-h-[200px]"
                            {...register("input_" + field.id, {
                              // validate: (value) => value !== "bill",
                            })}
                            placeholder={field.placeholder}
                          ></textarea>

                          {/* {errors.firstName && <p>Your name is not bill</p>} */}
                        </>
                      );
                  }
                })()}
              </div>
            );
          })}
          <Button
            href="#"
            text="Submit"
            variant="primary"
            onClick={(e?: Event) => {
              e?.preventDefault();
              handleSubmit(onSubmit)();
            }}
            effect={settings.btn_transition}
          />
        </form>
      )}
    </div>
  );
}
