import { getPage } from "lib/api";
import Image from "next/image";
import Button from "ui/components/atoms/Button";
import {
  ToggleMenuComponent,
  toggleMenuComponentProps,
} from "ui/components/molecules/ToggleMenuComponent";

const API_URL = process.env.NEXT_PUBLIC_WORDPRESS_API_URL;

type Props = {
  data: {
    pages: { title: string; link?: string }[];
  };
};

export async function ToggleMenu(props: Props) {
  const items = await Promise.all(
    props.data.pages.map(async (page: any) => {
      const item = await getPage(page);
      if (!item)
        return {
          title: "",
          link: "",
        };
      return {
        title: item.title.rendered,
        link: item.link ? item.link.replace(API_URL, "/") : "#",
      };
    })
  );
  return <ToggleMenuComponent items={items} />;
}
