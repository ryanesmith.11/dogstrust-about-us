"use client";
import type { MapOptions } from "leaflet";
import type { FC, ReactNode } from "react";
// import type { ComponentChildren, FunctionalComponent } from "preact";
import { MapContainer, TileLayer } from "react-leaflet";
import "leaflet/dist/leaflet.css";

const Map: FC<
  {
    markers: any;
    center: [number, number];
    zoom: number;
  } & MapOptions
> = ({ markers, ...options }) => {
  return (
    <MapContainer
      className="aspect-square relative w-full"
      maxZoom={18}
      center={[12, 12]}
      zoom={9}
      // {...options}
      attributionControl={false}
    >
      <TileLayer url="https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png" />
    </MapContainer>
  );
};

export default Map;
