import React from "react";
import { Header1 } from "./components/molecules/headers/Header1";
import { getAllMenus, getSettings, getSettingsByPostId } from "lib/api";
import { useContext } from "react";

type HeaderComponents = {
  [key: string]: React.ReactNode;
};

export async function Header({ settings }: { settings: any }) {
  const menuItems = await getAllMenus();
  console.log("settings", settings.header_type);
  const headerComponents: HeaderComponents = {
    "1": <Header1 settings={settings} menuItems={menuItems} />,
  };

  return <>{headerComponents[settings.header_type]}</>;
}
