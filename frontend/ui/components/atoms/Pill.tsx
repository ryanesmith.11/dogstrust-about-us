import cn from "classnames";

type pillProps = {
  text: string;
  onClick?: () => void;
  variant?: "primary" | "secondary" | "tertiary" | "quaternary";
};

export default function Pill({ text, onClick, variant }: pillProps) {
  return (
    <span
      className={cn(
        "pill relative w-fit inline-flex items-center justify-center px-[45px] py-[7px] text-center rounded-[var(--pill-border-radius)]",
        (!variant || variant === "primary") &&
          "bg-[color:var(--pill1-bg-color)] text-[color:var(--pill1-text-color)]",
        variant === "secondary" &&
          "bg-[color:var(--pill2-bg-color)] text-[color:var(--pill2-text-color)]"
      )}
      onClick={onClick}
    >
      {text}
    </span>
  );
}
