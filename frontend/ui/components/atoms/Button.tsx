import cn from "classnames";
import { getSettings } from "lib/api";
import Link from "next/link";
import Icon from "./Icon";
type buttonProps = {
  text: string;
  href?: string;
  onClick?: () => void;
  onMouseOver?: () => void;
  variant?: "primary" | "secondary";
  effect?: "none" | "fade" | "grow" | "expandRight";
  icon?: "chevronDown" | "chevronRight" | "externalLink";
  target?: "_self" | "_blank";
  active?: boolean;
};

export default function Button({
  text,
  href,
  onClick,
  variant = "primary",
  effect,
  icon,
  active,
  target = "_self",
  onMouseOver,
}: buttonProps) {
  if (!effect) effect = "fade";
  const innerButton = (
    <span
      className={cn(
        "button",
        variant ? variant : "primary",
        effect ? "effect-" + effect : "effect-none"
      )}
      onClick={onClick}
      onMouseOver={onMouseOver}
    >
      <span className="relative">{text}</span>
      <Icon variant={icon} />
    </span>
  );

  if (href)
    return (
      <Link href={href} target={target}>
        {innerButton}
      </Link>
    );
  else return innerButton;
}
