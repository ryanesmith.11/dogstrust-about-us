import cn from "classnames";
import { getSettings } from "lib/api";
import Link from "next/link";

type circleButtonProps = {
  href?: string;
  onClick?: () => void;
  variant?: "primary" | "secondary" | "tertiary" | "quaternary";
  icon?:
    | "chevronDown"
    | "chevronRight"
    | "plus"
    | "minus"
    | "arrowDown"
    | "arrowRight"
    | "arrowLeft"
    | "arrowUp";
  effect?: "none" | "fade" | "expandRight";
};

export default function CircleButton({
  href,
  onClick,
  variant,
  effect,
  icon,
}: circleButtonProps) {
  if (!effect) effect = "fade";

  const innerButton = (
    <span
      className={cn(
        "button circle-button",
        variant ? variant : "primary",
        effect ? "effect-" + effect : "effect-none"
      )}
      onClick={onClick}
    >
      <div className="relative z-[10] w-[20px]">
        {icon === "chevronDown" && (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-5 h-5 ml-1 rotate-90 fill-current"
            width="100%"
            height={20}
            viewBox="0 0 20 20"
          >
            <path d="M7.833 15 6.583 13.75 10.333 10 6.583 6.25 7.833 5 12.833 10Z"></path>
          </svg>
        )}
        {icon === "chevronRight" && (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-5 h-5 ml-1 fill-current"
            width="100%"
            height={20}
            viewBox="0 0 20 20"
          >
            <path d="M7.833 15 6.583 13.75 10.333 10 6.583 6.25 7.833 5 12.833 10Z"></path>
          </svg>
        )}
        {icon === "arrowDown" && (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="100%"
            height="38"
            viewBox="0 0 27 38"
            fill="none"
          >
            <path
              d="M13.5 -5.46392e-07L13.5 36M13.5 36L26 21.9512M13.5 36L1 21.9512"
              stroke="currentColor"
              strokeWidth="2"
            />
          </svg>
        )}
        {icon === "arrowRight" && (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="100%"
            height="38"
            viewBox="0 0 27 38"
            fill="none"
            className="-rotate-90"
          >
            <path
              d="M13.5 -5.46392e-07L13.5 36M13.5 36L26 21.9512M13.5 36L1 21.9512"
              stroke="currentColor"
              strokeWidth="2"
            />
          </svg>
        )}
      </div>
    </span>
  );

  if (href) return <Link href={href}>{innerButton}</Link>;
  else return innerButton;
}
