import cn from "classnames";
import Image from "next/image";
import Link from "next/link";

export type LanguageDropdownProps = {
  text: string;
  links: {
    icon: { url: string };
    link: { url: string; title: string; target: string };
  }[];
};

export function LanguageDropdown({ text, links }: LanguageDropdownProps) {
  return (
    <div
      className={cn(
        "lang-dropdown bg-primary relative max-w-screen-3xl relative h-[39px]  top-0 left-0 mx-auto md:flex hidden justify-end items-center px-[20px] xl:px-[70px] gap-[10px]"
      )}
    >
      <span>{text}</span>
      <div className="flex gap-[10px] self-baseline items-center justify-center h-full">
        <div
          className={cn(
            "cursor-pointer flex flex-row items-center justify-between select-none gap-[10px]"
          )}
        >
          {links.map((link, i) => (
            <Link
              href={link.link.url}
              key={i}
              target={link.link.target}
              className={cn(
                "flex items-center justify-center gap-[3px] px-[9px] py-[3px]  h-[26px] rounded-sm bg-white"
              )}
            >
              <Image
                src={link.icon.url}
                width={20}
                height={10}
                alt={link.link.title + " website"}
              />
              <span>{link.link.title}</span>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
}
