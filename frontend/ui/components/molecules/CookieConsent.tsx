"use client";

import Link from "next/link";
import Cookies from "js-cookie";
import { MouseEvent, useCallback, useEffect, useState } from "react";
import Button from "../atoms/Button";
import Image from "next/image";

const USER_CONSENT_COOKIE_KEY = "ngg_cookie_consent_is_true";
const USER_CONSENT_COOKIE_EXPIRE_DATE =
  new Date().getTime() + 365 * 24 * 60 * 60;

const CookieConsent = ({ settings }: any) => {
  const [cookieConsentIsTrue, setCookieConsentIsTrue] = useState(true);

  useEffect(() => {
    const consentIsTrue = Cookies.get(USER_CONSENT_COOKIE_KEY) === "true";
    setCookieConsentIsTrue(consentIsTrue);
  }, []);

  const onClick = () => {
    if (!cookieConsentIsTrue) {
      Cookies.set(USER_CONSENT_COOKIE_KEY, "true", {
        expires: USER_CONSENT_COOKIE_EXPIRE_DATE,
      });
      setCookieConsentIsTrue(true);
    }
  };

  if (cookieConsentIsTrue) {
    return null;
  }

  return (
    <div
      className="bg-tertiary lg:rounded-lg rounded-t-md fixed lg:bottom-[44px] lg:right-[114px] bottom-0 right-0 flex flex-col items-center justify-center lg:w-[512px] w-full px-[61px] py-[41px] z-[101]"
      style={{ backgroundColor: settings.cookie_notice_bg_color }}
    >
      {settings.cookie_notice_icon ? (
        <Image
          src={settings.cookie_notice_icon.url}
          alt="Cookie notice icon"
          width={settings.cookie_notice_icon.width}
          height={settings.cookie_notice_icon.height}
        />
      ) : (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="47"
          height="47"
          viewBox="0 0 47 47"
          fill="none"
        >
          <rect width="47" height="47" rx="13" fill="var(--color-primary)" />
          <g clipPath="url(#clip0_518_4448)">
            <path
              d="M35.6199 37H11.3792C11.335 36.9856 11.2917 36.9648 11.2465 36.9576C10.5759 36.8583 10.0353 36.5369 9.60022 36.0195C9.25633 35.6115 9.10109 35.1294 9 34.6211C9 26.541 9 18.4599 9 10.3798C9.01354 10.3365 9.0352 10.2949 9.04062 10.2507C9.21933 8.96691 10.3331 8 11.7141 8C19.5674 8.00181 27.4208 7.99639 35.2742 8.01264C35.6849 8.01264 36.128 8.11195 36.499 8.288C37.5424 8.78183 37.9982 9.66929 37.9982 10.805C37.9982 17.8 37.9982 24.7949 37.9982 31.789C37.9982 32.6196 38.0036 33.4502 37.9964 34.2807C37.9892 35.1267 37.685 35.8381 37.0279 36.3924C36.6163 36.7391 36.1317 36.8962 35.6199 36.9991V37ZM10.3277 14.6058C10.3223 14.7142 10.3142 14.8063 10.3142 14.8984C10.3142 21.3263 10.3133 27.7552 10.3142 34.1832C10.3142 35.1565 10.8476 35.6855 11.826 35.6855C19.6045 35.6855 27.382 35.6855 35.1605 35.6855C36.1569 35.6855 36.6849 35.1601 36.6849 34.1679C36.6849 27.7489 36.6849 21.33 36.6849 14.911C36.6849 14.8117 36.6759 14.7115 36.6714 14.6058H10.3277ZM36.6524 13.2534C36.6524 12.2956 36.6813 11.3539 36.6407 10.415C36.619 9.9275 36.2914 9.58082 35.8302 9.41921C35.6045 9.34067 35.35 9.3181 35.1081 9.3181C27.3685 9.31268 19.6288 9.31359 11.8892 9.31449C11.7854 9.31449 11.6807 9.31268 11.5778 9.32261C10.8485 9.39664 10.3593 9.85166 10.3286 10.5829C10.2916 11.4587 10.3214 12.3371 10.325 13.2146C10.325 13.2282 10.3485 13.2417 10.3593 13.2534H36.6524Z"
              fill="white"
            />
            <path
              d="M30.0859 23.4545C29.9604 23.5719 29.8846 23.6378 29.8142 23.7082C27.704 25.8172 25.5946 27.9261 23.4853 30.0351C22.9889 30.5307 22.6892 30.5316 22.1955 30.0387C19.6655 27.509 17.1356 24.9794 14.6057 22.4497C14.3927 22.2366 14.213 22.0136 14.306 21.6796C14.427 21.2481 14.8909 21.0576 15.27 21.2968C15.3801 21.3663 15.4776 21.4593 15.5705 21.5514C17.9082 23.8861 20.2441 26.2225 22.5782 28.5608C22.6576 28.6403 22.7145 28.7423 22.7578 28.801C24.887 26.6721 26.9702 24.5885 29.0578 22.5012C28.9874 22.5012 28.8972 22.5012 28.8069 22.5012C27.731 22.5012 26.6542 22.5012 25.5784 22.5012C25.2715 22.5012 25.0152 22.4 24.8843 22.1084C24.7823 21.88 24.7823 21.6236 24.9727 21.4521C25.1153 21.3239 25.3265 21.202 25.508 21.2002C27.2445 21.1813 28.982 21.1849 30.7186 21.193C31.1184 21.1948 31.3378 21.4142 31.3964 21.8132C31.4091 21.8963 31.4091 21.982 31.4091 22.0669C31.4091 23.6721 31.41 25.2764 31.4091 26.8816C31.4091 27.4485 31.178 27.76 30.7565 27.7681C30.326 27.7763 30.0841 27.4558 30.0832 26.8744C30.0832 25.8551 30.0832 24.8349 30.0832 23.8156C30.0832 23.7163 30.0832 23.6179 30.0832 23.4563L30.0859 23.4545Z"
              fill="white"
            />
            <path
              d="M14.9268 10.6477C15.2869 10.6458 15.573 10.9266 15.5784 11.2859C15.5839 11.6489 15.2761 11.9603 14.9205 11.9522C14.5784 11.9441 14.2733 11.6326 14.276 11.2932C14.2787 10.9447 14.5757 10.6495 14.9268 10.6477Z"
              fill="white"
            />
            <path
              d="M16.9201 11.3008C16.9182 10.9405 17.1971 10.6553 17.5582 10.6489C17.9201 10.6435 18.2315 10.9514 18.2234 11.3071C18.2153 11.6501 17.9039 11.9544 17.5645 11.9517C17.2125 11.949 16.9219 11.6547 16.9201 11.3008Z"
              fill="white"
            />
            <path
              d="M11.6474 11.3066C11.6429 10.9455 11.92 10.6584 12.2801 10.6502C12.6438 10.6421 12.9552 10.9446 12.9507 11.303C12.9471 11.6442 12.6384 11.9512 12.2972 11.953C11.9488 11.9548 11.651 11.6596 11.6465 11.3075L11.6474 11.3066Z"
              fill="white"
            />
          </g>
          <defs>
            <clipPath id="clip0_518_4448">
              <rect
                width="29"
                height="29"
                fill="white"
                transform="translate(9 8)"
              />
            </clipPath>
          </defs>
        </svg>
      )}
      <h3 className="mt-[17px] mb-[23px]">{settings.cookie_notice_heading}</h3>
      <div
        className="mb-[23px]"
        dangerouslySetInnerHTML={{ __html: settings.cookie_notice_text }}
      />
      <div className="flex gap-[23px]">
        <Button
          text={
            settings.cookie_notice_accept_text
              ? settings.cookie_notice_accept_text
              : "Accept cookies"
          }
          onClick={() => {
            onClick();
          }}
          effect={settings.btn_transition}
        />
        <Button
          text={
            settings.cookie_notice_reject_text
              ? settings.cookie_notice_reject_text
              : "Reject cookies"
          }
          onClick={() => {
            onClick();
          }}
          variant="secondary"
          effect={settings.btn_transition}
        />
      </div>
    </div>
  );
};

export default CookieConsent;
