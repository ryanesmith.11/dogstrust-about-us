"use client";

import { useState } from "react";
import { faMinus, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import cn from "classnames";

export const AccordionElement = (props: any) => {
  const [open, setOpen] = useState(props.open ? true : false);
  return (
    <div className="accordion">
      <div
        className="accordion-title flex justify-around w-full cursor-pointer py-[30px]"
        onClick={(e) => {
          setOpen(!open);
        }}
      >
        <div className="w-[90%] flex items-center">
          <h3 className="mb-0">{props.title}</h3>
        </div>
        <div className="w-[10%] flex items-center justify-end">
          <div className="text-[30px] flex items-center justify-center">
            {open ? (
              <FontAwesomeIcon icon={faMinus} />
            ) : (
              <FontAwesomeIcon icon={faPlus} />
            )}
          </div>
        </div>
      </div>
      <div
        className={cn(
          "accordion-content transition-all w-full overflow-hidden",
          open ? "max-h-[1000px] pb-[30px]" : "max-h-0"
        )}
      >
        {props.children}
      </div>
      <hr className="" />
    </div>
  );
};
