import Link from "next/link";
import { parse } from "node-html-parser";
import ReactDOM from "react-dom";
import cn from "classnames";

export function Modal({
  open,
  setOpen,
  children,
}: {
  open: boolean;
  setOpen: (open: boolean) => void;
  children: JSX.Element;
}) {
  return (
    <div
      className={cn(
        "modal fixed top-0 left-0 w-full h-full z-[83886360] flex md:items-center items-end justify-center",
        open ? "" : "pointer-events-none"
      )}
    >
      <div
        className={cn(
          "modal-overlay fixed top-0 left-0 w-full h-full bg-white z-[83886360] transition-all",
          open ? "bg-opacity-30" : "bg-opacity-0 pointer-events-none"
        )}
        onClick={() => {
          setOpen(false);
        }}
      />
      <div
        className={cn(
          "modal-content relative bg-primary w-full max-w-[650px] transition-all md:p-[77px] p-[30px] z-[83886361] md:rounded-[20px] rounded-t-[20px]",
          open ? "scale-1" : "scale-0"
        )}
      >
        <div
          className="absolute top-[33px] right-[33px] cursor-pointer"
          onClick={() => {
            setOpen(false);
          }}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="21"
            height="20"
            viewBox="0 0 21 20"
            fill="none"
          >
            <line
              x1="2.49609"
              y1="17.8577"
              x2="18.3538"
              y2="1.99996"
              stroke="white"
              strokeWidth="3"
              strokeLinecap="round"
            />
            <line
              x1="3.12132"
              y1="2"
              x2="18.979"
              y2="17.8577"
              stroke="white"
              strokeWidth="3"
              strokeLinecap="round"
            />
          </svg>
        </div>
        {children}
      </div>
    </div>
  );
}
