"use client";

import Link from "next/link";
import Image from "next/image";
import { HeaderNav } from "./navs/HeaderNav";
import { useState, useEffect } from "react";
import cn from "classnames";
import { usePathname } from "next/navigation";
import { MobileNav1 } from "../mobile-navs/MobileNav1";

export function Header1({
  menuItems,
  settings,
}: {
  menuItems: any;
  settings: any;
}) {
  const [mobileNavOpen, setMobileNavOpen] = useState(false);

  const pathname = usePathname();
  useEffect(() => {
    if (mobileNavOpen) setMobileNavOpen(false);
  }, [pathname]);

  return (
    <>
      <header className="header1 z-[99] w-full sticky top-0">
        <div
          className={cn(
            "header-wrapper flex flex-col justify-center z-[83886359] w-full transition-all duration-1000"
          )}
        >
          <nav className="desktop-nav flex flex-row items-center justify-between w-full xl:px-[70px] lg:px-[50px] md:px-[40px] px-[20px] md:py-[10px] py-[10px] mx-auto">
            <Link href="/">
              <Image
                src={settings.sitelogo.url}
                alt={settings.sitelogo.alt}
                width={settings.sitelogo.width}
                height={settings.sitelogo.height}
                className="w-fit max-w-[200px] md:max-w-[300px] lg:max-w-[500px]"
              />
            </Link>
            <HeaderNav
              menuItems={menuItems["header_nav"].items}
              mobileNavOpen={mobileNavOpen}
              setMobileNavOpen={setMobileNavOpen}
              settings={settings}
            />
          </nav>
          <MobileNav1
            menuItems={menuItems}
            settings={settings}
            mobileNavOpen={mobileNavOpen}
            setMobileNavOpen={setMobileNavOpen}
          />
        </div>
      </header>
    </>
  );
}
