import cn from "classnames";
import Image from "next/image";
import { getPlaiceholder } from "plaiceholder";
type CoverProps = {
  attrs: any;
  children: any;
  blockIndex: any;
};

export async function Cover({ attrs, children, blockIndex }: CoverProps) {
  const overlayOpacity = attrs.dimRatio ? attrs.dimRatio / 100 : 0;
  let overlayColor = "#000000";
  if (attrs.customOverlayColor) overlayColor = attrs.customOverlayColor;
  else if (attrs.overlayColor) {
    overlayColor = "var(--color-" + attrs.overlayColor + ")";
  }

  const { base64 } = attrs?.blurUrl
    ? await getImage(attrs?.blurUrl)
    : { base64: false };
  return (
    <div
      className={cn(
        "cover wp-block-column flex flex-col relative",
        attrs?.className,
        attrs?.backgroundColor && "bg-" + attrs?.backgroundColor,
        attrs?.textColor && "text-" + attrs?.textColor,
        attrs?.contentPosition &&
          "justify-" +
            attrs.contentPosition.split(" ")[0] +
            " items-" +
            attrs?.contentPosition.split(" ")[1]
      )}
      style={
        attrs.minHeight && {
          minHeight: attrs.minHeight + "px",
        }
      }
    >
      <div
        className="cover-overlay w-full h-full absolute top-0 left-0 pointer-events-none z-[9]"
        style={{
          opacity: overlayOpacity,
          backgroundColor: overlayColor,
        }}
      />
      {attrs.backgroundType === "video" && (
        <video
          className="absolute object-cover w-full h-full"
          autoPlay
          loop
          muted
          playsInline
          src={attrs?.url}
        />
      )}
      {!attrs.backgroundType && (
        <Image
          src={attrs?.url}
          alt={attrs?.alt}
          fill
          className={cn(
            "absolute object-cover w-full h-full",
            attrs?.className
          )}
          style={
            attrs.focalPoint && {
              objectPosition:
                parseFloat(attrs.focalPoint.x) * 100 +
                "% " +
                parseFloat(attrs.focalPoint.y) * 100 +
                "%",
            }
          }
          blurDataURL={base64 ? base64.toString() : ""}
          placeholder={base64 ? "blur" : "empty"}
          priority={blockIndex < 2 ? true : false}
        />
      )}
      <div className="z-[95] relative">{children}</div>
    </div>
  );
}

const getImage = async (src: string) => {
  const buffer = await fetch(src).then(async (res) =>
    Buffer.from(await res.arrayBuffer())
  );

  const {
    metadata: { height, width },
    ...plaiceholder
  } = await getPlaiceholder(buffer, { size: 10 });

  return {
    ...plaiceholder,
    img: { src, height, width },
  };
};
