"use client";
import cn from "classnames";
import Link from "next/link";
import { usePathname, useRouter } from "next/navigation";
import { motion, AnimateSharedLayout } from "framer-motion";
import { useEffect, useState } from "react";

export type toggleMenuComponentProps = {
  items: { title: string; link?: string }[];
};

export function ToggleMenuComponent(props: toggleMenuComponentProps) {
  const [selected, setSelected] = useState<number | null>(null);
  const path = usePathname();
  const router = useRouter();

  useEffect(() => {
    setSelected(props.items.findIndex((n: any) => n.link.includes(path)));
  }, []);
  return (
    <div className="flex justify-center w-full pb-[80px] pt-[20px] font-secondary text-[13px] ">
      <ul className="toggle-menu flex border-primary border-[1px] rounded-[var(--btn-border-radius)] relative">
        {props.items.map((item: any, i: number) => {
          return (
            <motion.li
              key={i}
              className={cn(
                "rounded-[var(--btn-border-radius)] py-[5px] px-[28px] relative cursor-pointer select-none !list-none !m-0"
                // item.link.includes(path) ? "bg-primary" : ""
              )}
              onMouseOver={() => setSelected(i)}
              onClick={() => router.push(item.link)}
              animate
            >
              {i === selected && (
                <motion.div
                  className="bg-primary underlinee absolute top-0 left-0 w-full h-full rounded-[var(--btn-border-radius)]"
                  layoutId="underlinee"
                />
              )}
              <span className="text-secondary z-[10] relative">
                {item.title}
              </span>
            </motion.li>
          );
        })}
      </ul>
    </div>
  );
}
