import cn from "classnames";
import Button from "../atoms/Button";
type ButtonsProps = {
  buttons: { text: string }[];
  attrs?: any;
  settings?: any;
};

export function Buttons({ buttons, attrs, settings }: ButtonsProps) {
  return (
    <div
      className={cn(
        "flex my-3",
        attrs.layout?.justifyContent == "right" && "justify-end",
        attrs.layout?.justifyContent == "left" && "justify-start",
        attrs.layout?.justifyContent == "center" && "justify-center"
      )}
    >
      {buttons.map((b: any, i: number) => {
        const variant =
          b.attrs.className && b.attrs.className.includes("is-style-outline")
            ? "secondary"
            : "primary";
        return (
          <Button
            effect={settings.btn_transition}
            text={b.text}
            href={b.href}
            variant={variant}
          />
        );
      })}
    </div>
  );
}
