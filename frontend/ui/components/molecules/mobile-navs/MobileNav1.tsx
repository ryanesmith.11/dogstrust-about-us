"use client";

import Link from "next/link";
import { useEffect, useState } from "react";
import Button from "../../../components/atoms/Button";
import cn from "classnames";
import { usePathname } from "next/navigation";
import { getMenu } from "lib/api";
import { decode } from "html-entities";
import { getLastPath } from "utils/url";
import Image from "next/image";
import Hamburger from "hamburger-react";
const API_URL = process.env.NEXT_PUBLIC_WORDPRESS_API_URL;

let currentScrollPos = 0;

export function MobileNav1({
  settings,
  menuItems,
  mobileNavOpen,
  setMobileNavOpen,
}: {
  settings: any;
  menuItems: any;
  mobileNavOpen: boolean;
  setMobileNavOpen: any;
}) {
  const pathname = usePathname();

  //onMobileNavOpen, scroll window to top and disable scrolling
  //remember scroll value on click and reset to that after closing
  useEffect(() => {
    if (mobileNavOpen) {
      currentScrollPos = window.pageYOffset;
      document.body.style.overflow = "hidden";
      // window.scrollTo(0, 0);
      //animate the scroll
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    } else {
      window.scrollTo(0, currentScrollPos);
      document.body.style.overflow = "auto";
    }
  }, [pathname]);
  return (
    <>
      <div
        className={cn(
          "mobile-nav fixed h-[100dvh] top-0 bg-white w-full gap-[5px] flex flex-col transition-opacity duration-300 overflow-hidden z-[98]",
          mobileNavOpen ? "opacity-1" : "opacity-0 pointer-events-none delay-0"
        )}
      >
        <nav className="max-w-screen-xl flex flex-row items-center justify-between w-full xl:px-[70px] lg:px-[50px] md:px-[40px] px-[20px] md:py-[10px] py-[10px] mx-auto">
          <Link href="/">
            <Image
              src={
                settings.header_mobile_menu.mobile_logo_replacement
                  ? settings.header_mobile_menu.mobile_logo_replacement.url
                  : settings.sitelogo.url
              }
              alt={settings.sitelogo.alt}
              width={80}
              height={80}
              className="max-w-[200px] md:max-w-[300px] lg:max-w-[500px]"
            />
          </Link>
          <div className="mobile-hamburger md:hidden flex items-center justify-center cursor-pointer">
            <Hamburger
              size={28}
              hideOutline={false}
              toggled={mobileNavOpen}
              toggle={setMobileNavOpen}
              color={cn(
                mobileNavOpen
                  ? "var(--header-mobile-hamburger-close-color)"
                  : "var(--header-mobile-hamburger-color)"
              )}
            />
          </div>
        </nav>
        <div className="flex flex-col justify-between h-full p-[20px]">
          <ul className={cn("flex flex-col")}>
            {menuItems["header_nav"] &&
              menuItems["header_nav"].items.map((n: any, i: number) => {
                const current = getLastPath(n.url) == pathname;
                return (
                  <li key={i} className="relative flex">
                    <Link
                      href={n.url.replace(API_URL, "/")}
                      target={n.target ? n.target : "_self"}
                    >
                      <div
                        className={cn(
                          "text-[37px] w-auto relative inline-flex transform transition-transform duration-300",
                          mobileNavOpen
                            ? "translate-x-0"
                            : "translate-x-[-200%]",
                          i == 0 && mobileNavOpen && "delay-100",
                          i == 1 && mobileNavOpen && "delay-200",
                          i == 2 && mobileNavOpen && "delay-300",
                          i == 3 && mobileNavOpen && "delay-400",
                          i == 4 && mobileNavOpen && "delay-500",
                          i == 5 && mobileNavOpen && "delay-600",
                          i == 6 && mobileNavOpen && "delay-700",
                          i == 7 && mobileNavOpen && "delay-800",
                          current
                            ? "text-primary hover:text-secondary"
                            : "text-secondary hover:text-secondary"
                        )}
                        role="button"
                        onClick={() => setMobileNavOpen(false)}
                      >
                        {decode(n.title)}
                      </div>
                    </Link>
                  </li>
                );
              })}
          </ul>
          {settings.header_mobile_menu.show_mobile_menu_social_nav && (
            <div className="font-secondary border-t-tertiary border-t-[1px] bottom-0 pt-[20px]">
              <div className="social-nav flex items-center justify-between gap-[20px]">
                <span>{settings.header_mobile_menu.mobile_social_text}</span>
                <ul className="flex flex-row gap-[20px]">
                  {menuItems["footer_social_nav"] &&
                    menuItems["footer_social_nav"].items.map(
                      (n: any, i: number) => {
                        return (
                          <li className=" text-[24px]" key={i}>
                            <Link href={n.url.replace(API_URL, "/")}>
                              <div
                                className="text-quaternary hover:text-primary"
                                dangerouslySetInnerHTML={{ __html: n.title }}
                              ></div>
                            </Link>
                          </li>
                        );
                      }
                    )}
                </ul>
              </div>
              <ul className="md:col-span-1 flex flex-col self-end col-span-2 mt-[40px] md:mt-0">
                {menuItems["footer_nav_alt"] &&
                  menuItems["footer_nav_alt"].items.map((n: any, i: number) => {
                    return (
                      <Link href={n.url.replace(API_URL, "/")}>
                        <li className="py-[8px] b2" key={i}>
                          <div
                            dangerouslySetInnerHTML={{ __html: n.title }}
                          ></div>
                        </li>
                      </Link>
                    );
                  })}
              </ul>
            </div>
          )}
        </div>
      </div>
    </>
  );
}
