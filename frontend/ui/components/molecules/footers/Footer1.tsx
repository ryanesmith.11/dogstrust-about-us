import { decode } from "html-entities";
import Link from "next/link";
import CookieConsent from "../CookieConsent";
import Image from "next/image";
const API_URL = process.env.NEXT_PUBLIC_WORDPRESS_API_URL;

export function Footer1({
  settings,
  menuItems,
}: {
  settings: any;
  menuItems: any;
}) {
  let footerNavItems = null;
  if (Object.keys(menuItems).includes("footer_nav")) {
    footerNavItems = menuItems["footer_nav"].items;
  } else if (Object.keys(menuItems).includes("header_nav")) {
    footerNavItems = menuItems["header_nav"].items;
  }
  return (
    <>
      <footer
        className="relative w-full overflow-hidden pt-[127px] pb-[197px]"
        style={{ backgroundColor: settings.footer_bg_color }}
      >
        <div className="relative px-[20px] xl:px-[70px] mx-auto">
          <div className="flex items-start max-w-[1085px] gap-10">
            <Image
              src={settings.footer_logo.url}
              alt="logo shrink-0"
              width={80}
              height={80}
            />
            <div
              className="footer-text text-[18px] text-secondary"
              dangerouslySetInnerHTML={{ __html: settings.footer_text }}
            />
          </div>
          <div className="grid md:gap-x-[94px] gap-x-[60px] grid-cols-2 w-fit relative z-[10] mt-[103px]">
            {footerNavItems && (
              <ul className="flex flex-col row-span-2">
                {footerNavItems.map((n: any, i: number) => {
                  return (
                    <Link href={n.url.replace(API_URL, "/")}>
                      <li
                        className="py-[8px] text-[22px] whitespace-nowrap hover:underline cursor-pointer"
                        key={i}
                        style={{ color: settings.footer_nav_color }}
                      >
                        {decode(n.title)}
                      </li>
                    </Link>
                  );
                })}
              </ul>
            )}
            {Object.keys(menuItems).includes("footer_social_nav") && (
              <ul className="flex flex-col xs:flex-row mt-[25px] xs:gap-[25px] gap-[10px]">
                {menuItems["footer_social_nav"].items &&
                  menuItems["footer_social_nav"].items.map(
                    (n: any, i: number) => {
                      return (
                        <Link href={n.url.replace(API_URL, "/")}>
                          <li
                            className="text-[24px]"
                            key={i}
                            style={{ color: settings.footer_social_text_color }}
                          >
                            <div
                              className=""
                              dangerouslySetInnerHTML={{ __html: n.title }}
                            ></div>
                          </li>
                        </Link>
                      );
                    }
                  )}
              </ul>
            )}
            {Object.keys(menuItems).includes("footer_nav_alt") && (
              <ul className="md:col-span-1 flex flex-col self-end col-span-2 mt-[40px] md:mt-0 hover:underline cursor-pointer">
                {menuItems["footer_nav_alt"].items &&
                  menuItems["footer_nav_alt"].items.map((n: any, i: number) => {
                    return (
                      <Link href={n.url.replace(API_URL, "/")}>
                        <li
                          className="py-[8px] b2"
                          key={i}
                          style={{ color: settings.footer_social_text_color }}
                        >
                          <div
                            dangerouslySetInnerHTML={{ __html: n.title }}
                          ></div>
                        </li>
                      </Link>
                    );
                  })}
              </ul>
            )}
          </div>
        </div>
      </footer>
      {settings.cookie_notice_enabled && <CookieConsent settings={settings} />}
    </>
  );
}
