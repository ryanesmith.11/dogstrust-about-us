import cn from "classnames";
type ContainerProps = {
  attrs?: any;
  children: any;
  settings?: any;
};

export function Container({ attrs, children, settings }: ContainerProps) {
  return (
    <section
      className={cn(
        "core-group mx-auto w-full h-full",
        attrs?.className,
        attrs?.backgroundColor && "bg-" + attrs?.backgroundColor,
        attrs?.textColor && "text-" + attrs?.textColor,
        attrs?.layout.type !== "flex" && "group-container",
        attrs?.layout.type == "flex" && "py-[10px]"
      )}
      style={{
        backgroundColor: attrs?.style?.color?.background,
        color: attrs?.style?.color?.text,
      }}
      id={attrs?.id}
    >
      <div
        className={cn(
          "container mx-auto overflow-hidden",
          attrs?.layout.type == "flex" && "flex gap-[20px]",
          attrs?.layout.orientation == "vertical" && "flex flex-col",
          attrs?.layout.type !== "flex" &&
            "xl:px-[70px] lg:px-[50px] md:px-[40px] px-[20px]"
        )}
        style={{ maxWidth: settings?.container_md + "px" }}
      >
        {children}
      </div>
    </section>
  );
}
