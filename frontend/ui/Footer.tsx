import React from "react";
import { Footer1 } from "./components/molecules/footers/Footer1";
import { getAllMenus, getSettings, getSettingsByPostId } from "lib/api";
import { useContext } from "react";

type FooterComponents = {
  [key: string]: React.ReactNode;
};

export async function Footer({ settings }: { settings: any }) {
  const menuItems = await getAllMenus();
  const footerComponents: FooterComponents = {
    "1": <Footer1 settings={settings} menuItems={menuItems} />,
  };

  return <>{footerComponents[settings.footer_type]}</>;
}
