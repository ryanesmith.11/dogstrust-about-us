import localFont from "next/font/local";
//Tt Ramillas used for alteainsurance
export const ttramillas = localFont({
  src: [
    {
      path: "../public/fonts/TT Ramillas Trial Medium.ttf",
      weight: "400",
      style: "normal",
    },
    {
      path: "../public/fonts/TT Ramillas Trial Bold.ttf",
      weight: "700",
      style: "normal",
    },
  ],
});
