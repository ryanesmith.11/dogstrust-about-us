type urlString = string;

export function getLastPath(urlString: urlString) {
  // if is not a valid url string
  if (urlString.includes("http") === false)
    urlString = "https://www.website.com/" + urlString;
  const url = new URL(urlString);
  const path = url.pathname.split("/");
  if (path[path.length - 1] === "/") path.pop();
  if (path[path.length - 1] === "") path.pop();
  return "/" + path[path.length - 1] + url.hash;
}

export function getBaseURL(url: string) {
  const urlObj = new URL(url);
  return `${urlObj.protocol}//${urlObj.host}/`;
}
