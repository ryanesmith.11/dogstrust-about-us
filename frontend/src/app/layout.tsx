import "../../styles/globals.scss";
import { Styles } from "./styles";
import { Scripts } from "./scripts";
import { getSettings } from "lib/api";
import { GTM } from "./gtm";
import { Suspense } from "react";
import "@fortawesome/fontawesome-free/css/all.min.css";

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const settings = await getSettings();
  return (
    <html lang="en">
      <body className="b1 no-transition" id="body">
        {settings && settings.google_tag_manager_enabled === true && (
          <Suspense>
            <GTM GTM_ID={settings.google_tag_manager_id} />
          </Suspense>
        )}
        {/* @ts-expect-error Server Component */}
        <Styles />
        <main
          id="main"
          className="selection:bg-secondary selection:text-white antialiased  z-[100]"
        >
          {children}
          <div id="modal-portal" />
        </main>
      </body>
      <Scripts settings={settings} />
    </html>
  );
}
