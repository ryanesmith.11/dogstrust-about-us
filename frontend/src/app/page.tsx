import {
  getAllPostsArray,
  getPage,
  getSettings,
  getSettingsByPostId,
} from "lib/api";
import { BlockContent } from "ui/BlockContent";
import { cookies } from "next/headers";
import { notFound, redirect } from "next/navigation";
import { Header } from "ui/Header";
import { Footer } from "ui/Footer";
import cn from "classnames";

type PageProps = {
  params: {
    slug: string;
  };
};

export default async function Page(props: PageProps) {
  const allPosts = await getAllPostsArray();
  const homePage = allPosts["page"].find(
    (post: any) => post.post_name === "home" //TODO: get the wp setting for homepage here
  );
  let settings = null;
  if (homePage) settings = await getSettingsByPostId(homePage.ID);
  else settings = await getSettings();

  // //if coming soon page is enabled and user is not logged in, show the coming soon page
  // const cookiesList = cookies();
  // const hasAccessCookie = cookiesList.has("next-gutenberg-pw-access");
  // const showComingSoonPage = settings.enable_coming_soon && !hasAccessCookie;

  const showComingSoonPage = settings.enable_coming_soon;

  if (showComingSoonPage) redirect("/coming-soon");

  if (!homePage) notFound();
  //otherwise show the homepage
  return (
    <>
      {/* Header and footer are delibrately within page.tsx (rather than layout.tsx) so that page/post level setting configs cna be controlled granularly.  */}
      {/* Whilst this causes a rerender after each navigation event, it has to be this way to allow different configs to be realised.  */}
      {/* @ts-expect-error Server Component */}
      <Header settings={settings} />
      <div
        className={cn(
          "content overflow-hidden bg-white",
          "button-effect-" + settings.btn_transition
        )}
      >
        <BlockContent
          settings={settings}
          blockData={homePage.blockData}
          allPosts={allPosts}
        />
      </div>
      {/* @ts-expect-error Server Component */}
      <Footer settings={settings} />
    </>
  );
}

export async function generateMetadata(props: PageProps) {
  const page = await getPage("home");
  if (!page) return;
  return page.yoast_head_json;
}
