import {
  getAllPages,
  getAllPostsArray,
  getPage,
  getPosts,
  getSettings,
} from "lib/api";
import { BlockContent } from "ui/BlockContent";
import { cookies } from "next/headers";
import { getBaseURL } from "utils/url";
import { AccessButton } from "./AccessButton";
const API_URL = process.env.NEXT_PUBLIC_WORDPRESS_API_URL;

type PageProps = {
  params: {
    slug: string;
  };
};

export default async function Page(props: PageProps) {
  const allPosts = await getAllPostsArray();
  const settings = await getSettings();

  //if coming soon page is enabled and user is not logged in, show the coming soon page
  if (!settings.coming_soon_page) return null;
  const comingSoonPage = await getPage(settings.coming_soon_page.ID);
  return (
    <div className="content coming-soon-active bg-white w-full min-h-screen">
      <BlockContent
        settings={settings}
        blockData={comingSoonPage.blockData}
        allPosts={allPosts}
      />
      {/* <AccessButton /> */}
    </div>
  );
}

export async function generateMetadata(props: PageProps) {
  const settings = await getSettings();
  if (!settings.coming_soon_page) return null;
  const comingSoonPage = await getPage(settings.coming_soon_page.ID);
  return comingSoonPage.yoast_head_json;
}
