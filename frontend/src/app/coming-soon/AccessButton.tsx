"use client";
import Link from "next/link";
import { useEffect, useRef, useState } from "react";
import devtools from "devtools-detect";
import Cookies from "js-cookie";
import { redirect, useRouter } from "next/navigation";

const API_URL = process.env.NEXT_PUBLIC_WORDPRESS_API_URL;

export function AccessButton() {
  const [showAccessButton, setShowAccessButton] = useState(false);
  const passwordRef = useRef<HTMLInputElement>(null);
  const router = useRouter();

  useEffect(() => {
    setShowAccessButton(devtools.isOpen);
    window.addEventListener("devtoolschange", (event) => {
      setShowAccessButton(event.detail.isOpen);
    });
  }, []);

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    console.log("submitted");
    const password = passwordRef.current?.value;
    //this is client component so technically this is accessible in browser, however we don't care for this case, until a better solution is in place
    if (password === "97tyigG)*bjd") {
      console.log("correct pw");
      // add the cookie
      Cookies.set("next-gutenberg-pw-access", "next-gutenberg-pw-access", {
        expires: 1,
      }); // Add the cookie
      router.push("/");
    }
  };

  // on incorrect pw entered, show error message

  return (
    <>
      {showAccessButton && (
        <div className="fixed inset-1/2 -translate-x-1/2 -translate-y-1/2 w-[90%] max-w-[300px] h-fit bg-white border-black border-[4px]">
          <div className="w-full p-[30px] mx-auto h-full flex flex-col item-center justify-center">
            <h5 className="text-center mb-[20px]">
              Enter password to access site preview
            </h5>
            <form onSubmit={handleSubmit}>
              <input
                ref={passwordRef}
                className="px-[30px] py-[20px] border-black border-[4px] w-full"
                name="access_pw"
                placeholder="Enter PW"
                type="password"
              />
              <input
                className="px-[30px] py-[20px] border-black border-[4px] w-full mt-[20px]"
                type="submit"
                value="Submit"
              />
            </form>
          </div>
        </div>
      )}
    </>
  );
}
