import {
  getAllPostsArray,
  getPage,
  getSettings,
  getSettingsByPostId,
} from "lib/api";
import { BlockContent } from "ui/BlockContent";
import { Header } from "ui/Header";
import { Footer } from "ui/Footer";
import cn from "classnames";
import { Container } from "ui/Container";
const API_URL = process.env.NEXT_PUBLIC_WORDPRESS_API_URL;

type PageProps = {
  params: {
    slug: string;
  };
};

export default async function NotFound() {
  const allPosts = await getAllPostsArray();
  const settings = await getSettings();
  //if coming soon page is enabled and user is not logged in, show the coming soon page
  if (!settings.page_404)
    return (
      <div className="page-404">
        {/* @ts-expect-error Server Component */}
        <Header settings={settings} />
        <div
          className={cn(
            "overflow-hidden bg-white",
            "button-effect-" + settings.btn_transition
          )}
        >
          <Container>
            <div className="py-[100px] text-center">
              <h1 className="text-[50px] font-bold">404</h1>
              <p className="text-[20px]">Page not found</p>
            </div>
          </Container>
        </div>
        {/* @ts-expect-error Server Component */}
        <Footer settings={settings} />
      </div>
    );
  const page404 = await getPage(settings.page_404.ID);
  return (
    <div className="page-404">
      {/* @ts-expect-error Server Component */}
      <Header settings={settings} />
      <div
        className={cn(
          "overflow-hidden bg-white",
          "button-effect-" + settings.btn_transition
        )}
      >
        <BlockContent
          settings={settings}
          blockData={page404.blockData}
          allPosts={allPosts}
        />
      </div>
      {/* @ts-expect-error Server Component */}
      <Footer settings={settings} />
    </div>
  );
}

export async function generateMetadata(props: PageProps) {
  const settings = await getSettings();
  if (!settings.page_404) return null;
  const page404 = await getPage(settings.page_404.ID);
  return page404.yoast_head_json;
}
