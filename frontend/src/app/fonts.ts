import {
  Inter,
  Merriweather,
  Open_Sans,
  Ubuntu,
  Montserrat,
  Roboto,
  Work_Sans,
  DM_Sans,
  Fjalla_One,
  Patrick_Hand,
} from "next/font/google";

//custom fonts
import { ttramillas } from "utils/fonts";

export const inter = Inter({ subsets: ["latin"] });
export const merriweather = Merriweather({ weight: "400", subsets: ["latin"] });
export const openSans = Open_Sans({ subsets: ["latin"] });
export const ubuntu = Ubuntu({ weight: "400", subsets: ["latin"] });
export const montserrat = Montserrat({ weight: "400", subsets: ["latin"] });
export const roboto = Roboto({ weight: "400", subsets: ["latin"] });
export const worksans = Work_Sans({ weight: "400", subsets: ["latin"] });
export const dmsans = DM_Sans({ weight: "400", subsets: ["latin"] });
export const dmsansmedium = DM_Sans({ weight: "500", subsets: ["latin"] });
export const fjallaone = Fjalla_One({ weight: "400", subsets: ["latin"] });
export const patrickhand = Patrick_Hand({ weight: "400", subsets: ["latin"] });
export const fonts = [
  { name: "Inter", font: inter },
  { name: "Merriweather", font: merriweather },
  { name: "Open+Sans", font: openSans },
  { name: "Ubuntu", font: ubuntu },
  { name: "Montserrat", font: montserrat },
  { name: "Roboto", font: roboto },
  { name: "Work+Sans", font: worksans },
  { name: "TTRamillas", font: ttramillas },
  { name: "DM+Sans", font: dmsans },
  { name: "DM+Sans+Medium", font: dmsansmedium },
  { name: "Fjalla+One", font: fjallaone },
  { name: "Patrick+Hand", font: patrickhand },
];
