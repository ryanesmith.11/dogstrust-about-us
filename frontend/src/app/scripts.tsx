"use client";

import { useEffect, useState } from "react";
import { usePathname, useSearchParams } from "next/navigation";
import { animate, inView, scroll } from "framer-motion";
import { duration } from "moment";

export function Scripts({ settings }: { settings: any }) {
  const [hydrated, setHydrated] = useState(false);
  const pathname = usePathname();

  //remove no-transition class after hydration, prevents all css animations from running on page load
  useEffect(() => {
    setHydrated(true);
    document.getElementById("body")?.classList.remove("no-transition");
  }, []);

  type animation = {
    [key: string]: any;
  };
  const animations: animation = {
    fade: { opacity: [0, 1], visibility: "visible" },
    "fade-up": { opacity: [0, 1], y: [100, 0], visibility: "visible" },
    "fade-down": { opacity: [0, 1], y: [-100, 0], visibility: "visible" },
    "fade-left": { opacity: [0, 1], x: [-100, 0], visibility: "visible" },
    "fade-right": { opacity: [0, 1], x: [100, 0], visibility: "visible" },
    "fade-up-right": {
      opacity: [0, 1],
      x: [100, 0],
      y: [100, 0],
      visibility: "visible",
    },
    "fade-up-left": {
      opacity: [0, 1],
      x: [-100, 0],
      y: [100, 0],
      visibility: "visible",
    },
    "fade-down-right": {
      opacity: [0, 1],
      x: [100, 0],
      y: [-100, 0],
      visibility: "visible",
    },
    "fade-down-left": {
      opacity: [0, 1],
      x: [-100, 0],
      y: [-100, 0],
      visibility: "visible",
    },
    "fade-in": { opacity: [0, 1], visibility: "visible" },
    "zoom-in": { scale: [0, 1], visibility: "visible" },
    "zoom-in-up": { scale: [0, 1], y: [100, 0], visibility: "visible" },
    "zoom-in-down": { scale: [0, 1], y: [-100, 0], visibility: "visible" },
    "zoom-in-left": { scale: [0, 1], x: [-100, 0], visibility: "visible" },
    "zoom-in-right": { scale: [0, 1], x: [100, 0], visibility: "visible" },
    "zoom-out": { scale: [2, 1], visibility: "visible" },
    "zoom-out-up": { scale: [2, 1], y: [100, 0], visibility: "visible" },
    "zoom-out-down": { scale: [2, 1], y: [-100, 0], visibility: "visible" },
    "zoom-out-left": { scale: [2, 1], x: [-100, 0], visibility: "visible" },
    "zoom-out-right": { scale: [2, 1], x: [100, 0], visibility: "visible" },
    "flip-left": { rotateY: [90, 0], visibility: "visible" },
    "flip-right": { rotateY: [-90, 0], visibility: "visible" },
    "flip-up": { rotateX: [90, 0], visibility: "visible" },
    "flip-down": { rotateX: [-90, 0], visibility: "visible" },
    "slide-up": { y: [100, 0], visibility: "visible" },
    "slide-down": { y: [-100, 0], visibility: "visible" },
    "slide-left": { x: [100, 0], visibility: "visible" },
    "slide-right": { x: [-100, 0], visibility: "visible" },
  };
  const duration = 1;
  const type = "spring";
  const repeat = 0;
  const delay = 0.2;

  useEffect(() => {
    if (settings.animation_enable === true) return;

    Object.keys(animations).forEach((key: string) => {
      const animation = animations[key];
      inView(`.animation-${key}`, ({ target }) => {
        const animationObj = animate(target, animation, {
          type: type,
          duration: duration,
          delay: delay,
        });
        if (repeat) return (leaveInfo) => animationObj.stop();
      });
    });
  }, [pathname]);

  return <></>;
}
