import {
  getAllPages,
  getAllPostsArray,
  getPage,
  getPostBySlug,
  getPosts,
  getSettings,
  getSettingsByPostId,
} from "lib/api";
import { BlockContent } from "ui/BlockContent";
import { Header } from "ui/Header";
import { PostTemplate } from "ui/PostTemplate";
import { createServerContext } from "react";
import { Footer } from "ui/Footer";
import { notFound } from "next/navigation";
const API_URL = process.env.NEXT_PUBLIC_WORDPRESS_API_URL;

type PageProps = {
  params: {
    slug: string;
  };
};

export default async function Page(props: PageProps) {
  const { slug } = props.params;
  const allPosts = await getAllPostsArray();
  if (slug.length === 1) {
    const page = allPosts["page"].find(
      (post: any) => post.post_name === slug[0]
    );
    if (!page) notFound();
    const settings = await getSettingsByPostId(page.ID);
    return (
      <>
        {/* Header and footer are delibrately within page.tsx (rather than layout.tsx) so that page/post level setting configs can be controlled granularly.  */}
        {/* Whilst this causes a rerender after each navigation event, it has to be this way to allow different configs to be realised.  */}
        {/* @ts-expect-error Server Component */}
        <Header settings={settings} />
        <div className="content overflow-hidden bg-white">
          <BlockContent
            settings={settings}
            blockData={page.blockData}
            allPosts={allPosts}
          />
        </div>
        {/* @ts-expect-error Server Component */}
        <Footer settings={settings} />
      </>
    );
  }
  if (slug.length === 2) {
    const post = allPosts[slug[0]].find(
      (post: any) => post.post_name === slug[1]
    );
    const settings = await getSettings();
    if (!post) {
      notFound();
      return null;
    }
    if (!post.blockData) {
      notFound();
      return null;
    }
    return (
      <>
        {/* @ts-expect-error Server Component */}
        <Header settings={settings} />
        <div className="content overflow-hidden bg-white">
          <PostTemplate
            settings={settings}
            postData={post}
            blockData={post.blockData}
            allPosts={allPosts}
          />
        </div>
        {/* @ts-expect-error Server Component */}
        <Footer settings={settings} />
      </>
    );
  }
  return null;
}

export async function generateStaticParams() {
  const allPosts: Record<string, any[]> = await getAllPostsArray();
  let pages: { slug: string[] }[] = [];
  Object.entries(allPosts).forEach(([postType, posts]) => {
    posts.forEach((post: any) => {
      let slug = [postType, post.post_name];
      if (postType === "page") slug = [post.post_name];
      pages.push({
        slug: slug,
      });
    });
  });
  return pages;
}

export async function generateMetadata(props: PageProps) {
  const { slug } = props.params;
  const post = await getPostBySlug(slug.at(-1)); //TODO: imprive this method to take into account allslug items
  if (post == null) return null;
  else return post["yoastHeadJSON"];
}
