import { NextRequest, NextResponse } from "next/server";
import { revalidateTag } from "next/cache";

type tags = "all" | "posts" | "menus" | "settings";

export async function GET(request: NextRequest) {
  const tag = request.nextUrl.searchParams.get("tag") as string;

  //check if tag is valid
  if (!["all", "posts", "menus", "settings"].includes(tag)) {
    return NextResponse.json({
      revalidated: false,
      now: Date.now(),
      tag: tag,
    });
  }

  //check if tag is 'all'
  if (tag === "all") {
    // revalidate all tags
    for (const tag of ["posts", "menus", "settings"]) {
      revalidateTag(tag as tags);
    }
    return NextResponse.json({
      revalidated: true,
      now: Date.now(),
      tag: "all",
    });
  }

  // otherwise revalidate individual tag
  revalidateTag(tag);
  return NextResponse.json({ revalidated: true, now: Date.now(), tag: tag });
}
