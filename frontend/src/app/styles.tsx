import { getSettings } from "lib/api";
import { fonts } from "./fonts";

export async function Styles() {
  const settings = await getSettings();
  let requiredFonts: { style: { fontFamily: any } }[] = [];
  for (var i in settings.fonts) {
    for (var k in fonts) {
      if (settings.fonts[i] === fonts[k].name)
        requiredFonts.push(fonts[k].font);
    }
  }

  return (
    <div
      dangerouslySetInnerHTML={{
        __html: `
      <style>
        :root {
          --color-primary: ${settings.primary_color};
          --color-secondary: ${settings.secondary_color};
          --color-tertiary: ${settings.tertiary_color};
          --color-quaternary: ${settings.quaternary_color};
          --color-quinary: ${settings.quinary_color};
          --default-text-color: ${settings.default_text_color};
          --font-primary: ${requiredFonts[0]?.style.fontFamily};
          ${
            requiredFonts[1]
              ? "--font-secondary:" + requiredFonts[1]?.style.fontFamily
              : "--font-secondary:" + requiredFonts[0]?.style.fontFamily
          };
          ${
            requiredFonts[2]
              ? "--font-tertiary:" + requiredFonts[2]?.style.fontFamily
              : ""
          };
          --h1-lg: ${settings.heading_sizes.h1_lg}px;
          --h1-lg-line-height: ${
            settings.heading_sizes.h1_lg_lineheight
              ? settings.heading_sizes.h1_lg_lineheight + "px"
              : "normal"
          };
          --h1-md: ${settings.heading_sizes.h1_md}px;
          --h1-md-line-height: ${
            settings.heading_sizes.h1_md_lineheight
              ? settings.heading_sizes.h1_md_lineheight + "px"
              : "normal"
          };
          --h1-sm: ${settings.heading_sizes.h1_sm}px;
          --h1-sm-line-height: ${
            settings.heading_sizes.h1_sm_lineheight
              ? settings.heading_sizes.h1_sm_lineheight + "px"
              : "normal"
          };
          --h2-lg: ${settings.heading_sizes.h2_lg}px;
          --h2-lg-line-height: ${
            settings.heading_sizes.h2_lg_lineheight
              ? settings.heading_sizes.h2_lg_lineheight + "px"
              : "normal"
          };
          --h2-md: ${settings.heading_sizes.h2_md}px;
          --h2-md-line-height: ${
            settings.heading_sizes.h2_md_lineheight
              ? settings.heading_sizes.h2_md_lineheight + "px"
              : "normal"
          };
          --h2-sm: ${settings.heading_sizes.h2_sm}px;
          --h2-sm-line-height: ${
            settings.heading_sizes.h2_sm_lineheight
              ? settings.heading_sizes.h2_sm_lineheight + "px"
              : "normal"
          };
          --h3-lg: ${settings.heading_sizes.h3_lg}px;
          --h3-lg-line-height: ${
            settings.heading_sizes.h3_lg_lineheight
              ? settings.heading_sizes.h3_lg_lineheight + "px"
              : "normal"
          };
          --h3-md: ${settings.heading_sizes.h3_md}px;
          --h3-md-line-height: ${
            settings.heading_sizes.h3_md_lineheight
              ? settings.heading_sizes.h3_md_lineheight + "px"
              : "normal"
          };
          --h3-sm: ${settings.heading_sizes.h3_sm}px;
          --h3-sm-line-height: ${
            settings.heading_sizes.h3_sm_lineheight
              ? settings.heading_sizes.h3_sm_lineheight + "px"
              : "normal"
          };
          --h4-lg: ${settings.heading_sizes.h4_lg}px;
          --h4-lg-line-height: ${
            settings.heading_sizes.h4_lg_lineheight
              ? settings.heading_sizes.h4_lg_lineheight + "px"
              : "normal"
          };
          --h4-md: ${settings.heading_sizes.h4_md}px;
          --h4-md-line-height: ${
            settings.heading_sizes.h4_md_lineheight
              ? settings.heading_sizes.h4_md_lineheight + "px"
              : "normal"
          };
          --h4-sm: ${settings.heading_sizes.h4_sm}px;
          --h4-sm-line-height: ${
            settings.heading_sizes.h4_sm_lineheight
              ? settings.heading_sizes.h4_sm_lineheight + "px"
              : "normal"
          };
          --h5-lg: ${settings.heading_sizes.h5_lg}px;
          --h5-lg-line-height: ${
            settings.heading_sizes.h5_lg_lineheight
              ? settings.heading_sizes.h5_lg_lineheight + "px"
              : "normal"
          };
          --h5-md: ${settings.heading_sizes.h5_md}px;
          --h5-md-line-height: ${
            settings.heading_sizes.h5_md_lineheight
              ? settings.heading_sizes.h5_md_lineheight + "px"
              : "normal"
          };
          --h5-sm: ${settings.heading_sizes.h5_sm}px;
          --h5-sm-line-height: ${
            settings.heading_sizes.h5_sm_lineheight
              ? settings.heading_sizes.h5_sm_lineheight + "px"
              : "normal"
          };
          --h6-lg: ${settings.heading_sizes.h6_lg}px;
          --h6-lg-line-height: ${
            settings.heading_sizes.h6_lg_lineheight
              ? settings.heading_sizes.h6_lg_lineheight + "px"
              : "normal"
          };
          --h6-md: ${settings.heading_sizes.h6_md}px;
          --h6-md-line-height: ${
            settings.heading_sizes.h6_md_lineheight
              ? settings.heading_sizes.h6_md_lineheight + "px"
              : "normal"
          };
          --h6-sm: ${settings.heading_sizes.h6_sm}px;
          --h6-sm-line-height: ${
            settings.heading_sizes.h6_sm_lineheight
              ? settings.heading_sizes.h6_sm_lineheight + "px"
              : "normal"
          };


          --header-bg-color: ${settings.header_bg_color};
          --header-border-bottom-color: ${settings.header_border_bottom_color};
          --header-border-bottom-width: ${
            settings.header_border_bottom_width
          }px;
          --header-text-color: ${settings.header_text_color};
          --header-position: ${settings.header_position};
          --header-width: ${settings.header_width}px;
          --header-mobile-hamburger-color: ${
            settings.header_mobile_menu.hamburger_color
          };
          --header-mobile-hamburger-close-color: ${
            settings.header_mobile_menu.hamburger_close_color
          };

          --btn1-bg-color: ${settings.buttons[0].btn_bg_color};
          --btn1-hover-bg-color: ${settings.buttons[0].btn_hover_bg_color};
          --btn1-text-color: ${settings.buttons[0].btn_text_color};
          --btn1-hover-text-color: ${settings.buttons[0].btn_hover_text_color};
          --btn1-border-color: ${settings.buttons[0].btn_border_color};
          --btn2-bg-color: ${settings.buttons[1].btn_bg_color};
          --btn2-hover-bg-color: ${settings.buttons[1].btn_hover_bg_color};
          --btn2-text-color: ${settings.buttons[1].btn_text_color};
          --btn2-hover-text-color: ${settings.buttons[1].btn_hover_text_color};
          --btn2-border-color: ${settings.buttons[1].btn_border_color};
          --btn-border-radius: ${settings.btn_border_radius}px;
          --btn-border-width: ${settings.btn_border_width}px;
          --btn-padding-x: ${settings.btn_padding_x}px;
          --btn-padding-y: ${settings.btn_padding_y}px;
          --btn-mobile-padding-x: ${settings.btn_mobile_padding_x}px;
          --btn-mobile-padding-y: ${settings.btn_mobile_padding_y}px;
          --btn-transition: ${settings.btn_transition};
          --pill1-bg-color: ${settings.pills[0].pill_bg_color};
          --pill1-text-color: ${settings.pills[0].pill_text_color};
          --pill2-bg-color: ${settings.pills[1].pill_bg_color};
          --pill2-text-color: ${settings.pills[1].pill_text_color};
          --pill-border-radius: ${settings.pill_border_radius}px;

          --border-radius-2xl: ${settings.border_radius_2xl}px;
          --border-radius-xl: ${settings.border_radius_xl}px;
          --border-radius-lg: ${settings.border_radius_lg}px;
          --border-radius-md: ${settings.border_radius_md}px;
          --border-radius-sm: ${settings.border_radius_sm}px;
          
          --container-2xl: ${settings.container_2xl}px;
          --container-xl: ${settings.container_xl}px;
          --container-lg: ${settings.container_lg}px;
          --container-md: ${settings.container_md}px;
          --container-sm: ${settings.container_sm}px;
          
          --cookie-notice-bg-color: ${settings.cookie_notice_bg_color};

        }
        body, a{
          color: var(--default-text-color);
        }
        .font-primary,
        body,
        .b1,
        .b2 {
          font-family: var(--font-primary);
        }
        .font-secondary,
        h1,
        h2,
        h3,
        h4,
        .b2,
        .b4 {
          font-family: var(--font-secondary);
        }
        .font-tertiary {
          font-family: var(--font-tertiary);
        }
        .bg-primary {
          background-color: var(--color-primary);
        }
        .bg-secondary {
          background-color: var(--color-secondary);
        }
        .bg-tertiary {
          background-color: var(--color-tertiary);
        }
        .bg-quaternary {
          background-color: var(--color-quaternary);
        }
        .bg-quinary {
          background-color: var(--color-quinary);
        }
        .text-primary, .has-primary-color {
          color: var(--color-primary);
        }
        .text-secondary, .has-secondary-color {
          color: var(--color-secondary);
        }
        .text-tertiary, .has-tertiary-color {
          color: var(--color-tertiary);
        }
        .text-quaternary , .has-quaternary-color {
          color: var(--color-quaternary);
        }

        /* Header border */
        ${
          settings.header_border_bottom_enabled
            ? "header nav {border-bottom: var(--header-border-bottom-width) solid var(--header-border-bottom-color);}"
            : ""
        }
        
        /* Header bg color */
        ${
          settings.header_bg_color
            ? "header .header-wrapper {background-color:var(--header-bg-color);}"
            : ""
        }

        /* Header text color */
        ${
          settings.header_text_color
            ? "header nav a{color:var(--header-text-color);} header .motiondiv{background-color:var(--header-text-color);}"
            : ""
        }
        /* Header text hover color */
        ${
          settings.header_text_hover_color
            ? "header nav a:hover{color:var(--header-text-hover-color);}"
            : "header nav a:hover{color: var(--bg-primary);}"
        } 

        ${
          settings.header_text_color
            ? "header nav a{color:var(--header-text-color);} header .motiondiv{background-color:var(--header-text-color);}"
            : ""
        }

        /* Header width */
        ${
          settings.header_width &&
          "header nav, footer > div {max-width:var(--header-width);}"
        }


        
        </style>`,
      }}
    />
  );
}
