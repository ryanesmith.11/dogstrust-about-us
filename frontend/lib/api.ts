const API_URL = process.env.NEXT_PUBLIC_WORDPRESS_API_URL;

export async function getAllPages() {
  try {
    const response = await fetch(`${API_URL}wp-json/wp/v2/pages/`, {
      method: "GET",
      cache: "force-cache",
      next: { tags: ["posts"] },
    });
    const res = await response.json();
    return res;
  } catch (error) {
    console.error(error);
  }
}

export async function getPage(req: any) {
  try {
    if (!isNaN(req)) {
      //if it is a number (post id)
      const response = await fetch(
        `${API_URL}wp-json/wp/v2/pages/${req.toString()}`,
        {
          method: "GET",
          cache: "force-cache",
          next: { tags: ["posts"] },
        }
      );
      const res = await response.json();
      return res;
    } else {
      //if its a slug
      const response = await fetch(
        `${API_URL}wp-json/wp/v2/pages?slug=${req}`,
        {
          method: "GET",
          cache: "force-cache",
          next: { tags: ["posts"] },
        }
      );
      const res = await response.json();
      return res[0];
    }
  } catch (error) {
    console.error(error);
  }
}

export async function getPostByID(id: number | string) {
  try {
    const allPostTypes = await getAllPostsArray();
    const post = allPostTypes.forEach((posts: any[]) => {
      posts.forEach((post: any) => {
        if (post.id === id) {
          return post;
        }
      });
    });
    return post;
  } catch (error) {
    console.error(error);
  }
}

export async function getPostBySlug(slug: string | undefined) {
  try {
    const allPostTypes = await getAllPostsArray();
    const allPosts: Record<string, any[]> = await getAllPostsArray();
    let foundPost = null;
    Object.entries(allPosts).forEach(([postType, posts]) => {
      posts.forEach((post: any) => {
        if (post.post_name === slug) foundPost = post;
      });
    });
    return foundPost;
  } catch (error) {
    console.error(error);
  }
}

export async function getAttachment(id: number | string) {
  try {
    const response = await fetch(
      `${API_URL}wp-json/wp/v2/media/${id.toString()}`,
      {
        method: "GET",
        cache: "force-cache",
      }
    );
    const res = await response.json();
    return res;
  } catch (error) {
    console.error(error);
  }
}

export async function getPosts() {
  try {
    const response = await fetch(`${API_URL}wp-json/wp/v2/posts`, {
      method: "GET",
      next: { tags: ["posts"] },
      cache: "force-cache",
    });
    const res = await response.json();
    return res;
  } catch (error) {
    console.error(error);
  }
}

export async function getAllPostsArray() {
  try {
    const response = await fetch(
      `${API_URL}wp-json/next-gutenberg/getpostsarray`,
      {
        method: "GET",
        next: { tags: ["posts"] },
        cache: "force-cache",
      }
    );
    const res = await response.json();
    return res;
  } catch (error) {
    console.error(error);
  }
}

export async function getMenu(slug: string) {
  try {
    const response = await fetch(
      `${API_URL}/wp-json/next-gutenberg/menus/?slug=${slug}`,
      {
        method: "GET",
        next: { tags: ["posts"] },
        cache: "force-cache",
      }
    );
    const res = await response.json();
    return res;
  } catch (error) {
    console.error(error);
  }
}

export async function getAllMenus() {
  try {
    const response = await fetch(`${API_URL}wp-json/next-gutenberg/menus`, {
      method: "GET",
      next: { tags: ["menus"] },
      cache: "force-cache",
    });
    const res = await response.json();
    return res;
  } catch (error) {
    console.error(error);
  }
}
export async function getSettings() {
  try {
    const apiUrl = `${API_URL}wp-json/next-gutenberg/settings`;

    const response = await fetch(apiUrl, {
      method: "GET",
      next: { tags: ["settings"] },
      cache: "force-cache",
    });

    const data = await response.json();

    return data;
  } catch (error) {
    console.error(error);
  }
}

export async function getSettingsByPostId(id: number) {
  try {
    const response = await fetch(
      `${API_URL}wp-json/next-gutenberg/settings/${id}`,
      {
        method: "GET",
        next: { tags: ["settings", "posts"] },
        cache: "force-cache",
      }
    );
    const res = await response.json();
    return res;
  } catch (error) {
    console.error(error);
  }
}
