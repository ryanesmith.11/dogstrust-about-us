/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./ui/**/*.{js,ts,jsx,tsx}",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    colors: {
      white: "#ffffff",
      black: "#000000",
      blue: "#1fb6ff",
      purple: "#7e5bef",
      pink: "#ff49db",
      orange: "#ff7849",
      green: "#13ce66",
      yellow: "#ffc82c",
      transparent: "transparent",
      current: "currentColor",
      "gray-dark": "#273444",
      gray: "#8492a6",
      "gray-light": "#F6F7F9",
      primary: "var(--color-primary)",
      secondary: "var(--color-secondary)",
      tertiary: "var(--color-tertiary)",
    },
    fontFamily: {
      opensans: ["Open Sans", "sans-serif"],
      inter: ["Inter", "sans-serif"],
      merriweather: ["Cal Sans", "Inter", "sans-serif"],
    },

    extend: {
      borderRadius: {
        xl: "var(--border-radius-2xl)",
        xl: "var(--border-radius-xl)",
        lg: "var(--border-radius-lg)",
        md: "var(--border-radius-md)",
        sm: "var(--border-radius-sm)",
      },
    },
  },
  plugins: [],
};
