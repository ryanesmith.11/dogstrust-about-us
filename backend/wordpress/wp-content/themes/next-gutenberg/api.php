<?php

//register acf fields to Wordpress API
function acf_to_rest_api($response, $post, $request)
{
    if (!function_exists('get_fields')) return $response;

    if (isset($post)) {
        $response->data['blockData'] = parse_blocks($post->post_content);
    }
    return $response;
}
add_filter('rest_prepare_post', 'acf_to_rest_api', 10, 3);
add_filter('rest_prepare_page', 'acf_to_rest_api', 10, 3);

//register acf fields to Wordpress API
function parse_reusable_blocks($response, $post, $request)
{
    if (!function_exists('get_fields')) return $response;

    if (isset($post)) {
        foreach ($response->data['blockData'] as &$top_level_block) {
            if ($top_level_block['blockName'] === "core/block") {
                $response->data['blockData'] = parse_blocks(get_post($top_level_block['attrs']['ref'])->post_content);
            }
        }
    }
    return $response;
}
add_filter('rest_prepare_post', 'parse_reusable_blocks', 10, 3);
add_filter('rest_prepare_page', 'parse_reusable_blocks', 10, 3);



//------------------------------------------------
// ** Gravity forms **
//-----------------------------------------------
function gf_to_rest_api($response, $post, $request)
{
    if (!class_exists('GFAPI')) return $response;

    if (isset($post)) {
        $response->data['blockData'] = replace_data($response->data['blockData']);
    }
    return $response;
}
add_filter('rest_prepare_post', 'gf_to_rest_api', 20, 3);
add_filter('rest_prepare_page', 'gf_to_rest_api', 20, 3);

function replace_data($json_array)
{
    foreach ($json_array as $key => &$value) {
        if ($value['blockName'] === 'gravityforms/form') {
            $value['gfData'] = GFAPI::get_form($value['attrs']['formId']);
        } elseif (is_array($value['innerBlocks'])) {
            $value['innerBlocks'] = replace_data($value['innerBlocks']);
        }
    }
    return $json_array;
}


/**
 * Returns menu items in a array based on the navigation menu id passed
 *
 * @param object The actual request where parameters can be accessed.
 * @return array The menu items contained in that specific menu
 */
function expose_navigation($request)
{
    $slug = $request['slug'];
    if (!$slug) return get_all_menus();

    return get_menu_items_by_registered_slug($slug);
}

/**
 * Exposes under /navigation/{id} the menu items in the wp-json api
 *
 * @return void
 */
function expose_navigation_to_rest()
{
    register_rest_route(
        'next-gutenberg',
        '/menus',
        [
            'methods' => 'GET',
            'callback' => 'expose_navigation'
        ]
    );
}
add_action('rest_api_init', 'expose_navigation_to_rest');

function get_menu_items_by_registered_slug($menu_slug)
{
    $menu_items = array();

    if (($locations = get_nav_menu_locations()) && isset($locations[$menu_slug]) && $locations[$menu_slug] != 0) {
        $menu = get_term($locations[$menu_slug]);
        $menu_items = wp_get_nav_menu_items($menu->term_id);
    }

    return $menu_items;
}

function get_all_menus()
{
    $all_menus = array();
    $locations = get_nav_menu_locations();
    foreach ($locations as $location => $id) {
        $menu = wp_get_nav_menu_object($id);
        $all_menus[$location] = array(
            'id'    => $menu->term_id,
            'name'  => $menu->name,
            'slug'  => $menu->slug,
            'items' => wp_get_nav_menu_items($menu->term_id)
        );
    }
    return $all_menus;
}

//add featrued image to rest api
add_action('rest_api_init', 'register_rest_categories');
function register_rest_categories()
{
    register_rest_field(
        array('post'),
        'categoryNames',
        array(
            'get_callback'    => 'get_categories_name_array',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}
function get_categories_name_array($object)
{
    if (is_array($object)) $categories = $object['categories'];
    else $categories = get_the_category($object);
    foreach ($categories as $cat) {
        $cat = get_term($cat);
        $cats[] = $cat->name;
    }
    return $cats;
}

//add post categries to rest api
add_action('rest_api_init', 'register_rest_images');
function register_rest_images()
{
    register_rest_field(
        array('post'),
        'fimg_url',
        array(
            'get_callback'    => 'get_rest_featured_image',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}
function get_rest_featured_image($object, $field_name, $request)
{
    if ($object['featured_media']) {
        $img = wp_get_attachment_image_src($object['featured_media'], 'app-thumb');
        return $img[0];
    }
    return false;
}


add_action('rest_api_init', 'any_post_api_route');
function any_post_api_route()
{
    register_rest_route('next-gutenberg', '/getpostsarray/', array(
        'methods' => 'GET',
        'callback' => 'get_all_posts_array',
    ));
}

/**
 *
 * Get content by slug
 *
 * @param WP_REST_Request $request
 * @return WP_REST_Response
 */
function get_all_posts_array(WP_REST_Request $request)
{
    $all_posts = get_posts(array(
        'post_type' => 'any',
        'post_status' => 'publish',
        'posts_per_page' => -1,
    ));
    $post_types_array = array();
    foreach ($all_posts as &$post) {
        $post->fimg_url = get_the_post_thumbnail_url($post->ID, 'full');
        // $post->gfData = GFAPI::get_form($value['attrs']['formId']);
        // $post->blockData = parse_blocks($post->post_content);
        $post->blockData = addAdditionalBlockData(parse_blocks($post->post_content));
        $post->post_content = apply_filters('the_content', $post->post_content); //ensures html is returned with <p> tags etc.

        $post->acfData = get_fields($post->ID);
        $post->categoryNames = get_categories_name_array($post);
        $post->postAuthor = get_the_author_meta("display_name", $post->post_author);

        if (function_exists('YoastSEO')) {
            $meta_helper = YoastSEO()->classes->get(Yoast\WP\SEO\Surfaces\Meta_Surface::class);
            $meta = $meta_helper->for_post($post->ID);
            $post->yoastHeadJSON = $meta->get_head()->json;
        }

        $post_types_array[$post->post_type][] = $post;
    }
    return $post_types_array;
}

function addAdditionalBlockData($blocks)
{
    foreach ($blocks as &$block) {
        if ($block['blockName'] === 'gravityforms/form') {
            $block['gfData'] = GFAPI::get_form($block['attrs']['formId']);
            $block['gfDataInnerHtml'] = do_shortcode("[gravityform id='" . $block['attrs']['formId'] . "' title='false' description='false' ajax='true']");
        } else if ($block['blockName'] === 'core/block') {
            $block['innerBlocks'] = parse_blocks(get_post($block['attrs']['ref'])->post_content);
        } else if ($block['blockName'] === 'core/cover') {
            $blur_url = wp_get_attachment_image_src($block['attrs']['id'], 'medium')[0];
            $block['attrs']['blurUrl'] = $blur_url;
        } else if ($block['blockName'] === 'acf/logo-slider') {
            //force return of array of repeater from parse_blocks https://support.advancedcustomfields.com/forums/topic/how-to-get-repeater-block-data-back-into-an-array/
            // acf_setup_meta($block['attrs']['data'], $block['attrs']['id'], true);
            // $block['attrs']['data']['logos'] = get_field('logos');
            // var_dump($block);
            // die;
        } else if (count($block["innerBlocks"]) > 0) {
            $block["innerBlocks"] = addAdditionalBlockData($block["innerBlocks"]);
        }
    }
    return $blocks;
}


/**
 * Returns menu items in a array based on the navigation menu id passed
 *
 * @param object The actual request where parameters can be accessed.
 * @return array The menu items contained in that specific menu
 */
function expose_ng_settings($request)
{
    if (function_exists("switch_to_blog")) switch_to_blog(get_current_blog_id());
    if ($request->get_param('id') && get_field("enable_settings_overrides", $request->get_param('id'))) $all_settings = array_merge(get_fields('options'), (array)array_filter((array)get_field("post_style_settings", $request->get_param('id'))), wp_load_alloptions());
    else $all_settings = array_merge(get_fields('options'), wp_load_alloptions());
    if (function_exists("switch_to_blog")) restore_current_blog();
    return $all_settings;
}

/**
 * Exposes under /settings
 */
function expose_ng_settings_to_rest()
{
    register_rest_route(
        'next-gutenberg',
        '/settings',
        [
            'methods' => 'GET',
            'callback' => 'expose_ng_settings'
        ]
    );
}
function expose_ng_settings_to_rest_with_id()
{
    register_rest_route(
        'next-gutenberg',
        '/settings/(?P<id>\d+)',
        [
            'methods' => 'GET',
            'callback' => 'expose_ng_settings'
        ]
    );
}
add_action('rest_api_init', 'expose_ng_settings_to_rest_with_id');
add_action('rest_api_init', 'expose_ng_settings_to_rest');



//image
function edit_attachment_response($object, $field_name, $request)
{
    if ($object['type'] == "attachment") {
        if (($xml = simplexml_load_file($object['guid']['raw'])) !== false) {
            $attrs = $xml->attributes();
            $viewbox = explode(' ', $attrs->viewBox);
            $image[1] = isset($attrs->width) && preg_match('/\d+/', $attrs->width, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[2] : null);
            $image[2] = isset($attrs->height) && preg_match('/\d+/', $attrs->height, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[3] : null);
            return array($viewbox[2], $viewbox[3]);
        }
    }
    return false;
}

function featured_media_posts_api()
{
    register_rest_field(
        array('attachment'), //name of post type 'post', 'page'
        'dimensions', //name of field to display
        array(
            'get_callback'    => 'edit_attachment_response',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}
add_action('rest_api_init', 'featured_media_posts_api');


//logged in api check
add_action('rest_api_init', 'add_is_logged_in_route');
function add_is_logged_in_route()
{
    $GLOBALS['user_id'] = get_current_user_id();
    register_rest_route('next-gutenberg', '/isloggedin/', array(
        'methods' => 'GET',
        'callback' => 'rest_is_logged_in',
    ));
}
function rest_is_logged_in()
{
    return get_user_by('id', $GLOBALS['user_id']) ? true : false;
}
