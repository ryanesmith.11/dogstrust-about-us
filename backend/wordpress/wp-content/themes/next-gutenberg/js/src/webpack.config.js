var path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  mode: "development",
  entry: {
    "../style": "./js/index.js",
  },
  output: {
    path: path.resolve(__dirname, ""),
    filename: "../theme-bundle.min.js",
  },

  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader",
          "postcss-loader",
        ],
      },
      {
        test: /\.(eot|svg|otf|ttf|woff|woff2|png|gif|jpg)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              limit: 10000,
              mimetype: "application/font-woff",
              outputPath: "../assets",
              publicPath: "assets/",
            },
          },
        ],
      },
      {
        test: /\.json$/,
        loader: "json-loader",
      },
    ],
  },

  // externals: ["jquery"],

  plugins: [
    // extract css into dedicated file
    new MiniCssExtractPlugin({
      path: "/",
      filename: "[name].css",
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
    }),
  ],
};
