module.exports = {
  content: ["../**/*.php", "./js/**/*.js"],
  theme: {
    screens: {
      "4k": "2560px",
      "laptop-l": "1440px",
      laptop: "1024px",
      tablet: "768px",
      "mobile-l": "425px",
      "mobile-m": "375px",
      "mobile-s": "320px",
    },
    extend: {
      colors: {
        "lr-blue": "#A7CAD4",
        "lr-charcoal": "#2F2D32",
        "lr-grey": "#ECECEC",
      },
      fontSize: {
        // 76pt - Bold
        h1: [
          "4.75rem",
          {
            fontWeight: "700",
          },
        ],
        // 76pt - Bold
        h2: [
          "4.75rem",
          {
            fontWeight: "700",
          },
        ],
        // 40pt - Semibold
        h3: [
          "2.5rem",
          {
            fontWeight: "600",
          },
        ],
        // 40pt - Bold
        h4: [
          "2.5rem",
          {
            fontWeight: "700",
          },
        ],
        // 33.5pt - Medium
        h5: [
          "2.1rem",
          {
            fontWeight: "500",
          },
        ],
        // 76pt - Medium
        h6: [
          "2.1rem",
          {
            fontWeight: "500",
          },
        ],
        p: [
          "1rem",
          {
            fontWeight: "400",
            lineHeight: "1.5rem",
          },
        ],
        b3: [
          "1rem",
          {
            fontWeight: "500",
          },
        ],
        "h1-mobile": [
          "3rem",
          {
            fontWeight: "700",
            letterSpacing: "0.15rem",
          },
        ],
        "b3-mobile": [
          "0.937rem",
          {
            fontWeight: "500",
          },
        ],
        "h3-mobile": [
          "1.562rem",
          {
            fontWeight: "600",
          },
        ],
      },
      fontfamily: {
        works: ["SuisseWorks-Book"],
      },
    },
  },
  plugins: [require("@tailwindcss/line-clamp")],
  important: true,
  prefix: "tw-",
};
