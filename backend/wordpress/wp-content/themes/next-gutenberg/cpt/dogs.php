<?php

/**
 * Register a custom post type called "Dog".
 */

add_action('init', array('DogCPT', 'init'));

class DogCPT
{
    private static $slug = "dog";
    private static $label = "Dogs";
    private static $plural = "Dogs";

    public static function init()
    {
        self::register();
        self::register_acf_fields();
    }

    private static function register()
    {
        $labels = array(
            "name"                  => _x(self::$plural, "Post type general name", "textdomain"),
            "singular_name"         => _x(self::$label, "Post type singular name", "textdomain"),
            "menu_name"             => _x(self::$plural, "Admin Menu text", "textdomain"),
            "name_admin_bar"        => _x(self::$label, "Add New on Toolbar", "textdomain"),
            "add_new"               => __("Add New", "textdomain"),
            "add_new_item"          => __("Add New " . self::$label, "textdomain"),
            "new_item"              => __("New " . self::$label, "textdomain"),
            "edit_item"             => __("Edit " . self::$label, "textdomain"),
            "view_item"             => __("View " . self::$label, "textdomain"),
            "all_items"             => __("All " . self::$plural . "", "textdomain"),
            "search_items"          => __("Search " . self::$plural, "textdomain"),
            "parent_item_colon"     => __("Parent " . self::$plural . ":", "textdomain"),
            "not_found"             => __("No " . self::$plural . " found.", "textdomain"),
            "not_found_in_trash"    => __("No " . self::$plural . " found in Trash.", "textdomain"),
            "set_featured_image"    => _x("Set cover image", "Overrides the “Set featured image” phrase for this post type. Added in 4.3", "textdomain"),
            "remove_featured_image" => _x("Remove cover image", "Overrides the “Remove featured image” phrase for this post type. Added in 4.3", "textdomain"),
            "use_featured_image"    => _x("Use as cover image", "Overrides the “Use as featured image” phrase for this post type. Added in 4.3", "textdomain"),
            "archives"              => _x(self::$label . " archives", "The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4", "textdomain"),
            "insert_into_item"      => _x("Insert into " . self::$label, "Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4", "textdomain"),
            "uploaded_to_this_item" => _x("Uploaded to this " . self::$label, "Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4", "textdomain"),
            "filter_items_list"     => _x("Filter " . self::$plural . " list", "Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4", "textdomain"),
            "items_list_navigation" => _x(self::$plural . "list navigation", "Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4", "textdomain"),
            "items_list"            => _x(self::$plural . "list", "Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4", "textdomain"),
        );

        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array('slug' => self::$slug),
            'capability_type'    => 'post',
            'has_archive'        => true,
            // 'taxonomies'  => array('category'),
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array('title', 'thumbnail', 'comments', 'categories'),
        );

        register_post_type(self::$slug, $args);
    }



    //home page fields
    private static function register_acf_fields()
    {
        if (function_exists('acf_add_local_field_group')) :

            $dog = new StoutLogic\AcfBuilder\FieldsBuilder('dog');
            $dog
                ->addTab('details')
                ->addTextarea('quote')
                ->addText('quotee_name')
                ->addText('quotee_position')
                ->setLocation('post_type', '==', self::$slug);


            acf_add_local_field_group($dog->build());
        endif;
    }
}
