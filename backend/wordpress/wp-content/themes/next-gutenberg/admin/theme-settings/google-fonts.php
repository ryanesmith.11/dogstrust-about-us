<?php


function getAllGoogleFonts()
{

    $google_fonts = array();
    $google_fonts["inherit"] = "Inherit";
    $url = "https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyCxxdoGtiswt3jFSTTrCt0SX24ufU0J4NU";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  // Disable SSL verification
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    $font_response = curl_exec($ch);
    curl_close($ch);

    $json = json_decode($font_response);
    $fonts = $json->items;
    foreach ($fonts as $font) {
        $google_fonts[$font->family] = $font->family;
    }
    return $google_fonts;
}

function getAllGoogleFontVariants($family)
{

    $variants = array();

    $url = "https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyCxxdoGtiswt3jFSTTrCt0SX24ufU0J4NU";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  // Disable SSL verification
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    $font_response = curl_exec($ch);
    curl_close($ch);

    $json = json_decode($font_response);
    $fonts = $json->items;
    foreach ($fonts as $font) {
        if ($family == $font->family) {
            foreach ($font->variants as $variant) {
                $toPush =  $variant;
                if ($variant == "regular") {
                    $toPush = "400";
                }
                if ($variant == "italic") {
                    $toPush = "400i";
                }
                array_push($variants, $toPush);
            }
            break;
        }
    }
    return $variants;
}


function ml_customise_enqueue_scripts_back_end($hook_suffix)
{
    if ($hook_suffix != "widgets.php") {
        return;
    }
    wp_enqueue_script('ml-customise-ajax-script', get_template_directory_uri() . '/admin/customise/js/acf-admin-script.js', array('jquery'));
    wp_localize_script('ml-customise-ajax-script', 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
}
add_action('admin_enqueue_scripts', 'ml_customise_enqueue_scripts_back_end');

// Same handler function...
add_action('wp_ajax_customise_font_action', 'customise_font_action');
// add_action( 'wp_ajax_nopriv_customise_font_action', 'customise_font_action' );
function customise_font_action()
{

    $font_family_chosen = $_POST["fontFamily"];
    $allVariants = getAllGoogleFontVariants($font_family_chosen);
    echo json_encode($allVariants);
    wp_die();
}
