<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

//add menus
add_action('acf/init', function () {
    if (function_exists('acf_add_options_page')) {
        acf_add_options_page(array(
            'page_title'    => __('Theme settings'),
            'menu_title'    => __('Theme settings'),
            'menu_slug'     => 'theme-settings',
            'capability'    => 'edit_posts',
            'redirect'      => true
        ));
        acf_add_options_sub_page(array(
            'page_title'    => 'Styles',
            'menu_title'    => 'Styles',
            'parent_slug'   => 'theme-settings',
        ));
        acf_add_options_sub_page(array(
            'page_title'    => 'Settings',
            'menu_title'    => 'Settings',
            'parent_slug'   => 'theme-settings',
        ));
    }
});

//styles
$colors = new FieldsBuilder('colors');
$colors
    ->addTab("colors")
    ->addColorPicker("primary_color", array(
        'default_value' => '#F5BC51',
    ))
    ->addColorPicker("secondary_color", array(
        'default_value' => '#1D1D1B',
    ))
    ->addColorPicker("tertiary_color", array(
        'default_value' => '#F6F7F9',
    ))
    ->addColorPicker("quaternary_color", array(
        'default_value' => '#D3D6D8',
    ))
    ->addColorPicker("quinary_color", array(
        'default_value' => '#FCFCFC',
    ));

$typography = new FieldsBuilder('typography');
$typography
    ->addTab("typography")
    ->addSelect("fonts", array(
        "multiple" => 1,
        "ui" => 1,
        "choices" => array(
            "Inter" => "Inter",
            "Merriweather" => "Merriweather",
            "Roboto" => "Roboto",
            "Montserrat" => "Montserrat",
            "Lato" => "Lato",
            "Open+Sans" => "Open Sans",
            "Poppins" => "Poppins",
            "Raleway" => "Raleway",
            "Roboto+Condensed" => "Roboto Condensed",
            "Roboto+Slab" => "Roboto Slab",
            "Source+Sans+Pro" => "Source Sans Pro",
            "Ubuntu" => "Ubuntu",
            "Work+Sans" => "Work Sans",
            "TTRamillas" => "TTRamillas",
            "DM+Sans" => "DM Sans",
            "DM+Sans+Medium" => "DM Sans Medium",
            "Fjalla+One" => "Fjalla One",
            "Patrick+Hand" => "Patrick Hand",
        )
    ))
    ->addColorPicker("default_text_color", array(
        'default_value' => '#1D1D1B',
        'instructions' => 'Default text color for body and headings. This can be changed on a per block basis.',
    ))
    ->addGroup("heading_sizes")
    ->addAccordion("h1", array(
        'open' => 0,
    ))
    ->addNumber("h1_lg", array('wrapper' => array("width" => 50), "default_value" => 70))
    ->addNumber("h1_lg_lineheight", array('wrapper' => array("width" => 50)))
    ->addNumber("h1_md", array('wrapper' => array("width" => 50), "default_value" => 50))
    ->addNumber("h1_md_lineheight", array('wrapper' => array("width" => 50)))
    ->addNumber("h1_sm", array('wrapper' => array("width" => 50), "default_value" => 37))
    ->addNumber("h1_sm_lineheight", array('wrapper' => array("width" => 50)))
    ->addAccordion("h1_end")->endpoint()
    ->addAccordion("h2", array(
        'open' => 0,
    ))
    ->addNumber("h2_lg", array('wrapper' => array("width" => 50), "default_value" => 50))
    ->addNumber("h2_lg_lineheight", array('wrapper' => array("width" => 50)))
    ->addNumber("h2_md", array('wrapper' => array("width" => 50), "default_value" => 40))
    ->addNumber("h2_md_lineheight", array('wrapper' => array("width" => 50)))
    ->addNumber("h2_sm", array('wrapper' => array("width" => 50), "default_value" => 30))
    ->addNumber("h2_sm_lineheight", array('wrapper' => array("width" => 50)))
    ->addAccordion("h2_end")->endpoint()
    ->addAccordion("h3", array(
        'open' => 0,
    ))
    ->addNumber("h3_lg", array('wrapper' => array("width" => 50), "default_value" => 25))
    ->addNumber("h3_lg_lineheight", array('wrapper' => array("width" => 50)))
    ->addNumber("h3_md", array('wrapper' => array("width" => 50), "default_value" => 22))
    ->addNumber("h3_md_lineheight", array('wrapper' => array("width" => 50)))
    ->addNumber("h3_sm", array('wrapper' => array("width" => 50), "default_value" => 20))
    ->addNumber("h3_sm_lineheight", array('wrapper' => array("width" => 50)))
    ->addAccordion("h3_end")->endpoint()
    ->addAccordion("h4", array(
        'open' => 0,
    ))
    ->addNumber("h4_lg", array('wrapper' => array("width" => 50), "default_value" => 60))
    ->addNumber("h4_lg_lineheight", array('wrapper' => array("width" => 50)))
    ->addNumber("h4_md", array('wrapper' => array("width" => 50), "default_value" => 47))
    ->addNumber("h4_md_lineheight", array('wrapper' => array("width" => 50)))
    ->addNumber("h4_sm", array('wrapper' => array("width" => 50), "default_value" => 40))
    ->addNumber("h4_sm_lineheight", array('wrapper' => array("width" => 50)))
    ->addAccordion("h4_end")->endpoint()
    ->addAccordion("h5", array(
        'open' => 0,
    ))
    ->addNumber("h5_lg", array('wrapper' => array("width" => 50), "default_value" => 14))
    ->addNumber("h5_lg_lineheight", array('wrapper' => array("width" => 50)))
    ->addNumber("h5_md", array('wrapper' => array("width" => 50), "default_value" => 14))
    ->addNumber("h5_md_lineheight", array('wrapper' => array("width" => 50)))
    ->addNumber("h5_sm", array('wrapper' => array("width" => 50), "default_value" => 14))
    ->addNumber("h5_sm_lineheight", array('wrapper' => array("width" => 50)))
    ->addAccordion("h5_end")->endpoint()
    ->addAccordion("h6", array(
        'open' => 0,
    ))
    ->addNumber("h6_lg", array('wrapper' => array("width" => 50), "default_value" => 12))
    ->addNumber("h6_lg_lineheight", array('wrapper' => array("width" => 50)))
    ->addNumber("h6_md", array('wrapper' => array("width" => 50), "default_value" => 12))
    ->addNumber("h6_md_lineheight", array('wrapper' => array("width" => 50)))
    ->addNumber("h6_sm", array('wrapper' => array("width" => 50), "default_value" => 12))
    ->addNumber("h6_sm_lineheight", array('wrapper' => array("width" => 50)))
    ->addAccordion("h6_end")->endpoint()
    ->endGroup();

$header = new FieldsBuilder('header');
$header
    ->addTab("header")
    ->addImage("sitelogo", array(
        'instructions' => 'Upload a 200x200px PNG image',
        'return_format' => 'array',
        'preview_size' => 'thumbnail',
        'library' => 'all',
        'width' => '200',
        'height' => '200',
    ))
    ->addSelect("header_type")
    ->addChoices(['1' => '1 (simple)'], ['2' => '2 (new)'])
    ->addColorPicker("header_bg_color", array(
        'default_value' => '#F5BC51',
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'header_overlay_enabled',
                    'operator' => '==',
                    'value' => '0',
                ),
            ),
        ),
    ))
    ->addTrueFalse("header_border_bottom_enabled", array(
        'default_value' => 0,
    ))
    ->addColorPicker("header_border_bottom_color", array(
        'default_value' => '#F6F7F9',
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'header_border_bottom_enabled',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        ),
    ))
    ->addNumber("header_border_bottom_width", array(
        'default_value' => 1,
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'header_border_bottom_enabled',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        ),
    ))
    ->addColorPicker("header_text_color", array(
        'default_value' => '#1D1D1B',
    ))
    ->addColorPicker("header_text_hover_color", array(
        'default_value' => '#1D1D1B',
    ))
    ->addNumber("header_width", array(
        'default_value' => 2000
    ))
    ->addGroup("header_mobile_menu")
    ->addImage("mobile_logo_replacement")
    ->addColorPicker("hamburger_color")
    ->addColorPicker("hamburger_close_color")
    ->addTrueFalse("show_mobile_menu_social_nav")
    ->addText("mobile_social_text",  array(
        'default_value' => 'Follow us',
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'show_mobile_menu_social_nav',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        )
    ))
    ->endGroup()
    ->addGroup("header_language_switcher", array(
        'label' => 'Language Switcher',
    ))
    ->addTrueFalse("enabled", array(
        'default_value' => 0,
    ))
    ->addText("text", array(
        'default_value' => 'Check out our sister company',
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'enabled',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        ),
    ))
    ->addRepeater("links", array(
        "layout" => "block",
        "button_label" => "Add link",
        "min" => 0,
        "max" => 5,
        "conditional_logic" => array(
            array(
                array(
                    'field' => 'enabled',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        ),
    ))
    ->addImage("icon")
    ->addLink("link")
    ->endRepeater()
    ->endGroup();

$footer = new FieldsBuilder('footer');
$footer
    ->addTab("footer")
    ->addSelect("footer_type")
    ->addChoices(['1' => '1 (simple)'], ['2' => '2 (Decorative swoosh)'])
    ->addImage("footer_logo")
    ->addWysiwyg("footer_text")
    ->addColorPicker("footer_bg_color", array(
        'default_value' => '#F5BC51',
    ))
    ->addColorPicker("footer_nav_color", array(
        'default_value' => '#1D1D1B',
    ))
    ->addColorPicker("footer_social_text_color", array(
        'default_value' => '#1D1D1B',
    ));


$buttons = new FieldsBuilder('buttons');
$buttons
    ->addTab("buttons")
    ->addRepeater("buttons", array(
        "layout" => "block",
        "button_label" => "Add button config",
        "min" => 1,
        "max" => 2,
    ))
    ->addColorPicker("btn_bg_color", array(
        'default_value' => '#F5BC51',
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addColorPicker("btn_hover_bg_color", array(
        'default_value' => '#1D1D1B',
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addColorPicker("btn_text_color", array(
        'default_value' => '#1D1D1B',
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addColorPicker("btn_hover_text_color", array(
        'default_value' => '#F5BC51',
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addColorPicker("btn_border_color", array(
        'default_value' => '#1D1D1B',
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->endRepeater()
    ->addNumber("btn_border_radius", array(
        'default_value' => 50,
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addNumber("btn_border_width", array(
        'default_value' => 2,
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addNumber("btn_padding_x", array(
        'default_value' => 20,
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addNumber("btn_mobile_padding_x", array(
        'default_value' => 10,
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addNumber("btn_padding_y", array(
        'default_value' => 10,
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addNumber("btn_mobile_padding_y", array(
        'default_value' => 7,
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addSelect('btn_transition', array(
        'choices' => array(
            'none' => 'none',
            'fade' => 'fade',
            'grow' => 'grow',
            'expandRight' => 'expandRight',
        ),
        'default_value' => 'fade',
        'wrapper' => array(
            'width' => '20',
        ),
    ));

$pill = new FieldsBuilder('pill');
$pill
    ->addTab("pills")
    ->addRepeater("pills", array(
        "layout" => "block",
        "button_label" => "Add pill config",
        "min" => 1,
        "max" => 2
    ))
    ->addColorPicker("pill_bg_color", array(
        'default_value' => '#F5BC51',
        'wrapper' => array(
            'width' => '25',
        ),
    ))
    ->addColorPicker("pill_text_color", array(
        'default_value' => '#1D1D1B',
        'wrapper' => array(
            'width' => '25',
        ),
    ))
    ->endRepeater()
    ->addNumber("pill_border_radius", array(
        'default_value' => 50,
    ));


$border_radius = new FieldsBuilder('border_radius');
$border_radius
    ->addNumber("border_radius_2xl", array(
        'default_value' => 80,
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addNumber("border_radius_xl", array(
        'default_value' => 50,
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addNumber("border_radius_lg", array(
        'default_value' => 30,
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addNumber("border_radius_md", array(
        'default_value' => 20,
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addNumber("border_radius_sm", array(
        'default_value' => 10,
        'wrapper' => array(
            'width' => '20',
        ),
    ));

$container = new FieldsBuilder('container');
$container
    ->addNumber("container_2xl", array(
        'default_value' => 1700,
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addNumber("container_xl", array(
        'default_value' => 1500,
        'wrapper' => array(
            'width' => '20',
        ),

    ))
    ->addNumber("container_lg", array(
        'default_value' => 1300,
        'instructions' => 'This is the value for main group container',
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addNumber("container_md", array(
        'default_value' => 1100,
        'wrapper' => array(
            'width' => '20',
        ),
    ))
    ->addNumber("container_sm", array(
        'default_value' => 900,
        'wrapper' => array(
            'width' => '20',
        ),
    ));

$tailwind = new FieldsBuilder('tailwind');
$tailwind
    ->addTab("tailwind")
    ->addAccordion("border_radius", array(
        'open' => 0,
    ))
    ->addFields($border_radius)
    ->addAccordion("border_radius_acc_end")->endpoint()
    ->addAccordion("container", array(
        'open' => 0,
    ))
    ->addFields($container)
    ->addAccordion("container_end")->endpoint();


$animation = new FieldsBuilder('animation');
$animation
    ->addTab("animation")
    ->addTrueFalse("animations_enable")
    ->addMessage('animation_instructions', 'possible options are .animation-fade, .animation-fade-up, .animation-fade-down, .animation-fade-left, .animation-fade-right, .animation-fade-up-right, .animation-fade-up-left, .animation-fade-down-right, .animation-fade-down-left, .animation-flip-up, .animation-flip-down, .animation-flip-left, .animation-flip-right, .animation-slide-up, .animation-slide-down, .animation-slide-left, .animation-slide-right, .animation-zoom-in, .animation-zoom-in-up, .animation-zoom-in-down, .animation-zoom-in-left, .animation-zoom-in-right, .animation-zoom-out, .animation-zoom-out-up, .animation-zoom-out-down, .animation-zoom-out-left, .animation-zoom-out-right', array(
        'label' => 'Instructions',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'animations_enable',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        ),
        'message' => 'possible options are .animation-fade, .animation-fade-up, .animation-fade-down, .animation-fade-left, .animation-fade-right, .animation-fade-up-right, .animation-fade-up-left, .animation-fade-down-right, .animation-fade-down-left, .animation-flip-up, .animation-flip-down, .animation-flip-left, .animation-flip-right, .animation-slide-up, .animation-slide-down, .animation-slide-left, .animation-slide-right, .animation-zoom-in, .animation-zoom-in-up, .animation-zoom-in-down, .animation-zoom-in-left, .animation-zoom-in-right, .animation-zoom-out, .animation-zoom-out-up, .animation-zoom-out-down, .animation-zoom-out-left, .animation-zoom-out-right,',
        'new_lines' => 'wpautop', // 'wpautop', 'br', '' no formatting
        'esc_html' => 0,
    ));

$cookie_notice = new FieldsBuilder('cookie_notice');
$cookie_notice
    ->addTab("cookie_notice")
    ->addTrueFalse("cookie_notice_enabled", array(
        'default_value' => 1,
    ))
    ->addImage("cookie_notice_icon", array(
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'cookie_notice_enabled',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        ),
    ))
    ->addText("cookie_notice_heading", array(
        'default_value' => 'We use cookies',
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'cookie_notice_enabled',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        ),
    ))
    ->addWysiwyg("cookie_notice_text", array(
        'default_value' => 'This site uses services that uses cookies to deliver better experience and analyze traffic. You can learn more about the services we use at our <a href="/privacy-policy">privacy policy</a>.',
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'cookie_notice_enabled',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        ),
    ))
    ->addText("cookie_notice_accept_text", array(
        'default_value' => 'Accept',
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'cookie_notice_enabled',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        ),
    ))
    ->addText("cookie_notice_reject_text", array(
        'default_value' => 'Reject',
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'cookie_notice_enabled',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        ),
    ))
    ->addColorPicker("cookie_notice_bg_color", array(
        'default_value' => '#fcfcfc',
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'cookie_notice_enabled',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        ),
    ));

$favicon = new FieldsBuilder('favicon');
$favicon
    ->addTab("favicon")
    ->addImage("favicon", array(
        'instructions' => 'Upload a 16x16px PNG image',
        'return_format' => 'array',
        'preview_size' => 'thumbnail',
        'library' => 'all',
        'width' => '16',
        'height' => '16',
    ));


$global = new FieldsBuilder('Settings');
$global
    ->addFields($colors)
    ->addFields($typography)
    ->addFields($header)
    ->addFields($footer)
    ->addFields($tailwind)
    ->addFields($buttons)
    ->addFields($pill)
    ->addFields($animation)
    ->addFields($cookie_notice)
    ->addFields($favicon)
    ->setLocation('options_page', '==', 'acf-options-styles')
    ->setGroupConfig('style', 'seamless');

add_action('acf/init', function () use ($global) {
    acf_add_local_field_group($global->build());
});

//add certain settings to indivudal pages as well
$post_settings = new FieldsBuilder('Post style settings');
$post_settings
    ->addTrueFalse('enable_settings_overrides')
    ->addGroup('post_style_settings')
    ->conditional('enable_settings_overrides', '==', '1')
    ->addFields($header)
    ->addFields($footer)
    ->addFields($container)
    ->endGroup()
    ->setLocation('post_type', '==', 'page')
    ->setGroupConfig('style', 'seamless');

add_action('acf/init', function () use ($post_settings) {
    acf_add_local_field_group($post_settings->build());
});

//general settings
$google_tag_manager = new FieldsBuilder('google_tag_manager');
$google_tag_manager
    ->addTab("google_tag_manager")
    ->addTrueFalse("google_tag_manager_enabled", array(
        'default_value' => 1,
    ))
    ->addText("google_tag_manager_id", array(
        'default_value' => 'GTM-XXXXXXX',
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'google_tag_manager_enabled',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        ),
    ));

$post_types = new FieldsBuilder('post_types');
$post_types
    ->addTab("post_types")
    ->addSelect("enabled_post_types", array(
        "multiple" => 1,
        "ui" => 1,
        "choices" => array("mgas", "clients", "event", "team", "testimonial")
    ));

$page_404 = new FieldsBuilder('page_404');
$page_404
    ->addTab("404")
    ->addPostObject('page_404', [
        'label' => 'Show page',
        'instructions' => 'This will show content from this page, with the default header and footer',
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'enable_page_404',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        ),
        'post_type' => ['page'],
        'ui' => 1,
    ]);

$coming_soon = new FieldsBuilder('coming_soon');
$coming_soon
    ->addTab("coming_soon")
    ->addTrueFalse("enable_coming_soon")
    ->addPostObject('coming_soon_page', [
        'label' => 'Show page',
        'instructions' => 'This will show content from this page, without any header and footer',
        'conditional_logic' => array(
            array(
                array(
                    'field' => 'enable_coming_soon',
                    'operator' => '==',
                    'value' => '1',
                ),
            ),
        ),
        'post_type' => ['page'],
        'ui' => 1,
    ]);

$settings = new FieldsBuilder('General-settings');
$settings
    ->addFields($google_tag_manager)
    ->addFields($post_types)
    ->addFields($page_404)
    ->addFields($coming_soon)
    ->setLocation('options_page', '==', 'acf-options-settings')
    ->setGroupConfig('style', 'seamless');

add_action('acf/init', function () use ($settings) {
    acf_add_local_field_group($settings->build());
});




function mytheme_setup_theme_supported_features()
{
    add_theme_support('editor-color-palette', array(
        array(
            'name'  => 'Primary',
            'slug'  => 'primary',
            'color' => get_field("primary_color", "option"),
        ),
        array(
            'name'  => 'Secondary',
            'slug'  => 'secondary',
            'color' => get_field("secondary_color", "option"),
        ),
        array(
            'name'  => 'Tertiary',
            'slug'  => 'tertiary',
            'color' => get_field("tertiary_color", "option"),
        ),
        array(
            'name'  => 'Quaternary',
            'slug'  => 'quaternary',
            'color' => get_field("quaternary_color", "option"),
        ),
        array(
            'name'  => 'Quinary',
            'slug'  => 'quinary',
            'color' => get_field("quinary_color", "option"),
        ),
    ));
}

add_action('after_setup_theme', 'mytheme_setup_theme_supported_features');
