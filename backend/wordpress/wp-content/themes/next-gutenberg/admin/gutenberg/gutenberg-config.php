<?php

function add_editor_styles()
{
    add_theme_support('editor-styles');
    add_editor_style('admin/gutenberg/styles/gutenberg-editor-edits.css');
    add_editor_style('style.css');
}
add_action('admin_init', 'add_editor_styles');
