<?php


add_action('after_setup_theme', 'ng_gutenberg_css');
function ng_gutenberg_css()
{
    add_theme_support('editor-styles'); // if you don't add this line, your stylesheet won't be added
    add_editor_style(get_stylesheet_directory_uri() . "/admin/gutenberg/styles/gutenberg-editor-edits.css");
    add_editor_style(get_stylesheet_directory_uri() . "/admin/gutenberg/styles/gutenberg-copied-styles.css");
}


//this mirrors the styles.tsx file in the frontend dir. 
add_action('admin_head', 'admin_styles', 100);
function admin_styles()
{
    $primary_color = get_field('primary_color', 'option');
    $secondary_color = get_field('secondary_color', 'option');
    $tertiary_color = get_field('tertiary_color', 'option');
    $quaternary_color = get_field('quaternary_color', 'option');
    $default_text_color = get_field('default_text_color', 'option');
    $fonts = get_field('fonts', 'option');
    $heading_sizes = get_field('heading_sizes', 'option');
    $header_bg_color = get_field('header_bg_color', 'option');
    $header_border_bottom_color = get_field('header_border_bottom_color', 'option');
    $header_border_bottom_width = get_field('header_border_bottom_width', 'option');
    $header_text_color = get_field('header_text_color', 'option');
    $header_position = get_field('header_position', 'option');
    $header_width = get_field('header_width', 'option');
    $header_border_bottom_enabled = get_field("header_border_bottom_enabled", "option");
    $header_overlay_enabled = get_field("header_overlay_enabled", "option");
    $container_width = get_field('container_width', 'option');
    $buttons = get_field('buttons', 'option');
    $pills = get_field('pills', 'option');
    $pill_border_radius = get_field('pill_border_radius', 'option');
    $btn_border_radius = get_field('btn_border_radius', 'option');
    $btn_border_width = get_field('btn_border_width', 'option');
    $btn_padding_x = get_field('btn_padding_x', 'option');
    $btn_padding_y = get_field('btn_padding_y', 'option');
    $btn_mobile_padding_x = get_field('btn_mobile_padding_x', 'option');
    $btn_mobile_padding_y = get_field('btn_mobile_padding_y', 'option');
    $btn_transition = get_field('btn_transition', 'option');
    $border_radius_sm = get_field('border_radius_sm', 'option');
    $border_radius_md = get_field('border_radius_md', 'option');
    $border_radius_lg = get_field('border_radius_lg', 'option');
    $border_radius_xl = get_field('border_radius_xl', 'option');
    $border_radius_2xl = get_field('border_radius_2xl', 'option');
    $cookie_notice_bg_color = get_field('cookie_notice_bg_color', 'option');

?>

    <style>
        :root {
            --color-primary: <?php echo $primary_color; ?>;
            --color-secondary: <?php echo $secondary_color; ?>;
            --color-tertiary: <?php echo $tertiary_color; ?>;
            --color-quaternary: <?php echo $quaternary_color; ?>;
            --default-text-color: <?php echo $default_text_color; ?>;
            --font-primary: <?php echo $fonts[0]; ?>;
            --font-secondary: <?php echo isset($fonts[1]) ? $fonts[1] : $fonts[0]; ?>;
            --font-tertiary: <?php echo isset($fonts[2]) ? $fonts[2] : ''; ?>;
            --h1-lg: <?php echo $heading_sizes['h1_lg']; ?>px;
            --h1-md: <?php echo $heading_sizes['h1_md']; ?>px;
            --h1-sm: <?php echo $heading_sizes['h1_sm']; ?>px;
            --h2-lg: <?php echo $heading_sizes['h2_lg']; ?>px;
            --h2-md: <?php echo $heading_sizes['h2_md']; ?>px;
            --h2-sm: <?php echo $heading_sizes['h2_sm']; ?>px;
            --h3-lg: <?php echo $heading_sizes['h3_lg']; ?>px;
            --h3-md: <?php echo $heading_sizes['h3_md']; ?>px;
            --h3-sm: <?php echo $heading_sizes['h3_sm']; ?>px;
            --h4-lg: <?php echo $heading_sizes['h4_lg']; ?>px;
            --h4-md: <?php echo $heading_sizes['h4_md']; ?>px;
            --h4-sm: <?php echo $heading_sizes['h4_sm']; ?>px;
            --h5-lg: <?php echo $heading_sizes['h5_lg']; ?>px;
            --h5-md: <?php echo $heading_sizes['h5_md']; ?>px;
            --h5-sm: <?php echo $heading_sizes['h5_sm']; ?>px;
            --h6-lg: <?php echo $heading_sizes['h6_lg']; ?>px;
            --h6-md: <?php echo $heading_sizes['h6_md']; ?>px;
            --h6-sm: <?php echo $heading_sizes['h6_sm']; ?>px;
            --header-bg-color: <?php echo $header_bg_color; ?>;
            --header-border-bottom-color: <?php echo $header_border_bottom_color; ?>;
            --header-border-bottom-width: <?php echo $header_border_bottom_width; ?>px;
            --header-text-color: <?php echo $header_text_color; ?>;
            --header-position: <?php echo $header_position; ?>;
            --header-width: <?php echo $header_width; ?>px;
            --container-width: <?php echo $container_width; ?>px;
            --btn1-bg-color: <?php echo $buttons[0]['btn_bg_color']; ?>;
            --btn1-hover-bg-color: <?php echo $buttons[0]['btn_hover_bg_color']; ?>;
            --btn1-text-color: <?php echo $buttons[0]['btn_text_color']; ?>;
            --btn1-hover-text-color: <?php echo $buttons[0]['btn_hover_text_color']; ?>;
            --btn1-border-color: <?php echo $buttons[0]['btn_border_color']; ?>;
            --btn2-bg-color: <?php echo $buttons[1]['btn_bg_color']; ?>;
            --btn2-hover-bg-color: <?php echo $buttons[1]['btn_hover_bg_color']; ?>;
            --btn2-text-color: <?php echo $buttons[1]['btn_text_color']; ?>;
            --btn2-hover-text-color: <?php echo $buttons[1]['btn_hover_text_color']; ?>;
            --btn2-border-color: <?php echo $buttons[1]['btn_border_color']; ?>;
            --btn-border-radius: <?php echo $btn_border_radius; ?>px;
            --btn-border-width: <?php echo $btn_border_width; ?>px;
            --btn-padding-x: <?php echo $btn_padding_x; ?>px;
            --btn-padding-y: <?php echo $btn_padding_y; ?>px;
            --btn-mobile-padding-x: <?php echo $btn_mobile_padding_x; ?>px;
            --btn-mobile-padding-y: <?php echo $btn_mobile_padding_y; ?>px;
            --btn-transition: <?php echo $btn_transition; ?>;
            --pill1-bg-color: <?php echo $pills[0]['pill_bg_color']; ?>;
            --pill1-text-color: <?php echo $pills[0]['pill_text_color']; ?>;
            --pill2-bg-color: <?php echo $pills[1]['pill_bg_color']; ?>;
            --pill2-text-color: <?php echo $pills[1]['pill_text_color']; ?>;
            --pill-border-radius: <?php echo $pill_border_radius; ?>;
            --border-radius-sm: <?php echo $border_radius_sm; ?>px;
            --border-radius-md: <?php echo $border_radius_md; ?>px;
            --border-radius-lg: <?php echo $border_radius_lg; ?>px;
            --border-radius-xl: <?php echo $border_radius_xl; ?>px;
            --border-radius-2xl: <?php echo $border_radius_2xl; ?>px;
            --cookie-notice-bg-color: <?php echo $cookie_notice_bg_color; ?>;
        }
    </style>

<?php

}
