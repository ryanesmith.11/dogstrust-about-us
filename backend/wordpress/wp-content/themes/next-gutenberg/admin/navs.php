<?php

//------------------------------------------------
// ** NAVS **
//------------------------------------------------

// register standard ones.
if (function_exists('wp_nav_menu')) {
    add_theme_support('nav-menus');
    register_nav_menus(
        array(
            'header_nav' => __('Header Nav', 'default_themes'),
            'footer_nav' => __('Footer Nav', 'default_themes'),
            'footer_social_nav' => __('Footer Social Nav', 'default_themes'),
            'footer_nav_alt' => __('Footer Nav Alternate', 'default_themes'),
            'mobile_nav' => __('Mobile Nav', 'default_themes'),
        )
    );
}
