<?php

//------------------------------------------------
// ** THEME SCRIPTS, STYLES AND SUPPORTS **
//------------------------------------------------

function add_theme_supports()
{
    // This theme uses post thumbnails
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');
    // Add default posts and comments RSS feed links to head
    add_theme_support('automatic-feed-links');
}
add_action('after_setup_theme', 'add_theme_supports');


function my_own_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg';
    $mimes['svg+xml'] = 'image/svg+xml';

    return $mimes;
}
add_filter('upload_mimes', 'my_own_mime_types');

//------------------------------------------------
// ** NAVS **
//-----------------------------------------------
require_once get_template_directory() . "/admin/navs.php";

//------------------------------------------------
// ** FIELDS **
//-----------------------------------------------

//acf builder plugin, see cheatsheet https://github.com/Log1x/acf-builder-cheatsheet
require_once get_template_directory() . "/includes/acf-builder/autoload.php";


//------------------------------------------------
// ** GUTENBERG **
//-----------------------------------------------
require_once get_template_directory() . "/admin/gutenberg/gutenberg-config.php";
require_once get_template_directory() . "/admin/gutenberg/admin-styles.php";
require_once get_template_directory() . "/gutenberg/register-blocks.php";

//show only some gutenberg blocks
add_filter('allowed_block_types_all', 'allowed_block_types', 25, 2);
function allowed_block_types($allowed_blocks, $editor_context)
{
    return array(
        'core/block', //this allows reusable blocks
        'core/image',
        'core/video',
        'core/paragraph',
        'core/quote',
        'core/list',
        'core/list-item',
        'core/group',
        'core/separator',
        'core/spacer',
        'core/buttons',
        'core/columns',
        'core/heading',
        'core/cover',
        'acf/hero',
        'acf/hero2',
        'acf/slider',
        'acf/slider2',
        'acf/slider3',
        'acf/logo-slider',
        'acf/testimonials',
        'acf/post-feed',
        'acf/marketo',
        'acf/post-feed2',
        'acf/post-feed3',
        'acf/post-feed4',
        'acf/pricing-summary',
        'acf/calendly',
        'acf/toggle-menu',
        'acf/map',
        'gravityforms/form',
        'pb/accordion-item',
        'outermost/icon-block'
    );
}


//------------------------------------------------
// ** API **
//-----------------------------------------------
require_once get_template_directory() . "/api.php";


//------------------------------------------------
// ** CPTS **
//-----------------------------------------------
require_once get_template_directory() . "/cpt/dogs.php";




//------------------------------------------------
// ** THEME UPDATE CHECKER **
//------------------------------------------------
// require_once get_template_directory() . '/includes/plugin-update-checker/plugin-update-checker.php';
// $myUpdateChecker = Puc_v4_Factory::buildUpdateChecker('bitbucketr_url_here', __FILE__, 'pome-theme');
//Optional: If you're using a private repository, specify the access token like this:
// $myUpdateChecker->setAuthentication('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');
//Optional: Set the branch that contains the stable release.
// $myUpdateChecker->setBranch('stable-branch-name');
// var_dump(get_field("logos_placeholder_image", "option"));



//------------------------------------------------
// ** THEME REQUIREMENTS CHECKER **
//------------------------------------------------
add_action('admin_notices', 'my_theme_dependencies');

function my_theme_dependencies()
{
    if (!function_exists('get_field'))
        echo '<div class="error"><p>' . __('Warning: The theme needs ACF to function', 'my-theme') . '</p></div>';
}



//------------------------------------------------
// ** Theme settings **
//------------------------------------------------
require_once get_template_directory() . "/admin/theme-settings/theme-settings.php";

//------------------------------------------------
// ** Extra hooks **
//------------------------------------------------

//remove comments from admin menu
add_action('admin_init', 'my_remove_admin_menus');
function my_remove_admin_menus()
{
    remove_menu_page('edit-comments.php');
}

add_action('admin_init', 'loadtinymce');
function loadtinymce()
{
    wp_enqueue_script('tiny_mce');
}



/**
 * Registers an editor stylesheet for the theme.
 */
function add_script_to_menu_page()
{
    // $pagenow, is a global variable referring to the filename of the current page, 
    // such as ‘admin.php’, ‘post-new.php’
    global $pagenow;

    // wp_register_script('tailwind', 'https://cdn.tailwindcss.com');
    // wp_enqueue_script('tailwind');
    add_theme_support('editor-styles');
    $font_string = "?family=" . implode("&family=", (array)get_field("fonts", "option"));
    wp_enqueue_style(
        // Handle
        'fonts',
        // CSS URL
        'https://fonts.googleapis.com/css2' . $font_string . '&display=swap',
        [],
        null
    );

    if ($pagenow != 'nav-menus.php') {
        return;
    }
}

add_action('admin_enqueue_scripts', 'add_script_to_menu_page');



//allow editors to use vercel plugin
add_action('wp_loaded', function () {
    $role = get_role('editor');
    $role->add_cap('vercel_deploy_capability', true);
    // $role->add_cap('vercel_adjust_settings_capability', true);
});


function klf_acf_input_admin_footer()
{ ?>
    <script type="text/javascript">
        (function($) {
            acf.add_filter('color_picker_args', function(args, $field) {
                // add the hexadecimal codes here for the colors you want to appear as swatches
                args.palettes = ['<?php echo get_field("primary_color", "option"); ?>', '<?php echo get_field("secondary_color", "option"); ?>', '<?php echo get_field("tertiary_color", "option"); ?>', '<?php echo get_field("quaternary_color", "option"); ?>', '<?php echo get_field("quinary_color", "option"); ?>']
                // return colors
                return args;
            });
        })(jQuery);
    </script>
<?php }
add_action('acf/input/admin_footer', 'klf_acf_input_admin_footer');


//adding js hooks
function block_editor_scripts()
{
    wp_enqueue_style(
        'block-settings',
        get_theme_file_uri() . '/js/theme-bundle.min.js',
        array('jquery', 'wp-hooks'),
    );
}
add_action("enqueue_block_editor_assets", "block_editor_scripts");


//patch for annoying gutenberg editor issue
function richedit_wp_patch()
{
    add_filter('user_can_richedit', '__return_true');
}
add_action('init', 'richedit_wp_patch', 9);



//

function example_register_custom_icons()
{
    wp_enqueue_script(
        'example-register-custom-icons',
        get_theme_file_uri('/js/register-custom-icons.js'),
        array('wp-i18n', 'wp-hooks', 'wp-dom'),
        wp_get_theme()->get('Version'),
        true // Very important, otherwise the filter is called too early.
    );
}
add_action('enqueue_block_editor_assets', 'example_register_custom_icons');



add_filter('wp_get_attachment_image_src', 'fix_wp_get_attachment_image_svg', 10, 4);  /* the hook */
function fix_wp_get_attachment_image_svg($image, $attachment_id, $size, $icon)
{
    if (is_array($image) && preg_match('/\.svg$/i', $image[0]) && $image[1] == 1) {
        if (is_array($size)) {
            $image[1] = $size[0];
            $image[2] = $size[1];
        } elseif (($xml = simplexml_load_file($image[0])) !== false) {
            $attr = $xml->attributes();
            $viewbox = explode(' ', $attr->viewBox);
            $image[1] = isset($attr->width) && preg_match('/\d+/', $attr->width, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[2] : null);
            $image[2] = isset($attr->height) && preg_match('/\d+/', $attr->height, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[3] : null);
        } else {
            $image[1] = $image[2] = null;
        }
    }
    return $image;
}


// add_filter('wp_get_attachment_image', function ($html, $attachment_id, $size, $icon, $attr) {
//     // var_dump($attr);
//     error_log(print_r($attr['src'], true) . "   size::::::::::::::::::::::::::::::::::" . $value);
//     if (($xml = simplexml_load_file($attr['src'])) !== false) {
//         $attrs = $xml->attributes();
//         $viewbox = explode(' ', $attrs->viewBox);
//         $image[1] = isset($attrs->width) && preg_match('/\d+/', $attrs->width, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[2] : null);
//         $image[2] = isset($attrs->height) && preg_match('/\d+/', $attrs->height, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[3] : null);
//         error_log(print_r($attr['src'], true) . "   size::::::::::::::::::::::::::::::::::" . $viewbox[2]);
//         $attr['dim'] = array($viewbox[2], $viewbox[3]);
//     }
//     return $attr;
// }, 5, 10);

// Function to save options on theme activation
// function save_theme_options_on_activation()
// {
//     $options = array(
//         'favicon' => false,
//         'sitelogo' => array(
//             'ID' => 90,
//             'id' => 90,
//             'title' => 'Mission Logo',
//             // ... rest of the sitelogo data
//         ),
//         'fonts' => array(
//             'Inter',
//             'Merriweather'
//         ),
//         'primary_color' => '#f5bc51',
//         'secondary_color' => '#1d1d1b',
//         // ... rest of the options
//     );

//     // Loop through the options and update each field
//     foreach ($options as $field => $value) {
//         update_field($field, $value, 'option');
//     }
// }

// Hook the function to run on theme activation
// add_action('after_setup_theme', 'save_theme_options_on_activation');
