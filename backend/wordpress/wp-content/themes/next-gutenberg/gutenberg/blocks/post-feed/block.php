<?php

$posts = get_posts(array(
    'numberposts' => get_field("number_of_posts"),
    'post_type' => get_field("post_type"),
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
));
?>

<div class="post-feed mb-[10px]">
    <?php if (get_field("show_search") || get_field("show_categories")) : ?>
        <div class="news-articles-top flex flex-col pb-[30px]">
            <?php if (get_field("show_search")) : ?>
                <div class="md:w-4/12 w-full"><input class="w-full max-w-[475px] h-[34px] px-[20px] py-[27px] border-[1px] border-solid border-secondary rounded-full" placeholder="Search"></div>
            <?php endif; ?>
            <?php if (get_field("show_categories")) : ?>
                <ul class="article-category-filter w-full flex gap-[18px] justify-start overflow-x-auto py-[20px]"><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-transparent md:px-[40px] md:py-[10px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-primary !w-[200%] !opacity-100"></span><span class="relative">All</span>
                        <div class="z-[10]"></div>
                    </span><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-transparent md:px-[40px] md:py-[10px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-primary"></span><span class="relative">Article</span>
                        <div class="z-[10]"></div>
                    </span><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-transparent md:px-[40px] md:py-[10px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-primary"></span><span class="relative">Example</span>
                        <div class="z-[10]"></div>
                    </span><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-transparent md:px-[40px] md:py-[10px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-primary"></span><span class="relative">News</span>
                        <div class="z-[10]"></div>
                    </span></ul>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <div class="grid w-full gap-x-[19px] gap-y-[35px] relative grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
        <?php foreach ($posts as $post) : ?>
            <div class="flex flex-col justify-between h-full">
                <div>
                    <div class="relative h-[302px] rounded-md overflow-hidden"><a href="#"><img alt="Example post 3" loading="lazy" decoding="async" data-nimg="fill" class="object-cover" style="position:absolute;height:100%;width:100%;left:0;top:0;right:0;bottom:0;color:transparent" sizes="100vw" src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>"></a></div>
                    <p class="b1 mt-[20px]"><?php echo get_the_author_meta("display_name", $post->post_author); ?></p>
                    <h3 class="max-w-[386px] mb-[23px] min-h-[105px] mt-[10px]"><?php echo get_the_title($post); ?></h3>
                </div>
                <div class="justify-self-end flex items-center justify-between"><span class="pill relative w-fit inline-flex items-center justify-center px-[45px] py-[7px] rounded-full bg-quaternary">Article</span>
                    <p class="b1">10/12/2023</p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php if (get_field("show_load_more")) : ?>
        <div class="w-full flex justify-center pt-[80px]"><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-primary md:px-[30px] md:py-[20px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-white"></span><span class="relative"><?php if (get_field("load_more_text")) echo get_field("load_more_text");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        else echo "Load more"; ?></span>
                <div class="z-[10]"><svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 ml-1 rotate-90 fill-current" height="20" width="20">
                        <path d="M7.833 15 6.583 13.75 10.333 10 6.583 6.25 7.833 5 12.833 10Z"></path>
                    </svg></div>
            </span></div>
    <?php endif; ?>
</div>