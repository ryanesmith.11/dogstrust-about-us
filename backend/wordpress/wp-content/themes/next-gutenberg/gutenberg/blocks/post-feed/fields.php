<?php

//see https://github.com/Log1x/acf-builder-cheatsheet
use StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('post-feed');
$block
    ->addSelect('post_type', [
        'label' => 'Post type',
        'default_value' => 'news',
        'choices' => [],
        'instructions' => "Select the post type you want to show in the feed"
    ])
    ->addNumber('number_of_columns', [
        'label' => 'Number of columns',
        'default_value' => 3,
        'min' => 1,
        'max' => 4,
        'step' => 1,
        'instructions' => "Number of columns to show in the feed"
    ])
    ->addNumber('number_of_posts', [
        'label' => 'Number of posts',
        'default_value' => 6,
        'min' => 1,
        'max' => 10,
        'step' => 1,
        'instructions' => "Number of posts to show in the feed."
    ])
    ->addTrueFalse('show_load_more', [
        'default_value' => 1,
        'ui' => 1,
        'instructions' => "Show load more button"
    ])
    ->addText('load_more_text', [
        'label' => 'Load more text',
        'default_value' => 'Load more',
        'instructions' => "Text to show on load more button",
        'conditional_logic' => [
            [
                [
                    'field' => 'show_load_more',
                    'operator' => '==',
                    'value' => '1',
                ]
            ]
        ]
    ])
    ->addNumber('number_on_load', [
        'label' => 'Number of posts to load on load',
        'default_value' => 3,
        'min' => 1,
        'max' => 10,
        'step' => 1,
        'instructions' => "Number of posts to show on load.",
        'conditional_logic' => [
            [
                [
                    'field' => 'show_load_more',
                    'operator' => '==',
                    'value' => '1',
                ]
            ]
        ]
    ])
    ->addTrueFalse('show_search', [
        'label' => 'Show search',
        'default_value' => 0,
        'ui' => 1,
        'instructions' => "Show search field"
    ])
    ->addTrueFalse('show_categories', [
        'label' => 'Show categories',
        'default_value' => 1,
        'ui' => 1,
        'instructions' => "Show categories"
    ])
    ->addPostObject('posts', [
        'label' => 'Posts',
        'post_type' => ['post', 'page'],
        'multiple' => 1,
        'return_format' => 'object',
        'ui' => 1,
        "instructions" => "Select the news you want to show in the slider. If unselected it will get the most recent n posts"
    ]);


$global = new FieldsBuilder('post-feed');
$global
    ->addFields($block)
    ->setLocation('block', '==', 'acf/post-feed')
    ->or('block', '==', 'acf/post-feed2')
    ->or('block', '==', 'acf/post-feed3')
    ->or('block', '==', 'acf/post-feed4');

add_action('acf/init', function () use ($global) {
    acf_add_local_field_group($global->build());
});



add_filter('acf/load_field/name=post_type', 'acf_load_post_types');
function acf_load_post_types($field)
{
    foreach (get_post_types(array(
        'public'   => true,
    ), 'names') as $post_type) {
        $field['choices'][$post_type] = $post_type;
    }

    // return the field
    return $field;
}
