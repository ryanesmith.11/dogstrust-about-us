<?php

//example way of sending variabled to js files without extra <script> tags
wp_enqueue_script("tailwind", "https://cdn.tailwindcss.com")

?>

<div class="px-[30px] xl:px-[70px] relative">
    <div class="relative max-w-[1782px] w-full xl:h-[760px] lg:h-[600px] md:h-[500px] h-[400px] mx-auto">
        <div class="hero-img relative max-w-[1782px] md:w-full w-[calc(100%+30px)] xl:h-[760px] lg:h-[600px] md:h-[500px] h-[400px] rounded-sm md:rounded-xl overflow-hidden md:mx-auto z-[10] mx-[-15px]"><img alt="" loading="lazy" decoding="async" data-nimg="fill" class="object-cover" style="position:absolute;height:100%;width:100%;left:0;top:0;right:0;bottom:0;color:transparent" sizes="100vw" src="<?php echo get_field("img"); ?>">
            <div class="hero-circle absolute md:top-[58px] md:left-[80px] left-[20px] top-[25px]"><span class="button group overflow-hidden relative hover:text-primary group-hover:text-primary text-white shrink-0 inline-flex items-center cursor-pointer justify-center rounded-full border-[2px] font-medium text-neutral-100 transition border-primary md:w-[71px] md:h-[71px] w-[41px] h-[41px] bg-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-white"></span>
                    <div class="relative z-[10] w-[20px]"><svg xmlns="http://www.w3.org/2000/svg" width="100%" height="38" viewBox="0 0 27 38" fill="none">
                            <path d="M13.5 -5.46392e-07L13.5 36M13.5 36L26 21.9512M13.5 36L1 21.9512" stroke="currentColor" stroke-width="2"></path>
                        </svg></div>
                </span></div>
        </div>
        <div class="w-full h-[398px] absolute top-[-161px] left-[0px]">
            <div class="absolute top-0 left-[-3383px] pointer-events-none cheetah-line"><span></span><svg xmlns="http://www.w3.org/2000/svg" width="5913" height="986" viewBox="0 0 5913 986" fill="none">
                    <path d="M2867.92 663.983C2681.96 510.431 2438.18 588.2 2340.8 628.966C2340.8 628.966 1384.37 1054.4 756.899 952.585C465.599 901.889 206.124 751.577 45.3047 623.112C12.8438 596.249 8.81269 548.479 35.9695 516.494C193.713 638.269 502.728 833.527 799.862 869.59C1456.19 949.24 2461.52 266.671 3117.1 147.091C3369.9 96.8133 3645.71 130.053 3725.48 141.969C4113.1 185.976 4472.61 321.34 4856.41 273.57C4856.41 273.57 5268.22 209.599 5484.42 20.0897C5486.33 18.4173 5488.98 17.5811 5491.52 18.2082C5495.87 19.2535 5498.52 23.7482 5497.57 28.2429L5475.5 123.573C5523.56 120.019 5651.39 151.9 5703.37 237.822C5753.12 320.085 5851.14 383.847 5891.45 409.875V409.979C5894.31 411.756 5895.91 415.31 5895.16 418.864C5857.19 576.806 5721.93 609.419 5646.4 614.959C5489.56 621.411 5208.67 634.91 4856.41 764.326" style="stroke:var(--color-primary)" stroke-width="2"></path>
                </svg></div>
        </div>
    </div>
</div>