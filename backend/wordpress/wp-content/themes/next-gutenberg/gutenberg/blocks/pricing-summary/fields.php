<?php

//see https://github.com/Log1x/acf-builder-cheatsheet
use StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('pricing-summary');
$block
    ->addText('heading')
    ->addText('subheading');


$global = new FieldsBuilder('pricing-summary');
$global
    ->addFields($block)
    ->setLocation('block', '==', 'acf/pricing-summary');

add_action('acf/init', function () use ($global) {
    acf_add_local_field_group($global->build());
});
