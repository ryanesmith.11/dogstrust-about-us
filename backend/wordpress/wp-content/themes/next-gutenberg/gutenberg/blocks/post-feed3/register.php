<?php

require_once plugin_dir_path(__DIR__) . 'post-feed/fields.php';

//register block here
add_action('acf/init', function () {
    if (!function_exists('acf_register_block_type')) return;
    acf_register_block_type(
        array(
            'name' => 'post-feed2',
            'title' => __('Post feed 2'),
            'description' => __(''),
            'render_template' => plugin_dir_path(__FILE__) . 'block.php',
            'category' => 'g-blocks',
            'icon' => array(
                'background' => '#7e70af',
                'foreground' => '#fff',
                'src' => 'book-alt',
            ),
        )
    );
});
