<?php

$posts = get_posts(array(
    'numberposts' => get_field("number_of_posts"),
    'post_type' => get_field("post_type"),
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
));
?>

<div class="post-feed mb-[10px]">
    <?php if (get_field("show_search") || get_field("show_categories")) : ?>
        <div class="news-articles-top flex flex-col pb-[30px]">
            <?php if (get_field("show_search")) : ?>
                <div class="md:w-4/12 w-full"><input class="w-full max-w-[475px] h-[34px] px-[20px] py-[27px] border-[1px] border-solid border-secondary rounded-full" placeholder="Search"></div>
            <?php endif; ?>
            <?php if (get_field("show_categories")) : ?>
                <ul class="article-category-filter w-full flex gap-[18px] justify-start overflow-x-auto py-[20px]"><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-transparent md:px-[40px] md:py-[10px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-primary !w-[200%] !opacity-100"></span><span class="relative">All</span>
                        <div class="z-[10]"></div>
                    </span><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-transparent md:px-[40px] md:py-[10px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-primary"></span><span class="relative">Article</span>
                        <div class="z-[10]"></div>
                    </span><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-transparent md:px-[40px] md:py-[10px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-primary"></span><span class="relative">Example</span>
                        <div class="z-[10]"></div>
                    </span><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-transparent md:px-[40px] md:py-[10px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-primary"></span><span class="relative">News</span>
                        <div class="z-[10]"></div>
                    </span></ul>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <div class="grid w-full gap-x-[19px] gap-y-[35px] relative grid-cols-1">
        <?php foreach ($posts as $post) : ?>
            <div class="flex items-center h-full">
                <div class="relative lg:h-[249px] h-[160px] w-[317px] rounded-md overflow-hidden shadow-[0_4px_4px_rgba(0,0,0,0.25)] bg-[#F6F7F9] bg-opacity-30 flex items-center justify-center">
                    <div class="relative w-[285px] h-[160px]"><a href="/post/quantum-specialty-group"><img alt="Quantum Specialty Group" loading="lazy" decoding="async" data-nimg="fill" class="object-contain" sizes="100vw" src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" style="position: absolute; height: 100%; width: 100%; inset: 0px; color: transparent;"></a></div>
                </div>
                <div class="w-[715px] md:ml-[85px] ml-[40px] md:mr-[80px] mr-[40px]">
                    <h3 class=""><?php echo get_the_title($post); ?></h3>
                    <p class="b1"><?php echo get_the_content(null, null, $post); ?></p>
                </div><span class="button group overflow-hidden relative hover:text-primary group-hover:text-primary text-white shrink-0 inline-flex items-center cursor-pointer justify-center rounded-full border-[2px] font-medium text-neutral-100 transition border-primary md:w-[71px] md:h-[71px] w-[41px] h-[41px] bg-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-white"></span>
                    <div class="relative z-[10] w-[20px]"><svg xmlns="http://www.w3.org/2000/svg" width="100%" height="38" viewBox="0 0 27 38" fill="none" class="-rotate-90">
                            <path d="M13.5 -5.46392e-07L13.5 36M13.5 36L26 21.9512M13.5 36L1 21.9512" stroke="currentColor" stroke-width="2"></path>
                        </svg></div>
                </span>
            </div>
        <?php endforeach; ?>
    </div>
    <?php if (get_field("show_load_more")) : ?>
        <div class="w-full flex justify-center pt-[80px]"><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-primary md:px-[30px] md:py-[20px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-white"></span><span class="relative"><?php if (get_field("load_more_text")) echo get_field("load_more_text");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        else echo "Load more"; ?></span>
                <div class="z-[10]"><svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 ml-1 rotate-90 fill-current" height="20" width="20">
                        <path d="M7.833 15 6.583 13.75 10.333 10 6.583 6.25 7.833 5 12.833 10Z"></path>
                    </svg></div>
            </span></div>
    <?php endif; ?>
</div>