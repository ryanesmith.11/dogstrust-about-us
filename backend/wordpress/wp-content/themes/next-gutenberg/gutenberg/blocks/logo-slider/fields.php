<?php

//see https://github.com/Log1x/acf-builder-cheatsheet
use StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('logo-slider');
$block
    ->addRepeater("logos")
    ->addText('logo_name')
    ->addImage('logo', ["return_format" => "url"])
    ->endRepeater();


$global = new FieldsBuilder('logo-slider');
$global
    ->addFields($block)
    ->setLocation('block', '==', 'acf/logo-slider');

add_action('acf/init', function () use ($global) {
    acf_add_local_field_group($global->build());
});
