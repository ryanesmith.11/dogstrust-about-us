<?php

//example way of sending variabled to js files without extra <script> tags
wp_enqueue_script("tailwind", "https://cdn.tailwindcss.com");

$logos = get_field("logos");
?>

<div class="logo-slider w-full pt-[40px]">
    <div class="swiper swiper-initialized swiper-horizontal swiper-free-mode">
        <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);">
            <?php foreach ($logos as $logo) : ?>
                <img alt="logo" src="<?php echo $logo['url'] ?>">
            <?php endforeach; ?>
        </div>
    </div>
</div>