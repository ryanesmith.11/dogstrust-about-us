<?php

//see https://github.com/Log1x/acf-builder-cheatsheet
use StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('hero');
$block
    ->addWysiwyg('heading')
    ->addWysiwyg('subheading')
    ->addText('cta_text')
    ->addText('cta_link')
    ->addWysiwyg('after_cta_text')
    ->addImage('img', ["return_format" => "url"]);


$global = new FieldsBuilder('hero');
$global
    ->addFields($block)
    ->setLocation('block', '==', 'acf/hero');

add_action('acf/init', function () use ($global) {
    acf_add_local_field_group($global->build());
});


// heading: string;
// subheading: string;
// cta_text: string;
// cta_link: string;
// after_cta_text: string;
// img: string;