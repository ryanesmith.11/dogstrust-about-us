<?php

$posts = get_posts(array(
    'numberposts' => 1,
    'post_type' => get_field("post_type"),
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
));
?>

<div class="w-[100%] max-w-[100%] max-h-[100vh] min-h-0 min-w-0 overflow-hidden rounded-t-xl">
    <div class="swiper swiper-initialized swiper-horizontal swiper-backface-hidden mySwiper">
        <div class="swiper-wrapper" id="swiper-wrapper-de9758b6ae10632210" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">

            <div class="swiper-slide swiper-slide-active" style="width: 1309px;" role="group" aria-label="1 / 3" data-swiper-slide-index="0">
                <div class="xl:h-[675px] lg:h-[500px] h-[400px] w-full relative bg-secondary overflow-hidden">
                    <img alt="Example post 3" loading="lazy" decoding="async" data-nimg="fill" class="object-cover" sizes="100vw" src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" style="position: absolute; height: 100%; width: 100%; inset: 0px; color: transparent;">
                    <div class="left-1/2 top-1/2 absolute flex flex-col justify-end w-full h-full max-w-screen-xl transform -translate-x-1/2 -translate-y-1/2 pb-[87px] xl:px-[70px] px-[30px]">
                        <h1 class="max-w-[828px] text-white"><?php echo get_the_title($posts[0]); ?></h1><span class="pill relative w-fit inline-flex items-center justify-center px-[45px] py-[7px] rounded-full bg-tertiary">Category</span>
                    </div>
                </div>
            </div>

        </div>
        <div class="swiper-pagination swiper-pagination-bullets swiper-pagination-horizontal">
            <span class="swiper-pagination-bullet swiper-pagination-bullet-active" aria-current="true"></span>
            <span class="swiper-pagination-bullet"></span>
            <span class="swiper-pagination-bullet"></span>
        </div>
        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span><span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
    </div>
</div>