<?php

//see https://github.com/Log1x/acf-builder-cheatsheet
use StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('slider');
$block
    ->addSelect('post_type', [
        'label' => 'Post type',
        'default_value' => 'news',
        'choices' => [],
        'instructions' => "Select the post type you want to show in the feed"
    ])
    ->addNumber('number_of_posts', [
        'label' => 'Number of posts',
        'default_value' => 3,
        'min' => 1,
        'max' => 10,
        'step' => 1,
        'instructions' => "Number of posts to show in the slider. If you select news below, this will be ignored"
    ])
    ->addNumber('number_of_slides_per_view', [
        'label' => 'Number of slides per view',
        'default_value' => 1,
        'min' => 1,
        'max' => 4,
        'step' => 1,
        'instructions' => "Number of slides to show per view"
    ])
    ->addPostObject('posts', [
        'label' => 'Posts',
        'post_type' => ['post'],
        'multiple' => 1,
        'return_format' => 'object',
        'ui' => 1,
        "instructions" => "Select the news you want to show in the slider. If unselected it will get the most recent n posts"
    ]);


$global = new FieldsBuilder('slider');
$global
    ->addFields($block)
    ->setLocation('block', '==', 'acf/slider')
    ->or('block', '==', 'acf/slider2')
    ->or('block', '==', 'acf/slider3')
    ->or('block', '==', 'acf/testimonials');

add_action('acf/init', function () use ($global) {
    acf_add_local_field_group($global->build());
});
