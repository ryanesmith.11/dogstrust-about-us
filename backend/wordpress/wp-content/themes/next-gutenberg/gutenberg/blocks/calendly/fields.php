<?php

//see https://github.com/Log1x/acf-builder-cheatsheet
use StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('calendly');
$block
    ->addText('heading')
    ->addText('subheading');


$global = new FieldsBuilder('calendly');
$global
    ->addFields($block)
    ->setLocation('block', '==', 'acf/calendly');

add_action('acf/init', function () use ($global) {
    acf_add_local_field_group($global->build());
});
