<?php

$posts = get_posts(array(
    'numberposts' => get_field("number_of_posts"),
    'post_type' => get_field("post_type"),
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
));
?>

<div class="post-feed post-feed-4 mb-[10px]">
    <?php if (get_field("show_search") || get_field("show_categories")) : ?>
        <div class="news-articles-top flex flex-col pb-[30px]">
            <?php if (get_field("show_search")) : ?>
                <div class="md:w-4/12 w-full"><input class="w-full max-w-[475px] h-[34px] px-[20px] py-[27px] border-[1px] border-solid border-secondary rounded-full" placeholder="Search"></div>
            <?php endif; ?>
            <?php if (get_field("show_categories")) : ?>
                <ul class="article-category-filter w-full flex gap-[18px] justify-start overflow-x-auto py-[20px]"><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-transparent md:px-[40px] md:py-[10px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-primary !w-[200%] !opacity-100"></span><span class="relative">All</span>
                        <div class="z-[10]"></div>
                    </span><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-transparent md:px-[40px] md:py-[10px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-primary"></span><span class="relative">Article</span>
                        <div class="z-[10]"></div>
                    </span><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-transparent md:px-[40px] md:py-[10px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-primary"></span><span class="relative">Example</span>
                        <div class="z-[10]"></div>
                    </span><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-transparent md:px-[40px] md:py-[10px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-primary"></span><span class="relative">News</span>
                        <div class="z-[10]"></div>
                    </span></ul>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <div class="grid w-full gap-x-[19px] gap-y-[35px] relative grid-cols-1">
        <?php foreach ($posts as $post) : ?>
            <div class="relative flex flex-col items-start">
                <div class="group relative w-full h-full overflow-hidden rounded-md cursor-pointer">
                    <div class="relative h-[431px] w-full overflow-hidden shrink-0 bg-transparent"><img alt="Our impact" loading="lazy" decoding="async" data-nimg="fill" class="object-cover" style="position:absolute;height:100%;width:100%;left:0;top:0;right:0;bottom:0;color:transparent" sizes="100vw" srcset="/_next/image?url=https%3A%2F%2Fwww.wp.dogstrust.astlb.dev%2Fwp-content%2Fuploads%2F2023%2F08%2FStaffordshireBullTerrier_dog_cardiff_dogstrust_01.jpg.webp&amp;w=640&amp;q=75 640w, /_next/image?url=https%3A%2F%2Fwww.wp.dogstrust.astlb.dev%2Fwp-content%2Fuploads%2F2023%2F08%2FStaffordshireBullTerrier_dog_cardiff_dogstrust_01.jpg.webp&amp;w=750&amp;q=75 750w, /_next/image?url=https%3A%2F%2Fwww.wp.dogstrust.astlb.dev%2Fwp-content%2Fuploads%2F2023%2F08%2FStaffordshireBullTerrier_dog_cardiff_dogstrust_01.jpg.webp&amp;w=828&amp;q=75 828w, /_next/image?url=https%3A%2F%2Fwww.wp.dogstrust.astlb.dev%2Fwp-content%2Fuploads%2F2023%2F08%2FStaffordshireBullTerrier_dog_cardiff_dogstrust_01.jpg.webp&amp;w=1080&amp;q=75 1080w, /_next/image?url=https%3A%2F%2Fwww.wp.dogstrust.astlb.dev%2Fwp-content%2Fuploads%2F2023%2F08%2FStaffordshireBullTerrier_dog_cardiff_dogstrust_01.jpg.webp&amp;w=1200&amp;q=75 1200w, /_next/image?url=https%3A%2F%2Fwww.wp.dogstrust.astlb.dev%2Fwp-content%2Fuploads%2F2023%2F08%2FStaffordshireBullTerrier_dog_cardiff_dogstrust_01.jpg.webp&amp;w=1920&amp;q=75 1920w, /_next/image?url=https%3A%2F%2Fwww.wp.dogstrust.astlb.dev%2Fwp-content%2Fuploads%2F2023%2F08%2FStaffordshireBullTerrier_dog_cardiff_dogstrust_01.jpg.webp&amp;w=2048&amp;q=75 2048w, /_next/image?url=https%3A%2F%2Fwww.wp.dogstrust.astlb.dev%2Fwp-content%2Fuploads%2F2023%2F08%2FStaffordshireBullTerrier_dog_cardiff_dogstrust_01.jpg.webp&amp;w=3840&amp;q=75 3840w" src="/_next/image?url=https%3A%2F%2Fwww.wp.dogstrust.astlb.dev%2Fwp-content%2Fuploads%2F2023%2F08%2FStaffordshireBullTerrier_dog_cardiff_dogstrust_01.jpg.webp&amp;w=3840&amp;q=75"></div>
                    <div class="right-[17px] bottom-[17px] absolute"></div>
                    <div class="bg-primary opacity-0 group-hover:opacity-20 z-[30] mix-blend-[soft-light] duration-300 pointer-events-none w-full h-full transition-all absolute top-0 left-0"></div>
                    <div class="group-hover:scale-[1] absolute top-[18px] right-[18px] transform scale-[0] duration-300"><span class="button circle-button primary effect-fade">
                            <div class="relative z-[10] w-[20px]"><svg xmlns="http://www.w3.org/2000/svg" width="100%" height="38" viewBox="0 0 27 38" fill="none" class="-rotate-90">
                                    <path d="M13.5 -5.46392e-07L13.5 36M13.5 36L26 21.9512M13.5 36L1 21.9512" stroke="currentColor" stroke-width="2"></path>
                                </svg></div>
                        </span></div>
                    <h3 class=""><?php echo get_the_title($post); ?></h3>
                </div>
                <div class="modal fixed top-0 left-0 w-full h-full z-[83886360] flex md:items-center items-end justify-center pointer-events-none">
                    <div class="modal-overlay fixed top-0 left-0 w-full h-full bg-white z-[83886360] transition-all bg-opacity-0 pointer-events-none"></div>
                    <div class="modal-content relative bg-primary w-full max-w-[650px] transition-all md:p-[77px] p-[30px] z-[83886361] md:rounded-[20px] rounded-t-[20px] scale-0">
                        <div class="absolute top-[33px] right-[33px] cursor-pointer"><svg xmlns="http://www.w3.org/2000/svg" width="21" height="20" viewBox="0 0 21 20" fill="none">
                                <line x1="2.49609" y1="17.8577" x2="18.3538" y2="1.99996" stroke="white" stroke-width="3" stroke-linecap="round"></line>
                                <line x1="3.12132" y1="2" x2="18.979" y2="17.8577" stroke="white" stroke-width="3" stroke-linecap="round"></line>
                            </svg></div>
                        <div class="flex flex-col md:flex-row gap-[20px] items-center justify-between w-full mb-[27px]">
                            <h3 class="flex-shrink-0 mb-0">Our impact</h3>
                        </div>
                        <div>
                            <p>dfgdfgfsg</p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>


    </div>
    <?php if (get_field("show_load_more")) : ?>
        <div class="w-full flex justify-center pt-[80px]"><span class="button group whitespace-nowrap shrink-0 overflow-hidden relative w-fit inline-flex items-center cursor-pointer justify-center rounded-full font-secondary text-secondary  transition border-[2px] bg-primary md:px-[30px] md:py-[20px] px-[25px] py-[18px] border-primary"><span class="group-hover:w-[200%] absolute left-0 w-[20px] aspect-square opacity-0 group-hover:opacity-100 transition-all duration-300 ease-out -translate-x-1/2 rounded-full bg-white"></span><span class="relative"><?php if (get_field("load_more_text")) echo get_field("load_more_text");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        else echo "Load more"; ?></span>
                <div class="z-[10]"><svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 ml-1 rotate-90 fill-current" height="20" width="20">
                        <path d="M7.833 15 6.583 13.75 10.333 10 6.583 6.25 7.833 5 12.833 10Z"></path>
                    </svg></div>
            </span></div>
    <?php endif; ?>
</div>