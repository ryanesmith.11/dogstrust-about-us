<?php

//see https://github.com/Log1x/acf-builder-cheatsheet
use StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('toggle-menu');
$block
    ->addPostObject('pages', [
        'label' => 'Pages',
        'post_type' => ['page'],
        'multiple' => 1,
        'return_format' => 'object',
        'ui' => 1,
        'instructions' => 'Select the pages you want to show in the menu.'
    ]);


$global = new FieldsBuilder('toggle-menu');
$global
    ->addFields($block)
    ->setLocation('block', '==', 'acf/toggle-menu');

add_action('acf/init', function () use ($global) {
    acf_add_local_field_group($global->build());
});
