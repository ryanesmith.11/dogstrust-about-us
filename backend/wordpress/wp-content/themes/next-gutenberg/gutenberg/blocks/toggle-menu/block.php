<?php

$pages = get_field("pages");

?>
<div class="flex justify-center w-full pb-[80px] pt-[20px] font-secondary text-[13px] ">
    <ul class="toggle-menu flex border-primary border-[1px] rounded-full">
        <?php foreach ($pages as $page) {
            $title = $page->post_title;
            $link = get_permalink($page->ID);
            echo "<li class='rounded-full py-[5px] px-[28px]'><a href='$link'>$title</a></li>";
        } ?>
    </ul>
</div>