<?php

//see https://github.com/Log1x/acf-builder-cheatsheet
use StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('marketo');
$block
    ->addText('marketo_base_url', ['instructions' => 'eg. //go.companyname.com'])
    ->addText('marketo_munchkin_id', ['instructions' => 'eg. 350-OKO-721'])
    ->addText('marketo_form_id',    ['instructions' => 'eg. 1234']);


$global = new FieldsBuilder('marketo');
$global
    ->addFields($block)
    ->setLocation('block', '==', 'acf/marketo');

add_action('acf/init', function () use ($global) {
    acf_add_local_field_group($global->build());
});


// heading: string;
// subheading: string;
// cta_text: string;
// cta_link: string;
// after_cta_text: string;
// img: string;