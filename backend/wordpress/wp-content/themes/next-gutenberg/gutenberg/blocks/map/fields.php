<?php

//see https://github.com/Log1x/acf-builder-cheatsheet
use StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('map');
$block
    ->addNumber("focus_latitude")
    ->addNumber("focus_longitude")
    ->addNumber("zoom_level")
    ->addImage("marker_icon")
    ->addRepeater("markers")
    ->addNumber("marker_latitude")
    ->addNumber("marker_longitude")
    ->endRepeater();


$global = new FieldsBuilder('map');
$global
    ->addFields($block)
    ->setLocation('block', '==', 'acf/map');

add_action('acf/init', function () use ($global) {
    acf_add_local_field_group($global->build());
});
