<?php

require_once plugin_dir_path(__DIR__) . 'slider/fields.php';

//register block here
add_action('acf/init', function () {
    if (!function_exists('acf_register_block_type')) return;
    acf_register_block_type(
        array(
            'name' => 'slider2',
            'title' => __('Slider 2'),
            'description' => __(''),
            'render_template' => plugin_dir_path(__FILE__) . 'block.php',
            'category' => 'g-blocks',
            'icon' => array(
                'background' => '#7e70af',
                'foreground' => '#fff',
                'src' => 'book-alt',
            ),
        )
    );
});
