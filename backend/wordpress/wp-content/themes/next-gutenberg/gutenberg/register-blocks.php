<?php


require_once plugin_dir_path(__FILE__) . 'blocks/hero/register.php';
require_once plugin_dir_path(__FILE__) . 'blocks/hero2/register.php';
require_once plugin_dir_path(__FILE__) . 'blocks/marketo/register.php';
require_once plugin_dir_path(__FILE__) . 'blocks/pricing-summary/register.php';
require_once plugin_dir_path(__FILE__) . 'blocks/calendly/register.php';
require_once plugin_dir_path(__FILE__) . 'blocks/slider/register.php';
require_once plugin_dir_path(__FILE__) . 'blocks/slider2/register.php';
require_once plugin_dir_path(__FILE__) . 'blocks/logo-slider/register.php';
require_once plugin_dir_path(__FILE__) . 'blocks/testimonials/register.php';
require_once plugin_dir_path(__FILE__) . 'blocks/post-feed/register.php';
require_once plugin_dir_path(__FILE__) . 'blocks/post-feed2/register.php';
require_once plugin_dir_path(__FILE__) . 'blocks/post-feed4/register.php';
require_once plugin_dir_path(__FILE__) . 'blocks/toggle-menu/register.php';
require_once plugin_dir_path(__FILE__) . 'blocks/map/register.php';
