# Vercel ISR helper

## ✅ Features

- 👉 &nbsp;&nbsp;Builds upon WP REST API, adding useful endpoints to build your next app router application
- 🚗 &nbsp;&nbsp;Allows setup of revalidation endpoints to Incrementally publish posts (Next ISR)

## 🛠 Installation

1. Download the plugin as a `.zip` file [from the repository](https://github.com/aderaaij/wp-vercel-deploy-hooks/archive/main.zip)
2. Login to your WordPress site and go to `Plugins -> Add new -> Upload plugin`
3. Locate the `.zip` file on your machine, upload and activate

## ⚙️ Settings / Configuration

To enable the plugin, you will need to create a [Deploy Hook for your Vercel Project](https://vercel.com/docs/more/deploy-hooks).

### 🎚 Settings

After you've created your deploy hook, navigate to `Deploy -> Settings` in the WordPress admin menu and paste your Vercel Deploy hook URL. On the settings page you can also activate deploys when you publish or update a post (disabled by default).

You can configure the Vercel Deploy Hook URL in your wp-config.php or other configuration file to be based on your environment using the constant `WP_VERCEL_WEBHOOK_ADDRESS`. An example follows:

```php
switch (WP_ENVIRONMENT_TYPE) {
    case "live":
    case "production":
        define( 'WP_VERCEL_WEBHOOK_ADDRESS', 'https://api.vercel.com/v1/integrations/deploy/<sample>/<sample>' );
        define( 'WP_VERCEL_REVALIDATION_BASE_URL', 'https://my-app.vercel.app' );
        break;

    case "test":
    case "staging":
        define( 'WP_VERCEL_WEBHOOK_ADDRESS', 'https://api.vercel.com/v1/integrations/deploy/<sample>/<sample>' );
        define( 'WP_VERCEL_REVALIDATION_BASE_URL', 'https://my-app.vercel.app' );
        break;

    case "dev":
    case "development":
        define( 'WP_VERCEL_WEBHOOK_ADDRESS', 'https://api.vercel.com/v1/integrations/deploy/<sample>/<sample>' );
        define( 'WP_VERCEL_REVALIDATION_BASE_URL', 'https://my-app.vercel.app' );
        break;

    case "local":
        define( 'WP_VERCEL_WEBHOOK_ADDRESS', 'https://api.vercel.com/v1/integrations/deploy/<sample>/<sample>' );
        define( 'WP_VERCEL_REVALIDATION_BASE_URL', 'https://my-app.vercel.app' );
        break;
}
```

See <https://make.wordpress.org/core/2020/07/24/new-wp_get_environment_type-function-in-wordpress-5-5/> for more guidance on using `WP_ENVIRONMENT_TYPE`

## 👯 Contributors & Credits

This plugin was based on the excellent WordPress Plugin [WP Netlify Webhook Deploy](https://github.com/lukethacoder/wp-netlify-webhook-deploy)

## 🤔 To Do

- Add support for linking vercel project directly, removing need to define multiple configs and providing a closer link incase of changes.
- Add support for deploy and build statusses and updates through the [Vercel API](https://vercel.com/docs/api)
- Add support for multiple Vercel deploy endpoints

## 🔌 Shameless Plug

Made ❤️ lovingly by the Dev team @ [Pomegranate](https://pomegranate.co.uk)

We make a tonne of custom plugins and funcionality for utilizing wordpress to it's full potential.
We also make websites; ecommerce, interactive and portfolio ones.

If you found this plugin useful, consider [contacting us](https://pomegranate.co.uk/contact-us) to help with your project.
